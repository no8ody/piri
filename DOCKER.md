# Build artifact

    mvn clean package -DskipTests

# Instructions using docker CLI

## Build image
 
    docker build -t piri .
    
## Start image in container

    docker run -v ${PWD}/docker-custom.properties:/application.properties -p 80:8080 piri
    curl http://127.0.0.1/actuator/health

## Tag and push image to registry (https://hub.docker.com/r/chrisss404/piri/)
    
    # login if not already authenticated
    docker login --username=yourhubusername
    
    docker tag piri chrisss404/piri:latest
    docker push chrisss404/piri


# Instructions using docker compose

## Build image

    docker-compose build

## Start image in container

    docker-compose up
    curl http://127.0.0.1/actuator/health

## Tag and push image to registry (https://hub.docker.com/r/chrisss404/piri/)   
    
    docker-compose push
