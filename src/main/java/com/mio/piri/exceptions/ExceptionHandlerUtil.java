/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.exceptions;

import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

public class ExceptionHandlerUtil {

    public static String extractValidationErrorMessage(WebExchangeBindException wex) {
        List<String> erroneousFields = wex.getFieldErrors().stream().map(fe -> fe.getField() + " " + fe.getDefaultMessage()).collect(Collectors.toList());
        return String.format("%s: %s", wex.getReason(), erroneousFields);
    }

    public static ResponseStatusException convertToResponseException(WebExchangeBindException wex, String message) {
        return new ResponseStatusException(wex.getStatus(), message);
    }

}
