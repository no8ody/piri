/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.metrics;

import io.micrometer.core.instrument.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

public class CommandStatistics {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final AtomicLong count = new AtomicLong(0);

    @Autowired
    private MeterFactory meterFactory;

    @PostConstruct
    public void startRegularStatisticsLog() {
        Flux.interval(Duration.ofMinutes(1), Duration.ofMinutes(10)).subscribe(logNumber -> {
                    long current = calculateCurrent(meterFactory.findAllCommandTimers());
                    long processed = calculateDifference(current, count.get());
                    updateCount(current);
                    logStatistics(processed, current, logNumber);
                },
                t -> logger.error(t.toString(), t));
    }

    long calculateCurrent(Collection<Timer> allTimers) {
        return allTimers == null ? 0L : allTimers.stream().mapToLong(Timer::count).sum();
    }

    long calculateDifference(long current, long old) {
        assert current >= old;
        return current - old;
    }

    long updateCount(long current) {
        return count.getAndSet(current);
    }

    void logStatistics(long processed, long current, long callNumber) {
        logger.info(String.format("Processed [%d] new commands. Overall: [%d]. Avg: [%.2f].", processed, current, (float) current / callNumber));
    }

}
