/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.metrics;

import com.mio.piri.nodes.IriNode;
import com.mio.piri.nodes.selection.SessionBinding;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.micrometer.core.instrument.*;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.distribution.DistributionStatisticConfig;
import io.micrometer.core.instrument.search.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Map;

import static io.github.resilience4j.circuitbreaker.utils.MetricNames.*;
import static io.github.resilience4j.micrometer.MetricUtils.getName;

public class MeterFactory {

    public static final String NAME_IRI_COMMANDS_TIMER = "iri.commands";
    private static final String REJECTED_COMMANDS_COUNTER_NAME = "iri.commands.rejected";
    private static final String RETRIED_COMMANDS_COUNTER_NAME = "iri.commands.retry";
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static final String COMMAND = "command";
    public static final String NODE_KEY = "key";
    private static final String ERROR = "error";
    public static final String NODE_NAME = "node";

    private final MeterRegistry registry;

    public MeterFactory(MeterRegistry registry) {
        this.registry = registry;
    }

    public Counter createRejectedCommandCounter(String command) {
        logger.debug("Create counter for rejected [{}] commands.", command);
        return Counter.builder(REJECTED_COMMANDS_COUNTER_NAME).tag(COMMAND, command).register(registry);
    }

    public Counter createFailedCommandCounter(String command, String error) {
        logger.debug("Create counter for retried [{}] commands", command);
        return Counter.builder(RETRIED_COMMANDS_COUNTER_NAME).tag(COMMAND, command).tag(ERROR, error).register(registry);
    }

    public Timer createCommandTimer(String command, String nodeName, String nodeKey) {
        logger.debug("Create timer for [{}] commands forwarded to node [{}].", command, nodeName);
        return Timer.builder(NAME_IRI_COMMANDS_TIMER)
                .tag(COMMAND, command)
                .tag(NODE_NAME, nodeName)
                .tag(NODE_KEY, nodeKey)
                .register(registry);
    }

    public Gauge createRateLimiterCountGauge(Map<String, RateLimiter> rateLimiters) {
        return Gauge.builder("piri.rate.limiters.count", rateLimiters, Map::size).register(registry);
    }

    public Gauge createTotalNodesGauge(Map<String, IriNode> nodes) {
        return Gauge.builder("iri.nodes.total", nodes, Map::size).register(registry);
    }

    public Gauge createActiveNodesGauge(Map<String, IriNode> nodes) {
        return Gauge.builder("iri.nodes.active", nodes, n -> n.values().stream().filter(IriNode::isAvailable).count()).register(registry);
    }

    public Gauge createInactiveNodesGauge(Map<String, IriNode> nodes) {
        return Gauge.builder("iri.nodes.inactive", nodes, n -> n.values().stream().filter(node -> !node.isAvailable()).count()).register(registry);
    }

    public Gauge createSyncedNodesGauge(Map<String, IriNode> nodes) {
        return Gauge.builder("iri.nodes.synced", nodes, n -> n.values().stream().filter(IriNode::isSynced).count()).register(registry);
    }

    public Gauge createClientMappingsGauge(SessionBinding mapper) {
        return Gauge.builder("iri.nodes.clients", mapper, SessionBinding::getSize).register(registry);
    }

    // see io.github.resilience4j.micrometer.CircuitBreakerMetrics.bindTo
    public void createMetricsForCircuitBreaker(CircuitBreaker circuitBreaker) {
        final String prefix = "iri";
        final String name = "circuit";
        Gauge.builder(getName(prefix, name, STATE), circuitBreaker, (cb) -> cb.getState().getOrder())
                .tag(NODE_NAME, circuitBreaker.getName())
                .register(registry);
        Gauge.builder(getName(prefix, name, FAILED), circuitBreaker, (cb) -> cb.getMetrics().getNumberOfFailedCalls())
                .tag(NODE_NAME, circuitBreaker.getName())
                .register(registry);
        Gauge.builder(getName(prefix, name, NOT_PERMITTED), circuitBreaker, (cb) -> cb.getMetrics().getNumberOfNotPermittedCalls())
                .tag(NODE_NAME, circuitBreaker.getName())
                .register(registry);
        Gauge.builder(getName(prefix, name, SUCCESSFUL), circuitBreaker, (cb) -> cb.getMetrics().getNumberOfSuccessfulCalls())
                .tag(NODE_NAME, circuitBreaker.getName())
                .register(registry);
    }

    public Timer.Sample sample() {
        return Timer.start(registry);
    }

    public Collection<Timer> findAllCommandTimers() {
        Search search = registry.find("iri.commands");
        return search.timers();
    }

    @PostConstruct
    public void afterPropertiesSet() {
        // add percentiles to all iri timers
        registry.config().meterFilter(
                new MeterFilter() {
                    @Override
                    public DistributionStatisticConfig configure(Meter.Id id, DistributionStatisticConfig config) {
                        if (id.getName().startsWith("iri")) {
                            return DistributionStatisticConfig.builder()
                                    //.percentiles(0.5, 0.95)
                                    .percentilesHistogram(true)
                                    .build()
                                    .merge(config);
                        }
                        return config;
                    }
                });
    }

}
