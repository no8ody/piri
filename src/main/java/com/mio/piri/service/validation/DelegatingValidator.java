/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.Map;

public class DelegatingValidator implements Validator {

    private final Class<?> supportedClass;
    private final Map<Class, Validator> validators = new HashMap<>();

    public DelegatingValidator(Class<?> supportedClass, IriCommandValidator... validator) {
        this.supportedClass = supportedClass;
        for (IriCommandValidator v : validator) {
            assert v.supports(v.getSupportedCommand());
            Validator old = validators.put(v.getSupportedCommand(), v);
            if (old != null) {
                throw new IllegalStateException("There is already a validator for class " + v.getSupportedCommand().getName());
            }
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return supportedClass.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Validator validator = validators.get(target.getClass());
        if (validator != null) {
            validator.validate(target, errors);
        }
    }

}
