/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.GetBalances;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;


public class GetBalancesValidator implements IriCommandValidator {

    private final int maxAddresses;
    private final int maxTips;

    private final MinMaxValidator minMaxValidator;

    @Override
    public boolean supports(Class<?> aClass) {
        return getSupportedCommand().equals(aClass);
    }

    @Override
    public Class getSupportedCommand() {
        return GetBalances.class;
    }

    public GetBalancesValidator(Environment env, MinMaxValidator minMaxValidator) {
        this.minMaxValidator = minMaxValidator;
        maxAddresses = env.getProperty("iri.getBalances.addresses.max", Integer.class, Integer.MAX_VALUE);
        maxTips = env.getProperty("iri.getBalances.tips.max", Integer.class, Integer.MAX_VALUE);
    }

    @Override
    public void validate(Object target, Errors errors) {
        GetBalances command = (GetBalances) target;
        minMaxValidator.validateMaxSize(command.getAddresses() == null ? 0 : command.getAddresses().size(), maxAddresses, "addresses", errors);
        minMaxValidator.validateMaxSize(command.getTips() == null ? 0 : command.getTips().size(), maxTips, "tips", errors);
    }

}
