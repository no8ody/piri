/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import org.springframework.validation.Errors;

public class MinMaxValidator {

    // see ValidationMessages.properties for 'correct' default messages. Don't know how to reuse them properly.
    public static final String MAX_MESSAGE = "> ";
    public static final String MIN_MESSAGE = "< ";
    public static final String MAX_SIZE_MESSAGE = "size > ";

    public void validate(int value, int min, int max, String fieldName, Errors errors) {
        validateMax(value, max, fieldName, errors);
        validateMin(value, min, fieldName, errors);
    }

    private void validateMin(int value, int min, String fieldName, Errors errors) {
        if (value < min) {
            errors.rejectValue(fieldName, "Min", MIN_MESSAGE + min);
        }
    }

    private void validateMax(int value, int max, String fieldName, Errors errors) {
        if (value > max) {
            errors.rejectValue(fieldName, "Max", MAX_MESSAGE + max);
        }
    }

    public void validateMaxSize(int value, int max, String fieldName, Errors errors) {
        if (value > max) {
            errors.rejectValue(fieldName, "Size", MAX_SIZE_MESSAGE + max);
        }
    }

}
