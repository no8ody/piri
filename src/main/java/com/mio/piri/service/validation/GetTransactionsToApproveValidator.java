/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.GetTransactionsToApprove;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;


public class GetTransactionsToApproveValidator implements IriCommandValidator {

    private final int max;
    private final int min;

    private final MinMaxValidator minMaxValidator;

    public GetTransactionsToApproveValidator(Environment env, MinMaxValidator minMaxValidator) {
        this.minMaxValidator = minMaxValidator;
        min = env.getProperty("iri.getTransactionsToApprove.depth.min", Integer.class, 0);
        max = env.getProperty("iri.getTransactionsToApprove.depth.max", Integer.class, Integer.MAX_VALUE);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return getSupportedCommand().equals(aClass);
    }

    @Override
    public Class getSupportedCommand() {
        return GetTransactionsToApprove.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        GetTransactionsToApprove command = (GetTransactionsToApprove) target;
        minMaxValidator.validate(command.getDepth(), min, max,"depth", errors);
    }

}
