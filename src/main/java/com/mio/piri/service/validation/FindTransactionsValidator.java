/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.FindTransactions;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;


public class FindTransactionsValidator implements IriCommandValidator {

    private final int maxAddresses;
    private final int maxBundles;
    private final int maxTags;
    private final int maxApprovees;

    private final MinMaxValidator minMaxValidator;

    @Override
    public boolean supports(Class<?> aClass) {
        return getSupportedCommand().equals(aClass);
    }

    @Override
    public Class getSupportedCommand() {
        return FindTransactions.class;
    }

    public FindTransactionsValidator(Environment env, MinMaxValidator minMaxValidator) {
        this.minMaxValidator = minMaxValidator;
        maxAddresses = env.getProperty("iri.findTransactions.addresses.max", Integer.class, Integer.MAX_VALUE);
        maxBundles = env.getProperty("iri.findTransactions.bundles.max", Integer.class, Integer.MAX_VALUE);
        maxTags = env.getProperty("iri.findTransactions.tags.max", Integer.class, Integer.MAX_VALUE);
        maxApprovees = env.getProperty("iri.findTransactions.approvees.max", Integer.class, Integer.MAX_VALUE);
    }

    @Override
    public void validate(Object target, Errors errors) {
        FindTransactions command = (FindTransactions) target;
        minMaxValidator.validateMaxSize(command.getAddresses() == null ? 0 : command.getAddresses().size(), maxAddresses, "addresses", errors);
        minMaxValidator.validateMaxSize(command.getBundles() == null ? 0 : command.getBundles().size(), maxBundles, "bundles", errors);
        minMaxValidator.validateMaxSize(command.getTags() == null ? 0 : command.getTags().size(), maxTags, "tags", errors);
        minMaxValidator.validateMaxSize(command.getApprovees() == null ? 0 : command.getApprovees().size(), maxApprovees, "approvees", errors);
    }

}
