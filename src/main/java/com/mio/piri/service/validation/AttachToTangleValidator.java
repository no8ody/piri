/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.AttachToTangle;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;


public class AttachToTangleValidator implements IriCommandValidator {

    private final int max;
    private final int min;
    private final int maxTrytes;

    private final MinMaxValidator minMaxValidator;

    @Override
    public boolean supports(Class<?> aClass) {
        return getSupportedCommand().equals(aClass);
    }

    @Override
    public Class getSupportedCommand() {
        return AttachToTangle.class;
    }

    public AttachToTangleValidator(Environment env, MinMaxValidator minMaxValidator) {
        this.minMaxValidator = minMaxValidator;
        min = env.getProperty("iri.attachToTangle.minWeightMagnitude.min", Integer.class, 0);
        max = env.getProperty("iri.attachToTangle.minWeightMagnitude.max", Integer.class, Integer.MAX_VALUE);
        maxTrytes = env.getProperty("iri.attachToTangle.trytes.max", Integer.class, Integer.MAX_VALUE);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AttachToTangle command = (AttachToTangle) target;
        minMaxValidator.validateMaxSize(command.getTrytes() == null ? 0 : command.getTrytes().size(), maxTrytes, "trytes", errors);
        // command.getMinWeightMagnitude can be null here
        minMaxValidator.validate(command.getMinWeightMagnitude() == null ? 0 : command.getMinWeightMagnitude(), min, max, "minWeightMagnitude", errors);
    }

}
