/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mio.piri.service;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.process.GetNodeInfoResponsePostProcessor;
import com.mio.piri.exceptions.ExceptionHandlerUtil;
import com.mio.piri.exceptions.NoNodeAvailable;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.nodes.Node;
import com.mio.piri.nodes.NodeRegistry;
import com.mio.piri.nodes.selection.NodeSelector;
import com.mio.piri.service.validation.DelegatingValidator;
import com.mio.piri.tolerance.RateLimitOperations;
import com.mio.piri.util.ClientSessionExtractor;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.reactor.ratelimiter.operator.RateLimiterOperator;
import io.micrometer.core.instrument.Counter;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.codec.DecodingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

@RestController
public class IriApiHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Logger commandLogger = LoggerFactory.getLogger("commands");

    private final CommandChecker commandChecker;

    private final MeterFactory metrics;

    private final DelegatingValidator commandValidator;

    private final ClientSessionExtractor session;

    private final RateLimitOperations rateLimitOperations;

    private final NodeSelector nodeSelector;

    private final NodeRegistry nodeRegistry;

    private final GetNodeInfoResponsePostProcessor nodeInfoPostProcessor;

    private final Map<String, Counter> rejectedCommandsCounter = new ConcurrentHashMap<>();
    private final Map<Tuple2<String, String>, Counter> failedCommandsCounter = new ConcurrentHashMap<>();

    @Autowired
    public IriApiHandler(NodeSelector nodeSelector, NodeRegistry nodeRegistry, CommandChecker commandChecker, DelegatingValidator commandValidator,
                         ClientSessionExtractor sessionExtractor, GetNodeInfoResponsePostProcessor getNodeInfoResponsePostProcessor,
                         RateLimitOperations rateLimitOperations, MeterFactory metrics) {
        this.nodeSelector = nodeSelector;
        this.nodeRegistry = nodeRegistry;
        this.commandChecker = commandChecker;
        this.commandValidator = commandValidator;
        this.session = sessionExtractor;
        this.nodeInfoPostProcessor = getNodeInfoResponsePostProcessor;
        this.rateLimitOperations = rateLimitOperations;
        this.metrics = metrics;
    }

    @CrossOrigin
    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> exchangeWithIri(@Valid @RequestBody IriCommand command, ServerHttpRequest request) {

        String ip = session.getClientIp(request);
        String sslId = StringUtils.abbreviate(session.getClientSslId(request), 64);
        command.setSessionId(StringUtils.defaultIfBlank(sslId, ip));
        command.setIp(ip);
        command.setUserAgent(StringUtils.abbreviate(session.getUserAgent(request), 64));

        // TODO use defer and retry
        Mono<ResponseEntity<String>> responseEntityMono = executeCommand(command)
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command))
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command))
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command))
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command))
                .map(responseEntity -> postProcess(command, responseEntity))
                .doOnError(t -> logException(t, command))
                .doOnSuccess(responseEntity -> logIfError(responseEntity, command));

        if (rateLimitOperations.isIpRateLimited(ip)) {
            RateLimiter rateLimiter = rateLimitOperations.rateLimiter(ip, command.getCommand());
            RateLimiter globalLimiter = rateLimitOperations.globalRateLimiter();
            rateLimitOperations.applyContentSpecificRateLimitWeight(command, rateLimiter);

            responseEntityMono = responseEntityMono
                    .transform(RateLimiterOperator.of(rateLimiter))
                    .transform(RateLimiterOperator.of(globalLimiter));
        }

        return responseEntityMono;

    }

    private ResponseEntity<String> postProcess(IriCommand command, ResponseEntity<String> responseEntity) {
        ResponseEntity<String> postProcessed = responseEntity;
        if (nodeInfoPostProcessor.accept(command, responseEntity)) {
            nodeInfoPostProcessor.updateGlobalNodeInfo(nodeRegistry.getAllNodes().values());
            postProcessed = ResponseEntity.status(responseEntity.getStatusCode())
                    .headers(responseEntity.getHeaders())
                    .body(nodeInfoPostProcessor.postProcess(responseEntity.getBody()));
        }
        return postProcessed;
    }

    private Mono<ResponseEntity<String>> retry(Throwable t, IriCommand command) {
        failedCommandCounter(command.getCommand(), t).increment();
        logger.info("Retry #{} for client [{}]: {}", command.getExecutionHistory().size(), command.getIp(), StringUtils.abbreviate(command.toString(), 100));
        if (logger.isTraceEnabled()) {
            logger.trace(t.toString(), t);
        }
        return executeCommand(command);
    }

    private Mono<ResponseEntity<String>> executeCommand(IriCommand command) {
        return commandChecker.isEnabled(command) ? forward(command) : unauthorized(command);
    }

    private Mono<ResponseEntity<String>> forward(IriCommand command) {
        List<Node> nodes = nodeSelector.selectNodes(command);
        if (nodes.isEmpty()) {
            String msg = String.format("Rejecting command [%s] because no node is available.", command.getCommand());
            logger.warn(msg);
            return Mono.error(new NoNodeAvailable(msg));
        } else {
            Node node = nodes.get();
            logCallToCommandLog(node.getName(), command);
            command.getExecutionHistory().add(node.getName());
            // TODO call multiple nodes
            // get several nodes.
            // flux zip run in parallel on another scheduler.
            // handle result flat/map/then something return.
            return node.call(command)
                    .doOnNext(responseEntity -> logToCommandLogIfError(node.getName(), command, responseEntity))
                    .map(responseEntity -> {
                        HttpStatus httpStatus = responseEntity.getStatusCode();
                        // TODO add error handling for unsynced get node info
                        if (httpStatus.isError()) {
                            if (command.getResponseErrorHandler().isCriticalError(httpStatus, responseEntity.getBody())) {
                                throw new ResponseStatusException(httpStatus, responseEntity.getBody());
                            }
                        } else {
                            // successful requests might count multiple times. this is a workaround for not being able
                            // to extend the rate limit dynamically within one rate limiting period.
                            rateLimitOperations.applySuccessRateLimitWeight(command.getIp(), command.getCommand());
                        }
                        return responseEntity;
                    });
        }
    }

    private void logCallToCommandLog(String nodeName, IriCommand command) {
        commandLogger.info("#A [{}] #IP [{}] #N [{}] #C [{}] #D [{}] #ID [{}] #UA [{}]",
                "REQ",
                command.getIp(),
                nodeName,
                command.getCommand(),
                command.toString(),
                command.getSessionId(),
                command.getUserAgent());
    }

    private void logToCommandLogIfError(String nodeName, IriCommand command, ResponseEntity<String> responseEntity) {
        if (responseEntity.getStatusCode().isError()) {
            commandLogger.warn("#A [{}] #IP [{}] #N [{}] #C [{}] #D [{} => {}] #ID [{}] #UA [{}]",
                    "ERR",
                    command.getIp(),
                    nodeName,
                    command.getCommand(),
                    responseEntity.getStatusCode(),
                    responseEntity.getBody(),
                    command.getSessionId(),
                    command.getUserAgent());
        }
    }

    private void logIfError(ResponseEntity<String> responseEntity, IriCommand command) {
        if (responseEntity.getStatusCode().isError()) {
            logger.warn("Returning error status code. #IP [{}} #C [{}] #D [{} => {}]",
                    command.getIp(),
                    command.getCommand(),
                    responseEntity.getStatusCodeValue(),
                    responseEntity.getBody());
        }
    }

    private void logException(Throwable t, IriCommand command) {
        logger.warn("[{}] #[ERR] #IP [{}] #C [{}] #D [{}]", t.getClass().getSimpleName(), command.getIp(), command.getCommand(), t.getMessage());
    }

    private Predicate<Throwable> retryPredicate() {
        return throwable -> !(throwable instanceof NoNodeAvailable);
    }

    private Mono<ResponseEntity<String>> unauthorized(IriCommand command) {
        rejectedCommandCounter(command.getCommand()).increment();
        logger.info("Rejecting unauthorized command [{}] from IP [{}]", command.getCommand(), command.getIp());
        return Mono.just(createUnauthorizedResponse(command));
    }

    @ExceptionHandler(DecodingException.class)
    public ResponseEntity handleDecodingErrors(DecodingException dex) {
        rejectedCommandCounter("unreadable").increment();
        logger.warn("Decoding error: {}", dex.getMessage());
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to read HTTP message");
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity handleInvalidCommand(WebExchangeBindException wex) {
        rejectedCommandCounter("invalid").increment();

        String errorMessage = ExceptionHandlerUtil.extractValidationErrorMessage(wex);
        String validationTarget = logger.isDebugEnabled()
                ? String.valueOf(wex.getTarget())
                : StringUtils.abbreviate(String.valueOf(wex.getTarget()), 116);

        if (isUnauthorizedIriCommand(wex.getTarget())) {
            // return unauthorized instead of bad request like IRI does it
            logger.info("Received invalid unauthorized request [{}]. Returning 401...", validationTarget);
            logger.info(errorMessage);
            return createUnauthorizedResponse((IriCommand) wex.getTarget());
        } else if (isEmptyAttachToTangle(wex.getTarget())) {
            logger.info("Received empty AttachToTangle (POW check). Returning 400...");
            // exactly this response is needed for pow check
            return invalidCommandResponse();
        } else {
            logger.info("Validation failure: [{}]", validationTarget);
            logger.info(errorMessage);
            // this is not 100% compatible with IRI as the error message is much more detailed. If this
            // is a problem change to invalidCommandResponse
            throw ExceptionHandlerUtil.convertToResponseException(wex, errorMessage);
        }
    }

    @ExceptionHandler({RequestNotPermitted.class, NoNodeAvailable.class})
    public ResponseEntity handleRateLimitReached(RuntimeException re) {
        rejectedCommandCounter("limit").increment();
        logger.info("Too many requests. {}", re.getMessage());
        throw new ResponseStatusException(HttpStatus.TOO_MANY_REQUESTS, "Rate limit reached or all nodes are busy. Try again later.");
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(commandValidator);
    }

    private boolean isUnauthorizedIriCommand(Object input) {
        return input instanceof IriCommand && !commandChecker.isEnabled((IriCommand) input);
    }

    private boolean isEmptyAttachToTangle(Object input) {
        if (input instanceof AttachToTangle) {
            AttachToTangle att = (AttachToTangle) input;
            return StringUtils.isAllEmpty(att.getTrunkTransaction(), att.getBranchTransaction())
                    && CollectionUtils.isEmpty(att.getTrytes());
        } else {
            return false;
        }
    }

    private ResponseEntity<String> invalidCommandResponse() {
        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"error\":\"Invalid parameters\",\"duration\":0}");
    }

    private ResponseEntity<String> createUnauthorizedResponse(IriCommand command) {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"error\":\"COMMAND " + command.getCommand() + " is not available on this node\",\"duration\":0}");
    }

    private Counter rejectedCommandCounter(String command) {
        return rejectedCommandsCounter.computeIfAbsent(command, key -> metrics.createRejectedCommandCounter(command));
    }

    private Counter failedCommandCounter(String command, Throwable throwable) {
        String error = throwable.getClass().getSimpleName();
        return failedCommandsCounter.computeIfAbsent(Tuple.of(command, error), key -> metrics.createFailedCommandCounter(command, error));
    }

}
