/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@Setter
@ToString
public class AttachToTangle extends IriCommand {

    static final String COMMAND = "attachToTangle";

    public AttachToTangle() {
        super();
        this.setCommand(AttachToTangle.COMMAND);
    }

    @JsonInclude(value=NON_EMPTY) // important for POW check
    @NotEmpty
    private String trunkTransaction;

    @JsonInclude(value=NON_EMPTY) // important for POW check
    @NotEmpty
    private String branchTransaction;

    @JsonInclude(value=NON_NULL) // important for POW check
    @NotNull
    private Integer minWeightMagnitude;

    @JsonInclude(value=NON_EMPTY) // important for POW check
    @NotNull
    private List<String> trytes;

    @JsonIgnore
    @Override
    public boolean needsUpToDateNode() {
        return false;
    }

    @JsonIgnore
    @Override
    public boolean wantsUpToDateNode() {
        return false;
    }

    @JsonIgnore
    @Override
    public boolean wantsSameNodePerClient() {
        return false;
    }

    @JsonIgnore
    @Override
    public int getRateLimitCount() {
        return CollectionUtils.size(trytes);
    }
}
