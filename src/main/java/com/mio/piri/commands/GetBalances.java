/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mio.piri.commands.response.GetBalancesErrorHandler;
import com.mio.piri.commands.response.ResponseErrorHandler;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@Setter
@ToString
public class GetBalances extends IriCommand {

    @JsonIgnore
    private static final ResponseErrorHandler ERROR_HANDLER = new GetBalancesErrorHandler();

    // @NotEmpty can be empty for a new seed (light wallet)
    @NotNull
    @JsonInclude(value=NON_NULL)
    private List<String> addresses;

    @NotNull
    private int threshold;

    @JsonInclude(value=NON_NULL)
    private List<String> tips;

    @JsonIgnore
    @Override
    public int getRateLimitCount() {
        return CollectionUtils.size(addresses);
    }

    @JsonIgnore
    public ResponseErrorHandler getResponseErrorHandler() {
        return GetBalances.ERROR_HANDLER;
    }

}
