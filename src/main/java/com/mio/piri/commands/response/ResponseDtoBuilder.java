/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import lombok.Builder;

import java.util.LinkedHashSet;
import java.util.List;

public class ResponseDtoBuilder {

    @Builder(builderMethodName = "nodeInfoBuilder")
    public static NodeInfo buildNodeInfo(String appName, String appVersion, int availableProcessors, long jreFreeMemory, long jreTotalMemory, long jreMaxMemory, int neighbors, int tips, int transactionsToRequest, Integer latestMilestoneIndex, Integer latestSolidSubtangleMilestoneIndex, List<String> features) {
        NodeInfo nodeInfo = new NodeInfo();
        nodeInfo.setAppName(appName);
        nodeInfo.setAppVersion(appVersion);
        nodeInfo.setJreAvailableProcessors(availableProcessors);
        nodeInfo.setJreFreeMemory(jreFreeMemory);
        nodeInfo.setJreTotalMemory(jreTotalMemory);
        nodeInfo.setJreMaxMemory(jreMaxMemory);
        nodeInfo.setNeighbors(neighbors);
        nodeInfo.setTips(tips);
        nodeInfo.setTransactionsToRequest(transactionsToRequest);
        nodeInfo.setFeatures(features == null ? null : new LinkedHashSet<>(features));
        if (latestMilestoneIndex != null) {
            nodeInfo.setLatestMilestoneIndex(latestMilestoneIndex);
        }
        if (latestSolidSubtangleMilestoneIndex != null) {
            nodeInfo.setLatestSolidSubtangleMilestoneIndex(latestSolidSubtangleMilestoneIndex);
        }
        return nodeInfo;
    }

}
