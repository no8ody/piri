/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class GetBalancesErrorHandler extends DefaultErrorHandler {

    @Override
    public boolean isCriticalError(HttpStatus httpStatus, String body) {
        return super.isCriticalError(httpStatus, body)
                || (httpStatus == HttpStatus.BAD_REQUEST && StringUtils.containsIgnoreCase(body, "Tips are not consistent"));
    }

}
