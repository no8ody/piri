/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NodeInfo {

    public static final int INVALID_MILESTONE_INDEX = -1;
    public static final String REMOTE_POW = "RemotePOW";

    @JsonInclude(value=NON_EMPTY)
    private String appName;

    @JsonInclude(value=NON_EMPTY)
    private String appVersion;

    private int jreAvailableProcessors;

    private long jreFreeMemory;

    private String jreVersion;

    private long jreMaxMemory;

    private long jreTotalMemory;

    private String latestMilestone;

    private long latestMilestoneIndex = INVALID_MILESTONE_INDEX;

    private String latestSolidSubtangleMilestone;

    private long latestSolidSubtangleMilestoneIndex = INVALID_MILESTONE_INDEX;

    private long milestoneStartIndex;

    private int neighbors;

    private int packetsQueueSize;

    private long time;

    private int tips;

    private int transactionsToRequest;

    @JsonInclude(value=NON_NULL)
    @JsonDeserialize(as=LinkedHashSet.class) // preserves order
    private Set<String> features;

    private String coordinatorAddress;

    private long duration;

    @JsonIgnore
    public boolean isSynced() {
        // milestones can be off by one for a short period of time
        return hasValidLatestMilestoneIndex() && latestSolidSubtangleMilestoneIndex + 1 >= latestMilestoneIndex;
    }

    public boolean supportsPow() {
        return features != null && features.contains(REMOTE_POW);
    }

    /**
     * Removes a features or does nothing if the feature is not present.
     * @param feature The feature to be removed.
     */
    public void removeFeature(String feature) {
        if (features != null) {
            features.remove(feature);
        }
    }

    /**
     * Adds a feature.
     * @param feature The feature to be added. Implementation makes sure that no duplicates are added.
     */
    public void addFeature(String feature) {
        if (features == null) {
            features = new LinkedHashSet<>();
        }
        features.add(feature);
    }

    @JsonIgnore
    private boolean hasValidLatestMilestoneIndex() {
        return latestMilestoneIndex > 0;
    }

}

