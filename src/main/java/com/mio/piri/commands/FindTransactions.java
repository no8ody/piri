/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@Setter
@ToString
public class FindTransactions extends IriCommand {

    @JsonInclude(value=NON_NULL)
    private List<String> addresses;

    @JsonInclude(value=NON_NULL)
    private List<String> bundles; // can be empty

    @JsonInclude(value=NON_NULL)
    private List<String> tags;

    @JsonInclude(value=NON_NULL)
    private List<String> approvees;

    @JsonIgnore
    @Override
    public int getRateLimitCount() {
        // the result only contains the intersection of these lists but IRI queries them all before intersecting.
        // the effort is retrieving all of them plus intersection, therefore we sum them.
        return CollectionUtils.size(addresses) + CollectionUtils.size(bundles) + CollectionUtils.size(tags) + CollectionUtils.size(approvees);
    }

}
