/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.process;

import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.nodes.Node;
import com.mio.piri.util.JsonUtil;
import io.vavr.collection.Seq;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.time.Duration;
import java.util.Set;
import java.util.stream.Collectors;

import static com.mio.piri.commands.response.NodeInfo.REMOTE_POW;

public class GetNodeInfoResponsePostProcessor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final boolean powAvailable;
    private final boolean accumulateNodeInfo;
    private final NodeInfo globalNodeInfo;
    private long validUntil;
    private boolean validGlobalNodeInfo;
    private final Duration cacheTimeout;
    private final NodeInfoOverride overrides;

    public GetNodeInfoResponsePostProcessor(boolean powAvailable, boolean accumulateNodeInfo, Duration cacheTimeout, NodeInfoOverride overrides) {
        this.powAvailable = powAvailable;
        this.accumulateNodeInfo = accumulateNodeInfo;
        this.cacheTimeout = cacheTimeout;
        this.globalNodeInfo = new NodeInfo();
        this.validUntil = System.currentTimeMillis();
        this.overrides = overrides;
    }

    public boolean accept(IriCommand request, ResponseEntity<String> response) {
        return request instanceof GetNodeInfo && response != null && response.getStatusCode().is2xxSuccessful();
    }

    public String postProcess(String body) {
        NodeInfo nodeInfo = JsonUtil.fromJson(body, NodeInfo.class);
        setGlobalProperties(nodeInfo);
        correctPowFeature(nodeInfo);
        return JsonUtil.toJson(nodeInfo);
    }

    public NodeInfo updateGlobalNodeInfo(Seq<Node> nodes) {

        if (accumulateNodeInfo && System.currentTimeMillis() >= validUntil) { // only update if outdated and multiple nodes are available
            Set<NodeInfo> syncedNodeInfos = nodes.map(Node::getLatestNodeInfo).filter(NodeInfo::isSynced).collect(Collectors.toSet());
            // prevent too many recalculation attempts
            if (syncedNodeInfos.size() > 1) {

                DescriptiveStatistics neighbors = new DescriptiveStatistics();
                DescriptiveStatistics processors = new DescriptiveStatistics();
                DescriptiveStatistics freeMem = new DescriptiveStatistics();
                DescriptiveStatistics maxMem = new DescriptiveStatistics();
                DescriptiveStatistics totalMem = new DescriptiveStatistics();
                DescriptiveStatistics tips = new DescriptiveStatistics();
                DescriptiveStatistics txToRequest = new DescriptiveStatistics();

                syncedNodeInfos.forEach(ni -> {
                    processors.addValue(ni.getJreAvailableProcessors());
                    freeMem.addValue(ni.getJreFreeMemory());
                    maxMem.addValue(ni.getJreMaxMemory());
                    totalMem.addValue(ni.getJreTotalMemory());
                    neighbors.addValue(ni.getNeighbors());
                    tips.addValue(ni.getTips());
                    txToRequest.addValue(ni.getTransactionsToRequest());
                });

                if (logger.isDebugEnabled()) {
                    logger.debug("Calculating aggregated node information...");
                    logger.debug("neighbors: [{}]. Values: {}", neighbors.getPercentile(50), neighbors.getValues());
                    logger.debug("processors: [{}]. Values: {}", processors.getPercentile(50), processors.getValues());
                    logger.debug("freeMem: [{}]. Values: {}", freeMem.getPercentile(50), freeMem.getValues());
                    logger.debug("maxMem: [{}]. Values: {}", maxMem.getPercentile(50), maxMem.getValues());
                    logger.debug("totalMem: [{}]. Values: {}", totalMem.getPercentile(50), totalMem.getValues());
                    logger.debug("tips: [{}]. Values: {}", tips.getPercentile(50), tips.getValues());
                    logger.debug("txToRequest: [{}]. Values: {}", txToRequest.getPercentile(50), txToRequest.getValues());
                }

                // we need to round because in case of even number of values the median is between two values
                globalNodeInfo.setJreAvailableProcessors((int) Math.round(processors.getPercentile(50)));
                globalNodeInfo.setJreFreeMemory(Math.round(freeMem.getPercentile(50)));
                globalNodeInfo.setJreMaxMemory(Math.round(maxMem.getPercentile(50)));
                globalNodeInfo.setJreTotalMemory(Math.round(totalMem.getPercentile(50)));
                globalNodeInfo.setTips((int) Math.round(tips.getPercentile(50)));
                globalNodeInfo.setNeighbors((int) Math.floor(neighbors.getPercentile(50)));
                globalNodeInfo.setTransactionsToRequest((int) Math.floor(txToRequest.getPercentile(50)));

                validGlobalNodeInfo = true; // use global node info
            } else {
                logger.debug("Could not calculate global node info data. Only [{}] synced nodes available.", syncedNodeInfos.size());
                validGlobalNodeInfo = false; // don't use global node info
            }
            validUntil = System.currentTimeMillis() + cacheTimeout.toMillis(); // next recalculation
        }

        return globalNodeInfo;
    }

    private void setGlobalProperties(NodeInfo nodeInfo) {
        // only manipulate getNodeInfo if valid global data is available
        if (accumulateNodeInfo && validGlobalNodeInfo) {

            nodeInfo.setAppName(StringUtils.defaultIfBlank(overrides.getAppName(), nodeInfo.getAppName()));
            nodeInfo.setAppVersion(StringUtils.defaultIfBlank(overrides.getAppVersion(), nodeInfo.getAppVersion()));

            nodeInfo.setJreAvailableProcessors(globalNodeInfo.getJreAvailableProcessors());
            nodeInfo.setJreTotalMemory(globalNodeInfo.getJreTotalMemory());
            nodeInfo.setJreFreeMemory(globalNodeInfo.getJreFreeMemory());
            nodeInfo.setJreMaxMemory(globalNodeInfo.getJreMaxMemory());
            nodeInfo.setNeighbors(globalNodeInfo.getNeighbors());
            nodeInfo.setTransactionsToRequest(globalNodeInfo.getTransactionsToRequest());
            nodeInfo.setTips(globalNodeInfo.getTips());

        }
    }

    private void correctPowFeature(NodeInfo nodeInfo) {
        if (!powAvailable) {
            nodeInfo.removeFeature(REMOTE_POW);
        } else {
            nodeInfo.addFeature(REMOTE_POW);
        }
    }

}
