/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mio.piri;

import com.mio.piri.actuator.UptimeHealthIndicator;
import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.process.GetNodeInfoResponsePostProcessor;
import com.mio.piri.commands.process.NodeInfoOverride;
import com.mio.piri.metrics.CommandStatistics;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.nodes.*;
import com.mio.piri.nodes.selection.*;
import com.mio.piri.service.CommandChecker;
import com.mio.piri.service.validation.*;
import com.mio.piri.tolerance.CircuitBreakerFactory;
import com.mio.piri.tolerance.RateLimitOperations;
import com.mio.piri.tolerance.RateLimiterFactory;
import com.mio.piri.util.ClientSessionExtractor;
import com.mio.piri.util.UrlValidator;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Configuration
public class PiriConfiguration {

    @Bean
    public GetNodeInfoResponsePostProcessor getNodeInfoResponsePostProcessor(CommandChecker commandChecker, Environment env) {
        Boolean accumulateNodeInfo = env.getProperty("piri.getNodeInfo.response.accumulate", Boolean.class, true);
        Duration cacheTimeout = parseDuration(env.getProperty("piri.getNodeInfo.response.accumulate.cache", "1m"));
        String appName = env.getProperty("piri.getNodeInfo.override.appName");
        String appVersion = env.getProperty("piri.getNodeInfo.override.appVersion");
        NodeInfoOverride overrides = new NodeInfoOverride(appName, appVersion);
        return new GetNodeInfoResponsePostProcessor(commandChecker.isEnabled(new AttachToTangle()), accumulateNodeInfo, cacheTimeout, overrides);
    }

    @Bean
    public CommandChecker commandChecker(Environment env) {
        return new CommandChecker(env);
    }

    @Bean
    public WebClientCustomizer iriWebClientCustomizer() {
        return new NodeClientCustomizer();
    }

    @Bean
    public MeterFactory commandMetrics(MeterRegistry registry) {
        return new MeterFactory(registry);
    }

    @Bean
    public CircuitBreakerFactory circuitBreakerFactory(MeterFactory meterFactory, Environment env) {
        return new CircuitBreakerFactory(meterFactory, env);
    }

    @Bean
    public RateLimiterFactory rateLimiterFactory(Environment env, MeterFactory meterFactory) {
        return new RateLimiterFactory(env, meterFactory);
    }

    @Bean
    public RateLimitOperations rateLimitOperations(Environment env, RateLimiterFactory rateLimiterFactory) {
        return new RateLimitOperations(env, rateLimiterFactory);
    }

    @Bean
    public NodeSelector nodeSelector(Environment env, NodeRegistry nodeRegistry, SessionSelectionStrategy sessionStrategy,
                                     RandomSelectionStrategy randomStrategy) {
        NodeSelector nodeSelector = new NodeSelector(sessionStrategy, randomStrategy, nodeRegistry);
        Boolean enabled = env.getProperty("piri.session.binding.enabled", Boolean.class, true);
        nodeSelector.setSessionBindingEnabled(enabled);
        return nodeSelector;
    }

    @Bean
    public UrlValidator urlValidator() {
        return new UrlValidator();
    }

    @Bean
    public SessionSelectionStrategy sessionSelectionStrategy() {
        return new SessionSelectionStrategy();
    }

    @Bean
    public RandomSelectionStrategy randomSelectionStrategy() {
        return new RandomSelectionStrategy();
    }

    @Bean
    public SelectionUtility selectionUtility(Environment env) {
        SelectionUtility selectionUtility = new SelectionUtility();
        boolean retryOnSameNode = env.getProperty("piri.retry.successive-on-same-node", Boolean.class, false);
        int maxRetries = env.getProperty("piri.retry.max-per-node", Integer.class, 1);
        selectionUtility.setSuccessiveRetryOnSameNode(retryOnSameNode);
        selectionUtility.setMaxRetriesPerNode(maxRetries);
        return selectionUtility;
    }

    @Bean
    @DependsOn("piriInitialization")
    public NodeRegistry nodeRegistry(UrlValidator urlValidator, NodeClient nodeClient, MeterFactory meterFactory, CircuitBreakerFactory circuitBreakerFactory, NodesRepository nodesRepository, Environment env) {
        return new NodeRegistry(urlValidator, nodeClient, meterFactory, circuitBreakerFactory, nodesRepository, env);
    }

    @Bean
    public CommandStatistics commandStatistics() {
        return new CommandStatistics();
    }

    @Bean
    public DelegatingValidator commandValidator(Environment env) {
        MinMaxValidator minMaxValidator = new MinMaxValidator();
        return new DelegatingValidator(IriCommand.class,
                new AttachToTangleValidator(env, minMaxValidator),
                new GetTransactionsToApproveValidator(env, minMaxValidator),
                new GetTrytesValidator(env, minMaxValidator),
                new FindTransactionsValidator(env, minMaxValidator),
                new WereAddressesSpentFromValidator(env, minMaxValidator),
                new GetInclusionStatesValidator(env, minMaxValidator),
                new CheckConsistencyValidator(env, minMaxValidator),
                new GetBalancesValidator(env, minMaxValidator),
                new StoreTransactionsValidator(env, minMaxValidator),
                new BroadcastTransactionsValidator(env, minMaxValidator)
        );
    }

    @Bean
    public SessionBinding nodeToClientMapper(Environment env, MeterFactory meterFactory) {
        Duration clientIdleTime = parseDuration(env.getProperty("piri.session.binding.idle.time", "1m"));
        Duration cleanInterval = DurationStyle.detectAndParse(env.getProperty("piri.session.binding.clean.interval", "1h"));
        SessionBinding mapper = new SessionBinding(clientIdleTime, cleanInterval);
        meterFactory.createClientMappingsGauge(mapper);
        return mapper;
    }

    @Bean
    public NodesRepository nodesRepository(Environment env, HashGenerator hashGenerator) {
        String fileName = env.getProperty("piri.nodes.persistence.file");
        String password = env.getProperty("piri.nodes.persistence.pass");
        boolean compressed = env.getProperty("piri.nodes.persistence.compress", Boolean.class, false);
        return new NodesRepository(fileName, password, compressed, hashGenerator);
    }

    @Bean
    public ClientSessionExtractor clientSessionExtractor(Environment env) {
        return new ClientSessionExtractor(env);
    }

    @Bean
    public NodeClient nodeClient(WebClient.Builder builder) {
        NodeClient nodeClient = new NodeClient();
        nodeClient.setWebClient(builder.build());
        return nodeClient;
    }

    @Bean
    public UptimeHealthIndicator uptimeHealthIndicator(MeterRegistry meterRegistry) {
        return new UptimeHealthIndicator(meterRegistry);
    }

    @Bean
    public RandomKeyGenerator randomKeyGenerator() {
        return new RandomKeyGenerator();
    }

    @Bean
    public HashGenerator hashGenerator() {
        return new HashGenerator();
    }

    // The following bean is only used for initializing the system. Maybe there is a better way to do this...
    @Bean
    public PiriInitialization piriInitialization(Environment env) {
        return new PiriInitialization(env);
    }

    private Duration parseDuration(String duration) {
        return DurationStyle.detectAndParse(duration);
    }

}
