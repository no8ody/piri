/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import io.vavr.control.Option;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class NodesRepository {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final HashGenerator hashGenerator;

    private final MVStore store;

    private final MVMap<String, String[]> registrations;

    public NodesRepository(String fileName, String password, boolean compressed, HashGenerator hashGenerator) {
        this.hashGenerator = hashGenerator;
        logger.debug("Opening file store [{}]...", fileName);
        if (StringUtils.isBlank(fileName)) {
            logger.warn("No file name specified. Persistence disabled.");
        }
        MVStore.Builder storeBuilder = new MVStore.Builder().fileName(fileName);
        if (compressed) {
            logger.debug("Compression enabled.");
            storeBuilder.compress();
        }
        if (StringUtils.isNotBlank(password)) {
            logger.debug("Encryption enabled.");
            storeBuilder.encryptionKey(password.toCharArray());
        }
        store = storeBuilder.open();
        logger.debug("Getting registrations from file store...");
        registrations = store.openMap("registrations");
        logger.info("Opened data base: {}", dbToString());
        try {
            if (!registrations.isEmpty()) {
                String firstKey = registrations.firstKey();
                if (registrations.get(firstKey).length < 3) {
                    String msg = "Unsupported database version detected. Delete the current database file and restart.";
                    logger.error(msg);
                    throw new IllegalStateException(msg);
                } else if (registrations.get(firstKey).length == 3) {
                    // old compatible db version detected
                    logger.warn("Old database version detected. Migrating to current version...");
                    migrateEntriesToDbVersion4(registrations);
                } // else everything is fine
            }
        } catch (Exception e) {
            logger.error("Fatal error trying to migrate persisted nodes to another db version. " +
                    "This is either a severe bug or the db is corrupt.");
            store.closeImmediately();
            throw e;
        }

        logger.info("Loaded [{}] registered nodes.", registrations.size());
    }

    private String dbToString() {
        StringJoiner sj = new StringJoiner(", ", "[", "]");
        registrations.forEach((k, v) -> sj.add(k + ": " + Arrays.toString(v)));
        return sj.toString();
    }

    // added pow field
    @Deprecated
    private void migrateEntriesFromVersion1To2(MVMap<String, String[]> registrations) {
        HashSet<String> keys = new HashSet<>(registrations.keySet()); // copy
        keys.forEach(
                key -> {
                    String[] old = registrations.get(key);
                    String name = old[0];
                    String url = old[1];
                    registrations.replace(key, entryOf(name, url, false, null));
                }
        );
    }

    @Deprecated
    // replaced password with hashed password
    private void migrateEntriesFromVersion2To3(MVMap<String, String[]> registrations) {
        final char[] OLD_KEY_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ9".toCharArray();
        HashSet<String> keys = new HashSet<>(registrations.keySet()); // copy
        keys.forEach(
                key -> {
                    if (StringUtils.containsOnly(key, OLD_KEY_CHARACTERS)) {
                        // we found an old record
                        String[] record = registrations.get(key);
                        registrations.remove(key);
                        registrations.put(hashGenerator.sha256(key), record);
                        logger.info("Database version 2 detected. Migrating entry for node [{}/{}] to version 3.", record[0], record[1]);
                    }
                }
        );
    }

    // add address field (and optionally pow field)
    private void migrateEntriesToDbVersion4(MVMap<String, String[]> registrations) {
        HashSet<String> keys = new HashSet<>(registrations.keySet()); // copy
        keys.forEach(
                key -> {
                    String[] record = registrations.get(key);
                    if (record.length < 4) {
                        logger.info("Old database version detected. Migrating entry for node {} to current version.", Arrays.toString(record));
                        String name = record[0];
                        String url = record[1];
                        boolean pow = record.length > 2 && BooleanUtils.toBoolean(record[2]);
                        registrations.replace(key, entryOf(name, url, pow, null));
                    } else {
                        logger.debug("Db entry is already v4. Not migrating entry: {}", Arrays.toString(record));
                    }
                }
        );
    }

    @PreDestroy
    public void saveToFile() {
        logger.info("Saving [{}] registered nodes to disk...", registrations.size());
        store.close();
        logger.debug("File store closed.");
    }

    public void addNode(String key, String nodeName, String url, boolean pow, String address) {
        if (registrations.containsKey(key)) {
            logger.error("Error adding node with name [{}]. Key already in map.", nodeName);
            throw new IllegalStateException("Could not add node [" + nodeName + "]. Key already exists.");
        }
        logger.info("Storing node with name [{}] and url [{}]. Pow enabled: [{}].", nodeName, url, pow);
        registrations.put(key, entryOf(nodeName, url, pow, address));
    }

    public Option<String> getNodeName(String key) {
        return key == null ? Option.none() : Option.of(getNodeProperties(key).map(nodeProperties -> nodeProperties.name).getOrNull());
    }

    private Option<NodeProperties> getNodeProperties(String key) {
        return Option.of(toNodeProperties(key, registrations.get(key)));
    }

    public Option<NodeProperties> removeNode(String key) {
        String[] remove = registrations.remove(key);
        return Option.of(toNodeProperties(key, remove));
    }

    public Option<NodeProperties> removeNodeByName(String name) {
        Set<Map.Entry<String, String[]>> allNodes = new HashSet<>(registrations.entrySet()); // copy
        List<String> toBeRemoved = allNodes.stream()
                .filter(entry -> StringUtils.equals(name, entry.getValue()[0]))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        logger.debug("Removing [{}] entries for name [{}].", toBeRemoved.size(), name);
        if (toBeRemoved.size() > 1) {
            logger.error("More than one node found for name [{}]. This should never happen.", name);
            logger.error("Removing nodes with the following keys: {}", toBeRemoved);
        }
        Option<NodeProperties> removed = toBeRemoved.isEmpty() ? Option.none() : getNodeProperties(toBeRemoved.get(0));
        toBeRemoved.forEach(registrations::remove);
        return removed;
    }

    public Collection<NodeProperties> getAllNodes() {
        Set<Map.Entry<String, String[]>> allNodes = new HashSet<>(registrations.entrySet()); // copy
        return allNodes.stream().map(e -> toNodeProperties(e.getKey(), e.getValue())).collect(Collectors.toSet());
    }

    private String[] entryOf(String name, String url, boolean pow, String address) {
        Objects.requireNonNull(name, "Node name must not be null.");
        Objects.requireNonNull(url, "Node url must not be null.");
        return new String[]{name, url, Boolean.toString(pow), address};
    }

    private NodeProperties toNodeProperties(String key, String[] entry) {
        return entry == null || entry.length < 2
                ? null
                : new NodeProperties(entry[0], entry[1], hashGenerator.publicKey(key), Boolean.parseBoolean(entry[2]), entry[3]);
    }

    public boolean containsKey(String hashedPassword) {
        return registrations.containsKey(hashedPassword);
    }

    public boolean containsKeyStartingWith(String nodeKey) {
        HashSet<String> keys = new HashSet<>(registrations.keySet()); // copy
        return keys.stream().anyMatch(key -> StringUtils.startsWith(key, nodeKey));
    }
}
