/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import org.apache.commons.lang3.RandomStringUtils;

import java.security.SecureRandom;

public class RandomKeyGenerator {

    private final SecureRandom random;
    private final static char[] RANDOM_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ9".toCharArray();

    public RandomKeyGenerator() {
        random = new SecureRandom();
    }

    public String generateNodePassword() {
        return RandomStringUtils.random(81, 0, RANDOM_CHARS.length, false, false, RANDOM_CHARS, random);
    }


}
