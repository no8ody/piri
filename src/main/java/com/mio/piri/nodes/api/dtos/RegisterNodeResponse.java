/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Response you receive after successfully registering a new node.")
public class RegisterNodeResponse {

    @ApiModelProperty(notes = "${api.model.register-node-response.name.notes}")
    private String name;

    @ApiModelProperty(notes = "${api.model.register-node-response.url.notes}", position = 1)
    private String url;

    @ApiModelProperty(notes = "${api.model.register-node-response.password.notes}",
            example = "DANWXQHUKKVWQYOUDWPRLRHXEYDQLCGUTQRDXTMLQDYPRYBIXJKWBAZQPOOORMMIYAFAJIRIAXRPLDOKR",
            position = 2)
    @ToString.Exclude
    private String password;

    @ApiModelProperty(notes = "${api.model.register-node-response.hash.notes}", position = 3)
    private String hash;

    @ApiModelProperty(notes = "${api.model.register-node-response.address.notes}", position = 4)
    private String address;

}
