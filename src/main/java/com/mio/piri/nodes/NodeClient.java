/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.IriCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeoutException;

import static java.util.logging.Level.FINE;
import static reactor.core.publisher.SignalType.ON_NEXT;

public class NodeClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private WebClient webClient;

    public Mono<ResponseEntity<String>> post(Node node, IriCommand command) {
        return exchange(node, command)
                .doOnError(e -> logger.warn("Command [{}] on Node [{}] failed: {}", command.getCommand(), node.getName(), e instanceof TimeoutException ? "Timeout" : e.toString()));
    }

    public <T> Mono<T> retrieve(Node node, IriCommand command, Class<T> responseType) {
        return webClient
                .post()
                .uri(node.getUri())
                .headers(node.getHeadersConsumer())
                .bodyValue(command)
                .retrieve()
                .bodyToMono(responseType)
                .timeout(node.getTimeout(command))
                .log(logger.getName(), FINE, ON_NEXT)
                ;
    }

    public Mono<ResponseEntity<String>> exchange(Node node, IriCommand command) {
        return webClient
                .post()
                .uri(node.getUri())
                .headers(node.getHeadersConsumer())
                .bodyValue(command)
                .exchange()
                .flatMap(response -> response.toEntity(String.class))
                .timeout(node.getTimeout(command))
                .log(logger.getName(), FINE, ON_NEXT)
                ;
    }

    public void setWebClient(WebClient webClient) {
        this.webClient = webClient;
    }

}
