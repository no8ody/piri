/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.IriCommands;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.util.JsonUtil;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.micrometer.core.instrument.Timer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.regex.Pattern;

@ToString(of = {"name", "url"})
public class IriNode implements Node {

    private final Logger logger = LoggerFactory.getLogger(IriNode.class);

    protected final NodeInfo info = ResponseDtoBuilder.nodeInfoBuilder().appName("IRI").build();

    private final AtomicLong latestKnownGlobalMilestone = new AtomicLong();

    static Pattern VERSION_WHITELIST;
    static Pattern VERSION_BLACKLIST;

    @Getter
    protected final String name;

    @Getter
    protected final String key;

    @Getter
    protected final String url; // TODO remove me. URI is better.

    @Getter
    private final URI uri;

    @Getter
    @Setter
    private String donationAddress;

    @Getter
    private final boolean powEnabled;

    // if pow is enabled we check from time to time if the information is (still) correct.
    // that means it is possible to have a node that has pow enabled but doesn't support it.
    @Getter
    protected boolean powAvailable = true;

    @NotNull
    protected final NodeClient nodeClient;

    @NotNull
    private final MeterFactory metrics;

    @NotNull
    protected final CircuitBreaker circuitBreaker;

    @Setter
    @Getter
    private Consumer<HttpHeaders> headersConsumer = httpHeaders -> {};

    private boolean available = true;

    private final Map<String, Timer> timers = new ConcurrentHashMap<>(); // needs to thread safe because of possible concurrent timer creation

    public IriNode(String name, String url, String key, boolean pow, NodeClient nodeClient, MeterFactory metrics, CircuitBreaker circuitBreaker) {
        this.name = name;
        this.url = url;
        this.key = key;
        this.powEnabled = pow;
        this.nodeClient = nodeClient;
        this.metrics = metrics;
        this.circuitBreaker = circuitBreaker;
        try {
            this.uri = new URI(url);
        } catch (URISyntaxException e) {
            logger.error(e.toString(), e);
            throw new IllegalArgumentException(e);
        }
    }

    public Mono<ResponseEntity<String>> call(final IriCommand command) {
        final Timer timer = commandTimer(command);
        final Timer.Sample sample = metrics.sample();
        // doFinally is executed after mono completion. Therefore execution time might be slightly lower.
        // if we need shorter timings use onNext AND onError (and maybe onCancel).
        return post(command).doFinally(x -> sample.stop(timer));
    }

    private Mono<ResponseEntity<String>> post(IriCommand command) {
        if (command instanceof GetNodeInfo) {
            return getNodeInfo((GetNodeInfo) command);
        } else {
            return standardCall(command);
        }
    }

    private Mono<ResponseEntity<String>> standardCall(IriCommand command) {
        return nodeClient
                .post(this, command)
                .transform(CircuitBreakerOperator.of(circuitBreaker))
                ;
    }

    private Mono<ResponseEntity<String>> getNodeInfo(GetNodeInfo getNodeInfo) {
        return standardCall(getNodeInfo)
                .doOnNext(responseEntity -> {
                    if (responseEntity.getStatusCode().is2xxSuccessful()) {
                        try {
                            // update node info as we already have all information here...
                            updateWithHealthCheckResult(JsonUtil.fromJson(responseEntity.getBody(), NodeInfo.class));
                        } catch (Exception e) {
                            // catch, log and ignore
                            logger.error("Error on health update for node [{}] with node info response [{}]: {}.",
                                    getName(), responseEntity, e.toString());
                        }
                    }
                });
    }

    public Mono<NodeInfo> queryNodeHealth() {
        return nodeClient.retrieve(this, new GetNodeInfo(), NodeInfo.class)
                .doOnNext(this::updateWithHealthCheckResult)
                .transform(CircuitBreakerOperator.of(circuitBreaker)) // might throw => before onErrorResume
                .onErrorResume(t -> Mono.just(this.updateWithHealthCheckError(t)));
    }

    private boolean isCallPermitted() {
        boolean callPermitted = circuitBreaker.isCallPermitted();
        if (!callPermitted && logger.isDebugEnabled()) {
            logger.debug("Node [{}]: call not permitted. Active: [{}]. Circuit: [{}]", name, available, circuitBreaker.getState());
        }
        return callPermitted;
    }

    private Timer commandTimer(IriCommand command) {
        return timers.computeIfAbsent(command.getCommand(), key -> metrics.createCommandTimer(command.getCommand(), name, this.key));
    }

    // needed here until different node types set their timeout
    public Duration getTimeout(IriCommand command) {
        return IriCommands.getTimeout(command.getCommand());
    }

    private NodeInfo updateWithHealthCheckError(String message) {
//        this.info.setLatestMilestoneIndex(NodeInfo.INVALID_MILESTONE_INDEX);
//        this.info.setLatestSolidSubtangleMilestoneIndex(NodeInfo.INVALID_MILESTONE_INDEX);
        if (isAvailable() || logger.isDebugEnabled()) {
            setUnavailable(message);
        }
        return this.info;
    }

    NodeInfo updateWithHealthCheckError(Throwable t) {
        return this.updateWithHealthCheckError(getHealthCheckErrorMessage(t));
    }

    /**
     * Uses the provided node info to update this nodes health status if necessary.
     * @param currentNodeInfo Currently queried from the node via getNodeInfo.
     */
    public void updateWithHealthCheckResult(final NodeInfo currentNodeInfo) {
        logger.debug("Update [{}] with health check result: [{}]", name, currentNodeInfo);
        this.latestKnownGlobalMilestone.set(Math.max(currentNodeInfo.getLatestMilestoneIndex(), latestKnownGlobalMilestone.get()));
        this.info.setAppName(currentNodeInfo.getAppName());
        String appVersion = currentNodeInfo.getAppVersion();
        this.info.setAppVersion(appVersion);
        this.info.setNeighbors(currentNodeInfo.getNeighbors());
        this.info.setJreAvailableProcessors(currentNodeInfo.getJreAvailableProcessors());
        this.info.setJreMaxMemory(currentNodeInfo.getJreMaxMemory());
        this.info.setJreFreeMemory(currentNodeInfo.getJreFreeMemory());
        this.info.setJreTotalMemory(currentNodeInfo.getJreTotalMemory());
        this.info.setTransactionsToRequest(currentNodeInfo.getTransactionsToRequest());
        this.info.setTips(currentNodeInfo.getTips());
        this.info.setLatestMilestoneIndex(currentNodeInfo.getLatestMilestoneIndex());
        this.info.setLatestSolidSubtangleMilestoneIndex(currentNodeInfo.getLatestSolidSubtangleMilestoneIndex());

        updatePowAvailability(currentNodeInfo.supportsPow());

        if (!isVersionSupported(appVersion) || isVersionBlacklisted(appVersion)) {
            // disable or leave disabled if version is not supported
            if (isAvailable()) {
                setUnavailable("Unsupported IRI version: " + appVersion);
            }
        } else if (!isAvailable()) {
            setAvailable();
        }
    }

    /**
     * Supported means that that the whitelist is either not set (null) or it contains the version.
     * @param appVersion IRI version string.
     * @return <code>true</code> if the version is supported.
     */
    private boolean isVersionSupported(String appVersion) {
        return VERSION_WHITELIST == null || (appVersion != null && VERSION_WHITELIST.matcher(appVersion).matches());
    }

    private boolean isVersionBlacklisted(String appVersion) {
        return VERSION_BLACKLIST != null
                && VERSION_BLACKLIST.matcher(StringUtils.defaultString(appVersion, "null")).matches();
    }

    void updatePowAvailability(boolean available) {
        logger.debug("POW check for node [{}]. POW: [{}]", name, available);
        if (powAvailable != available) {
            powAvailable = available;
            logger.info("Changed POW availability for node [{}] to [{}].", name, powAvailable);
        }
    }

    public boolean isAvailable() {
        return available;
    }

    protected void setAvailable() {
        logger.info("Setting node [{}/{}] available.", name, url);
        this.available = true;
    }

    void setUnavailable(String reason) {
        logger.info("Setting node [{}/{}] unavailable. Reason: [{}].", name, url, reason);
        this.available = false;
    }

    /**
     * Check how up to date this node is.
     * @return the number of milestones the node (local solid milestone) is behind the newest known (global) milestone.
     */
    public long getDelay() {
        // the latest known global milestone among all nodes is the most current one.
        // the latest solid local milestone is the latest consistent state the node is able to handle.
        // the delay therefore is the difference between the latest global milestone and the local one.
        // the local node info object shouldn't be much older than the global information for this check to work.
        return this.latestKnownGlobalMilestone.get() - this.info.getLatestSolidSubtangleMilestoneIndex();
    }

    public boolean isCallPossible() {
        return isCallPermitted() && isAvailable();
    }

    public boolean isSupported(IriCommand command) {
        // currently we only check pow
        return (powEnabled && powAvailable) || !(command instanceof AttachToTangle);
    }

    String getHealthCheckErrorMessage(Throwable t) {
        if (t instanceof Exception) {
            return t instanceof TimeoutException ? "Timeout" : t.getMessage(); // timeout exception is too verbose
        } else {
            logger.error(t.toString(), t);
            return t.toString();
        }
    }

    // move the following methods into an own class as soon as more properties than node info properties are necessary

    public void updateLatestKnownMilestone(long latestKnownMilestone) {
        this.latestKnownGlobalMilestone.set(latestKnownMilestone);

    }

    public NodeInfo getLatestNodeInfo() {
        return this.info;
    }

    public long getLatestMilestoneIndex() {
        return this.info.getLatestMilestoneIndex();
    }

    public boolean isSynced() {
        return this.info.isSynced();
    }

}
