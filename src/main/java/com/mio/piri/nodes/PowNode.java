/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import reactor.core.publisher.Mono;

import java.time.Duration;

import static com.mio.piri.commands.response.NodeInfo.REMOTE_POW;

// TODO fix hierarchy Pow node is NOT an IRI node
public class PowNode extends IriNode implements Node {

    public static Duration POW_TIMEOUT = Duration.ofSeconds(5);
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public PowNode(String name, String url, String key, NodeClient nodeClient, MeterFactory metrics, CircuitBreaker circuitBreaker) {
        super(name, url, key,true, nodeClient, metrics, circuitBreaker);
        info.setAppName("POW Node");
        info.setAppVersion(null);
        info.addFeature(REMOTE_POW);
        info.setNeighbors(0);
    }

    @SuppressWarnings("unused") // false inspection warning in intellij
    @Override
    public Duration getTimeout(IriCommand command) {
        logger.debug("Return pow timeout [{}] for command [{}].", POW_TIMEOUT, command.getCommand());
        return POW_TIMEOUT;
    }

    @SuppressWarnings("unused") // false inspection warning in intellij
    @Override
    public Mono<NodeInfo> queryNodeHealth() {
        logger.debug("Query node health for node [{}]", name);
        return nodeClient.exchange(this, new AttachToTangle())
                .timeout(Duration.ofSeconds(3)) // maximum timeout for health check
                .doOnNext(response -> {
                    if (response.getStatusCode().isError() && response.getStatusCode() != HttpStatus.BAD_REQUEST) { // HTTP 400 is OK
                        this.updateWithHealthCheckError(String.format("Response status [%s] with body [%s].", response.getStatusCode(), response.getBody()));
                    } else {
                        this.updateWithHealthCheckSuccess();
                    }
                })
                .transform(CircuitBreakerOperator.of(circuitBreaker)) // might throw => therefore before onErrorResume
                // attachToTangle might throw. resume with empty mono otherwise error gets replayed.
                .onErrorResume(t -> {
                    Mono.just(updateWithHealthCheckError(getHealthCheckErrorMessage(t)));
                    return Mono.empty();
                })
                // return dummy node info as an result
                .then(Mono.just(info))
                ;
    }

    private NodeInfo updateWithHealthCheckError(String message) {
        if (isAvailable() || logger.isDebugEnabled()) {
            setUnavailable(message);
        }
        return info;
    }

    private void updateWithHealthCheckSuccess() {
        logger.debug("[{}] Health check successful.", name);
        if (!isAvailable()) {
            setAvailable();
        }
    }

    @Override
    public boolean isSupported(IriCommand command) {
        // we support pow only
        return command instanceof AttachToTangle;
    }

}
