/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.Node;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class SessionSelectionStrategy {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SessionBinding sessionBinding;

    @Autowired
    private SelectionUtility selectionUtility;

    public Node selectNode(IriCommand command, Map<String, Node> nodes) {
        if (!command.wantsSameNodePerClient()) {
            logger.warn("Session strategy called with command that doesn't bind to session: [{}].", command);
        }
        return getNodeMappedToClient(command, nodes).getOrNull();
    }

    private Option<Node> getNodeMappedToClient(IriCommand command, Map<String, Node> nodes) {
        Option<Node> node = sessionBinding.getNodeForClient(command.getSessionId()).flatMap(nodes::get);
        if (!selectionUtility.isCallFeasible(node.getOrNull(), command, command.wantsUpToDateNode() ? 2 : Integer.MAX_VALUE)) {
            sessionBinding.removeNodeFromMapping(node.getOrNull());
            node = Option.none();
        }
        return node;
    }

    public void bindNodeToSession(IriCommand command, Node node) {
        if (node != null) {
            logger.debug("Bind session [{}] to node [{}].", command.getSessionId(), node.getName());
            sessionBinding.putNodeForClient(command.getSessionId(), node.getName());
        }
    }


}
