/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.Node;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class SelectionUtility {

    @Setter
    private boolean successiveRetryOnSameNode = false;

    @Setter
    private int maxRetriesPerNode = 1; // 1 retry means maximum of two calls

    //   call not possible => unavailable or circuit open
    //   synced => node in a consistent state. might be delayed, though.
    //   delayed => behind latest milestone. slightly behind is ok for many commands.
    //   retry => we don't want to timeout on the same node many times (takes too long for the client)
    public boolean isCallFeasible(Node node, IriCommand command, int maxDelay) {
        return node != null
                && node.isCallPossible()
                && node.isSupported(command)
                && (!command.needsUpToDateNode() || node.isSynced())
                && node.getDelay() <= maxDelay
                && retryAllowed(node, command);
    }

    private boolean retryAllowed(Node node, IriCommand command) {
        List<String> executionHistory = command.getExecutionHistory();
        return executionHistory.isEmpty() || (retryOnSameNodeAllowed(node, executionHistory) && retryOnNodeAllowed(node, executionHistory));
    }

    private boolean retryOnNodeAllowed(Node node, List<String> executionHistory) {
        return maxRetriesPerNode >= executionHistory.size() || maxRetriesPerNode >= executionHistory.stream().filter(name -> StringUtils.equals(name, node.getName())).count();
    }

    private boolean retryOnSameNodeAllowed(Node node, List<String> executionHistory) {
        return !StringUtils.equals(node.getName(), executionHistory.get(executionHistory.size() - 1)) || successiveRetryOnSameNode;
    }

}
