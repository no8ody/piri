/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.tolerance;

import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.core.env.Environment;

import java.time.Duration;

public class CircuitBreakerFactory {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final MeterFactory meterFactory;

    private final CircuitBreakerRegistry circuitBreakerRegistry;

    public CircuitBreakerFactory(MeterFactory meterFactory, Environment env) {
        this.meterFactory = meterFactory;
        CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
                .failureRateThreshold(getFailureRate(env))
                .waitDurationInOpenState(getOpenDuration(env))
                .ringBufferSizeInHalfOpenState(getHalfOpenBuffer(env))
                .ringBufferSizeInClosedState(getClosedBuffer(env))
                .build();
        circuitBreakerRegistry = CircuitBreakerRegistry.of(circuitBreakerConfig);
    }

    public CircuitBreaker circuitBreaker(String name) {
        logger.debug("Creating circuit breaker for [{}].", name);
        CircuitBreaker circuitBreaker = circuitBreakerRegistry.circuitBreaker(name);
        meterFactory.createMetricsForCircuitBreaker(circuitBreaker);
        return circuitBreaker;
    }

    private int getInt(Environment env, String key, int defaultValue) {
        return env == null ? defaultValue : env.getProperty(key, Integer.class, defaultValue);
    }

    private float getFailureRate(Environment env) {
        float value = env == null ? 19.9f : env.getProperty("piri.circuit.failure.rate", Float.class, 19.9f);
        logger.info("Configured circuit breaker failure rate: [{}].", value);
        return value;
    }

    private Duration getOpenDuration(Environment env) {
        Duration duration = env == null
                ? Duration.ofSeconds(120)
                : DurationStyle.detectAndParse(env.getProperty("piri.circuit.open.duration", "120s"));
        logger.info("Configured circuit breaker open time: [{}].", duration);
        return duration;
    }

    private int getClosedBuffer(Environment env) {
        int value = getInt(env, "piri.circuit.buffer.closed", 10);
        logger.info("Configured circuit breaker closed buffer size: [{}].", value);
        return value;
    }

    private int getHalfOpenBuffer(Environment env) {
        int value = getInt(env, "piri.circuit.buffer.half-open", 1);
        logger.info("Configured circuit breaker half open buffer size: [{}].", value);
        return value;
    }

}
