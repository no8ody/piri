/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.tolerance;

import com.mio.piri.commands.IriCommand;
import io.github.resilience4j.ratelimiter.RateLimiter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

public class RateLimitOperations {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Pattern noRateLimitForPattern;
    private final RateLimiterFactory rateLimiterFactory;

    private final Map<String, Integer> commandsSuccessWeight = new HashMap<>();
    private final Set<String> rateLimitedCommands;

    public RateLimitOperations(Environment env, RateLimiterFactory rateLimiterFactory) {
        this.rateLimiterFactory = rateLimiterFactory;

        rateLimitedCommands = rateLimiterFactory.getListOfRateLimitedCommands();
        // get maximum dynamic limit (limit that can be increased, e.g. for ignoring erroneous responses).
        rateLimitedCommands.forEach(str -> commandsSuccessWeight.put(str,
                initRateLimitWeight(env, "ip." + str)));

        String noLimitPattern = StringUtils.stripToNull(env.getProperty("piri.rate.ip.unlimited.pattern"));
        if (noLimitPattern == null) {
            logger.debug("No rate limit exception configured.");
            noRateLimitForPattern = null;
        } else {
            logger.info("Set rate limit exception for IPs matching: [{}]", noLimitPattern);
            noRateLimitForPattern = Pattern.compile(noLimitPattern);
        }
    }

    public RateLimiter globalRateLimiter() {
        return rateLimiterFactory.globalRateLimiter();
    }

    public RateLimiter rateLimiter(final String ip, final String command) {
        return rateLimiterFactory.rateLimiter(ip, command);
    }

    public boolean isIpRateLimited(String ip) {
        // rate limit if no exception defined or ip does not match pattern
        return noRateLimitForPattern == null || StringUtils.isBlank(ip) || !noRateLimitForPattern.matcher(ip).matches();
    }

    public void applyContentSpecificRateLimitWeight(final IriCommand command, RateLimiter rateLimiter) {

        Objects.requireNonNull(rateLimiter, "No rate limiter. Command: " + command.getCommand() + "");

        if (rateLimitedCommands.contains(command.getCommand())) {
            int weight = command.getRateLimitCount();
            if (weight > 1) {
                applyRateLimitWeight(rateLimiter, weight);
            }
        }

    }

    public void applySuccessRateLimitWeight(final String ip, final String command) {

        // only apply if command is rate limit and weight > 1
        if (rateLimitedCommands.contains(command) && commandsSuccessWeight.getOrDefault(command, 1) > 1) {
            RateLimiter rateLimiter = rateLimiterFactory.rateLimiter(ip, command);
            applyRateLimitWeight(rateLimiter, commandsSuccessWeight.get(command));
        }

    }

    private void applyRateLimitWeight(final RateLimiter rateLimiter, int weight) {
        assert rateLimiter != null : "Rate limiter must not be null.";
        int processed = 1;
        long waitTime = 0;
        while (processed < weight && waitTime >= 0) {
            // negative wait time means there is no reservation possible
            // zero means reserved
            // positive means reservation possible within timeout
            waitTime = rateLimiter.reservePermission(rateLimiter.getRateLimiterConfig().getTimeoutDuration());
            processed++;
            if (waitTime != 0) {
                logger.debug("Reservation wait time for rate limiter [{}]: [{}] nanos.", rateLimiter.getName(), waitTime);
            }
        }
        logger.debug("Applied rate limit weight of [{}] for rate limiter [{}].", processed, rateLimiter.getName());

    }

    private int initRateLimitWeight(Environment env, String commandName) {
        int value = env.getProperty("piri.rate." + commandName + ".weight", Integer.class, 1);
        logger.info("Configured [{}] weight: [{}].", commandName, value);
        return value;
    }

}
