/*
 * Copyright (c) 2020.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.actuator;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;

import java.lang.management.ManagementFactory;
import java.time.Instant;

public class UptimeHealthIndicator extends AbstractHealthIndicator {

    private final static Instant startupTime = Instant.ofEpochMilli(ManagementFactory.getRuntimeMXBean().getStartTime());
    private final MeterRegistry meterRegistry;

    public UptimeHealthIndicator(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) {
        Gauge uptimeGauge = meterRegistry.find("process.uptime").gauge();
        double uptime = uptimeGauge == null ? 0 : uptimeGauge.value();
        builder.withDetail("uptime", uptime).up();
        builder.withDetail("startup", startupTime);
    }

}

