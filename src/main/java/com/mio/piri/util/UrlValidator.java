/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Optional;

public class UrlValidator {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public boolean isValidUrl(String uri) {
        return url(uri).isPresent();
    }

    public boolean areEqual(String firstUrl, String secondUrl) {
        return url(firstUrl).equals(url(secondUrl));
    }

    private Optional<URL> url(String url) {
        try {
            return Optional.of(new URL(url));
        } catch (Exception e1) {
            return Optional.empty();
        }
    }

    public boolean resolveToSameAddress(String firstUrlOrHost, String secondUrlOrHost) {
        try {
            InetAddress firstAddress = addressStringToInetAddress(firstUrlOrHost);
            InetAddress secondAddress = addressStringToInetAddress(secondUrlOrHost);
            boolean equals = firstAddress.equals(secondAddress);
            if (equals) {
                logger.info("Urls [{}] and [{}] are resolving to the same address: [{}] <-> [{}].", firstUrlOrHost, secondUrlOrHost, firstAddress, secondAddress);
            }
            return equals;
        } catch (Exception e) {
            logger.warn("Error comparing urls [{}] and [{}]. {}", firstUrlOrHost, secondUrlOrHost, e.toString());
            return false;
        }
    }

    private InetAddress addressStringToInetAddress(String address) {
        return url(address).map(url -> inetAddressFromHost(url.getHost())).orElseGet(() -> inetAddressFromHost(address));
    }

    private InetAddress inetAddressFromHost(String host) {
        try {
            return InetAddress.getByName(host);
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
