/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri;

import com.mio.piri.commands.IriCommands;
import com.mio.piri.nodes.PowNode;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.core.env.Environment;

import java.time.Duration;

public class PiriInitialization {

    public PiriInitialization(Environment env) {
        // initialize iri timeouts.
        String defaultTimeout = env.getProperty("iri.timeout.default", "10s");
        IriCommands.DEFAULT_TIMEOUT = parseDuration(defaultTimeout);
        for (IriCommands command : IriCommands.values()) {
            command.setTimeout(parseDuration(env.getProperty("iri.timeout." + command.getKey(), defaultTimeout)));
        }
        // initialize custom timeouts
        PowNode.POW_TIMEOUT = parseDuration(env.getProperty("pow.timeout.attachToTangle", "5s"));
    }

    private Duration parseDuration(String duration) {
        return DurationStyle.detectAndParse(duration);
    }

}
