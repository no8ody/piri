/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.header.XFrameOptionsServerHttpHeadersWriter;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.StringUtils.toStringArray;

// see https://docs.spring.io/spring-security/site/docs/current/reference/html5/#jc-webflux
@Configuration
@EnableWebFluxSecurity
public class WebSecurityConfiguration {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    public final static String ROLE_NODE_CLIENT = "NODE_CLIENT";
    public final static String ROLE_NODE_ADMIN = "NODE_ADMIN";
    public final static String ROLE_ACTUATOR_USER = "ACTUATOR_USER";

    private static final String[] GLOBAL_WHITELIST = {
            "/",
            "/favicon.ico"
    };

    private static final String[] SWAGGER_WHITELIST = {
            // -- swagger ui
            "/v2/api-docs",
            "/csrf", // otherwise some authentication popup is triggered
            "/swagger-resources/configuration/ui",
            "/swagger-resources/configuration/security",
            "/swagger-resources",
            "/swagger-ui.html",
            "/webjars/**"
    };

    private static final String[] NODES_API = {
            // -- swagger ui
            "/nodes",
            "/nodes/*"
    };

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http, Environment env) {

        boolean secureRegistrationEndpoint = env.getProperty("piri.nodes.registration.secure", Boolean.class, true);

        http.headers().frameOptions().mode(XFrameOptionsServerHttpHeadersWriter.Mode.SAMEORIGIN); // default is DENY

        ServerHttpSecurity.AuthorizeExchangeSpec exchangeSpec = http
                .authorizeExchange()
                .pathMatchers(GLOBAL_WHITELIST).permitAll() // needs no authentication
                .pathMatchers("/nodes/unregister").hasRole(ROLE_NODE_ADMIN); // always needs authorization

        if (secureRegistrationEndpoint) {
            logger.info("Node registration endpoint is open for authenticated users only.");
            exchangeSpec = exchangeSpec
                    .pathMatchers(HttpMethod.GET, "/nodes").permitAll() // get is allowed for /nodes
                    .pathMatchers(SWAGGER_WHITELIST).hasRole(ROLE_NODE_CLIENT)
                    .pathMatchers(NODES_API).hasRole(ROLE_NODE_CLIENT); // all other methods are not
        } else {
            logger.info("Node registration endpoint is open for the public.");
            exchangeSpec = exchangeSpec
                    .pathMatchers(SWAGGER_WHITELIST).permitAll()
                    .pathMatchers(NODES_API).permitAll();
        }

        ServerHttpSecurity filterChain = exchangeSpec.pathMatchers("/actuator/health").permitAll() // needs no authentication
                .pathMatchers("/actuator", "/actuator/*").hasRole(ROLE_ACTUATOR_USER) // needs endpoint authorization
                // .matchers(EndpointRequest.toAnyEndpoint()).hasRole("ENDPOINT_ADMIN") // throws SPR-17059
                .anyExchange().authenticated() // default fallback. requires authentication.
                .and().httpBasic()
                .and().formLogin()
                .and().csrf().disable()
                ;
        return filterChain.build();
    }

    @Bean
    public MapReactiveUserDetailsService userDetailsService(Environment env, SecurityProperties properties) {

        SecurityProperties.User user = properties.getUser();
        UserDetails defaultUser = userDetails(user.getName(), user.getPassword(), toStringArray(user.getRoles()));

        List<UserDetails> users = new ArrayList<>();
        users.add(defaultUser);

        for (String name : splitPropertyList(env.getProperty("piri.security.users"))) {
            String password = env.getRequiredProperty("piri.security.users." + name + ".password");
            String[] roles = splitPropertyList(env.getRequiredProperty("piri.security.users." + name + ".roles"));
            UserDetails additionalUser = userDetails(name, password, roles);
            users.add(additionalUser);
        }

        return new MapReactiveUserDetailsService(users);
    }

    private static String[] splitPropertyList(String propertyList) {
        String[] split = StringUtils.split(StringUtils.remove(propertyList, ' '), ',');
        return split == null ? new String[] {} : split;
    }

    private UserDetails userDetails(String name, String password, String[] roles) {
        logger.info("Registering user [{}] with roles {}", name, roles);
        return User
                .withUsername(name)
                .password(passwordEncoder.encode(password))
                .roles(roles)
                .build();
    }

}
