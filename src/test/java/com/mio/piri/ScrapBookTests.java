/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri;

import org.junit.Ignore;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.List;

@Ignore("Only for playing around")
public class ScrapBookTests {

    @Test
    public void testCommandQuorumWorkflow() {

        Flux<List<String>> flux = multipleCalls(301, 50, 100)
                .switchIfEmpty(Mono.error(new RuntimeException("no results")));

        // doesn't work this way.
        Flux<List<String>> second = flux
                .zipWith(multipleCalls(20, 201, 200, 220, 240, 300), (a, b) -> {
                    a.addAll(b);
                    return a; })
                .doOnNext(System.out::println)
                .log(getClass().getName());

//        flux.blockLast();
        second.blockLast();

    }

    private Flux<List<String>> multipleCalls(Integer... delays) {
        return Flux.just(delays)
                .flatMap(d -> doSomething(d).subscribeOn(Schedulers.parallel()))
                .filterWhen(res -> Mono.just(res.contains("success")))
                .doOnNext(next -> System.out.println("next: " + next))
                .take(2)
                .buffer(2) // Attention: flux does not complete until finished or at least two success elements
                .doOnNext(str -> System.out.println("buffer: " + str))
                .log(getClass().getName())
                ;
    }

    @Test
    public void givenResultsWhenBroadcastWorkflowThenReturnFirst() {
        // TODO better handling for mismatching results for non quorum commands
        // in case none is successful but 400 we should return 400
        // in case one is 200 and one 400 we should return 200
        Mono<String> publisher = multipleCallsReturnFirst(301, 100, 51, 200)
                .switchIfEmpty(Mono.error(new RuntimeException("no results")));
        StepVerifier.create(publisher).expectNextCount(1).verifyComplete();
    }

    @Test
    public void givenNoResultsWhenBroadcastWorkflowThenError() {
        Mono<String> publisher = multipleCallsReturnFirst(301, 101, 51, 201)
                .switchIfEmpty(Mono.error(new RuntimeException("no results")));
        StepVerifier.create(publisher).verifyError();
    }

    private Mono<String> multipleCallsReturnFirst(Integer... delays) {
        return Flux.just(delays)
                .flatMap(d -> doSomething(d).subscribeOn(Schedulers.parallel()))
                .filterWhen(res -> Mono.just(res.contains("success")))
                .next()
                .log(getClass().getName());
    }

    private Mono<String> doSomething(int delay) {
        return Mono.delay(Duration.ofMillis(delay))
                //.publishOn(Schedulers.parallel())
//                .doOnSubscribe(m -> System.out.println("subscribe: " + delay))
                .map(d -> delay%2 == 0 ? "success (" + delay + "ms)" : "error (" + delay + "ms)")
//                .delay(Duration.ofMillis(delay)) // simulate operation that takes some time
//                .thenReturn(delay + "ms")
                .log(getClass().getName())
                ;
    }



    @Test
    public void whenEmptyReturnAlternative() {
        StepVerifier.create(run(Mono.just("hui")))
                .expectNext(1)
                .expectComplete()
                .verify();

        StepVerifier.create(run(Mono.just("foo")))
                .expectNext(0)
                .expectComplete()
                .verify();
    }

    private Mono<Integer> run(Mono<String> input) {
        return input.filter(s -> !"foo".equals(s))
                .map(i -> { System.out.println("map called: " + i); return 1; })
                .switchIfEmpty(Mono.just(0));
    }

}
