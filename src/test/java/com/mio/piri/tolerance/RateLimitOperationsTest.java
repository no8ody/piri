/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.tolerance;

import com.mio.piri.commands.IriCommand;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import org.junit.Test;
import org.springframework.core.env.Environment;

import java.time.Duration;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RateLimitOperationsTest {

    private final Environment env = mock(Environment.class);
    private final RateLimiterFactory rateLimiterFactory = mock(RateLimiterFactory.class);

    @Test
    public void whenGlobalRateLimiterThenDelegate() {
        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        op.globalRateLimiter();
        verify(rateLimiterFactory).globalRateLimiter();
    }

    @Test
    public void whenRateLimiterThenDelegate() {
        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        op.rateLimiter("foo", "bar");
        verify(rateLimiterFactory).rateLimiter("foo", "bar");
    }

    @Test
    public void givenNoConfigWhenIsRateLimitedIpThenReturnTrue() {
        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        assertThat(op.isIpRateLimited("foo")).isTrue();
        assertThat(op.isIpRateLimited(null)).isTrue();
    }

    @Test
    public void givenNoMatchWhenIsRateLimitedIpThenReturnTrue() {
        when(env.getProperty("piri.rate.ip.unlimited.pattern")).thenReturn("foo");
        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        assertThat(op.isIpRateLimited("bar")).isTrue();
        assertThat(op.isIpRateLimited(null)).isTrue();
    }

    @Test
    public void givenMatchWhenIsRateLimitedIpThenReturnFalse() {
        when(env.getProperty("piri.rate.ip.unlimited.pattern")).thenReturn("foo");
        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        assertThat(op.isIpRateLimited("foo")).isFalse();
    }

    @Test
    public void whenApplyContentSpecificRateLimitThenDecreaseLimit() {
        when(env.getProperty(anyString(), eq(Integer.class), eq(1))).thenReturn(1);
        when(rateLimiterFactory.getListOfRateLimitedCommands()).thenReturn(Collections.singleton("some-command")); // needed to rate limit
        RateLimiter limiter = RateLimiter.of("some-limiter", RateLimiterConfig.custom()
                .timeoutDuration(Duration.ZERO) // no reservations for next period
                .limitRefreshPeriod(Duration.ofSeconds(1)) // long enough for several calls
                .limitForPeriod(5).build()); // test limit

        IriCommand command = mock(IriCommand.class);
        when(command.getRateLimitCount()).thenReturn(5);
        when(command.getCommand()).thenReturn("some-command");
        when(command.getIp()).thenReturn("foo-ip");

        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        limiter.getPermission(Duration.ZERO); // one permission for the call
        op.applyContentSpecificRateLimitWeight(command, limiter);
        assertThat(limiter.getMetrics().getAvailablePermissions()).isZero(); // content specific limit - 1
    }

    @Test
    public void givenNoRateLimitedCommandWhenApplyContentSpecificRateLimitThenDoNothing() {

        // Explanation why we do not want to apply the weight in this case:
        // if command is not rate limited it should be easy to calculate rate limits for global and per ip limiting
        // therefore we exclude not rate limited commands from this complicated calculation. for example if
        // wereAddressesSpentFrom is not limited it is difficult to say how many checked addresses are feasible for a
        // non malicious client and how that correlates to global limits.
        // malicious clients should be restricted by specifying max values in this case or a rate limiting configuration
        // should be added.

        RateLimiter limiter = RateLimiter.of("some-limiter", RateLimiterConfig.custom()
                .timeoutDuration(Duration.ZERO)
                .limitRefreshPeriod(Duration.ofSeconds(1))
                .limitForPeriod(5).build());

        IriCommand command = mock(IriCommand.class);
        when(command.getRateLimitCount()).thenReturn(5);
        when(command.getCommand()).thenReturn("some-command");
        when(command.getIp()).thenReturn("foo-ip");

        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        op.applyContentSpecificRateLimitWeight(command, limiter);
        assertThat(limiter.getMetrics().getAvailablePermissions()).isEqualTo(5); // no decrease happened
    }

    @Test
    public void whenApplyRateLimitWeightThenDecreaseLimit() {
        when(rateLimiterFactory.getListOfRateLimitedCommands()).thenReturn(Collections.singleton("some")); // needed for init

        RateLimiter limiter = RateLimiter.of("some-foo", RateLimiterConfig.custom()
                .timeoutDuration(Duration.ZERO) // no reservations for next period
                .limitRefreshPeriod(Duration.ofSeconds(1)) // long enough for several calls
                .limitForPeriod(10).build()); // test limit

        when(rateLimiterFactory.rateLimiter(anyString(), anyString())).thenReturn(limiter);
        when(env.getProperty("piri.rate.ip.some.weight", Integer.class, 1)).thenReturn(42);

        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);

        limiter.getPermission(Duration.ZERO); // one permission for the call
        assertThat(limiter.getMetrics().getAvailablePermissions()).isEqualTo(9);
        op.applySuccessRateLimitWeight("foo", "some"); // decreases by 9 (stops when no more reservations possible)
        assertThat(limiter.getMetrics().getAvailablePermissions()).isZero();
    }

    @Test
    public void givenWaitTimeWhenApplyRateLimitWeightThenDecreaseLimitForNextPeriod() {
        when(rateLimiterFactory.getListOfRateLimitedCommands()).thenReturn(Collections.singleton("some")); // needed for init

        final int limit = 10;
        final int weight = 11;

        RateLimiter limiter = RateLimiter.of("some-rate-limiter", RateLimiterConfig.custom()
                .timeoutDuration(Duration.ofSeconds(1)) // additional reservations go into next period
                .limitRefreshPeriod(Duration.ofSeconds(1)) // long enough for several calls
                .limitForPeriod(limit).build()); // test limit

        when(rateLimiterFactory.rateLimiter(anyString(), anyString())).thenReturn(limiter);
        when(env.getProperty("piri.rate.ip.some.weight", Integer.class, 1)).thenReturn(weight);

        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);

        limiter.getPermission(Duration.ZERO); // one permission for the call
        assertThat(limiter.getMetrics().getAvailablePermissions()).isEqualTo(limit - 1);
        op.applySuccessRateLimitWeight("foo", "some"); // decreases by 10 (weight - 1)
        assertThat(limiter.getMetrics().getAvailablePermissions()).isEqualTo(limit - weight); // 1 reservation waiting
    }

    @Test
    public void givenNoWeightConfigWhenApplyRateLimitWeightThenDoNotDecrease() {
        RateLimiter limiter = RateLimiter.ofDefaults("some-rate-limiter");
        when(rateLimiterFactory.rateLimiter(anyString(), anyString())).thenReturn(limiter);

        RateLimitOperations op = new RateLimitOperations(env, rateLimiterFactory);
        int limitForPeriod = limiter.getRateLimiterConfig().getLimitForPeriod();
        op.applySuccessRateLimitWeight("foo", "some"); // does nothing
        assertThat(limiter.getMetrics().getAvailablePermissions()).isEqualTo(limitForPeriod);
    }


}