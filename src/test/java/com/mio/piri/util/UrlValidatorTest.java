/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UrlValidatorTest {

    private final UrlValidator validator = new UrlValidator();

    @Test
    public void givenValidThenReturnTrue() {
        assertThat(validator.isValidUrl("http://foo.bar.com:14265")).isTrue();
        assertThat(validator.isValidUrl("http://foo.com")).isTrue();
        assertThat(validator.isValidUrl("https://foo.bar.com:8443")).isTrue();
        assertThat(validator.isValidUrl("https://foo.bar.com:443")).isTrue();
        assertThat(validator.isValidUrl("https://foo.com")).isTrue();
        assertThat(validator.isValidUrl("http://192.168.0.1")).isTrue();
    }

    @Test
    public void givenInvalidThenReturnFalse() {
        assertThat(validator.isValidUrl("foo.bar.com:14265")).isFalse();
        assertThat(validator.isValidUrl("foo")).isFalse();
    }

    @Test
    public void whenEqualsThenReturnTrue() {
        assertThat(validator.areEqual("http://foo.bar.com", "http://foo.bar.com")).isTrue();
        assertThat(validator.areEqual("https://foo.bar.com:443", "https://foo.bar.com")).isTrue();
        assertThat(validator.areEqual("http://foo.bar.com:443", "https://foo.bar.com")).isFalse();
    }

    @Test
    public void givenSameHostInUrlWhenResolveToSameAddressThenReturnTrue() {
        assertThat(validator.resolveToSameAddress("http://localhost:1234", "http://127.0.0.1:80")).isTrue();
        assertThat(validator.resolveToSameAddress("http://127.0.0.1", "http://localhost")).isTrue();
        assertThat(validator.resolveToSameAddress("http://localhost", "https://127.0.0.1")).isTrue();
        assertThat(validator.resolveToSameAddress("http://localhost:8080", "https://127.0.0.1:443")).isTrue();
        assertThat(validator.resolveToSameAddress("https://google-public-dns-a.google.com", "http://8.8.8.8")).isTrue();

    }

    @Test
    public void givenHostInsteadOfUrlWhenResolveToSameAddressThenReturnTrue() {
        assertThat(validator.resolveToSameAddress("http://8.8.8.8", "google-public-dns-a.google.com")).isTrue();
        assertThat(validator.resolveToSameAddress("https://google-public-dns-a.google.com", "8.8.8.8")).isTrue();
        assertThat(validator.resolveToSameAddress("8.8.8.8", "google-public-dns-a.google.com")).isTrue();
        assertThat(validator.resolveToSameAddress("localhost", "127.0.0.1")).isTrue(); // no valid url but host
        assertThat(validator.resolveToSameAddress("localhost", null)).isTrue(); // no valid url but host
        assertThat(validator.resolveToSameAddress(null, null)).isTrue(); // null resolves to localhost
    }

    @Test
    public void givenDifferentIpWhenResolveToSameAddressThenReturnFalse() {
        assertThat(validator.resolveToSameAddress("http://8.8.8.8", "http://google.com")).isFalse();
        assertThat(validator.resolveToSameAddress("http://localhost", "http://192.168.0.1")).isFalse();
        assertThat(validator.resolveToSameAddress("http://i.am.not.resolvable", "http://1.2.3.4")).isFalse();
        assertThat(validator.resolveToSameAddress("http://1.2.3.4", "http://5.6.7.8")).isFalse();
        assertThat(validator.resolveToSameAddress("foo", "foo")).isFalse(); // no valid url

    }



}