/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.util;

import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;

public class IriClientTestUtility {

    private final WebTestClient webTestClient;

    public IriClientTestUtility(WebTestClient webTestClient) {
        this.webTestClient = webTestClient;
    }

    public WebTestClient.ResponseSpec postCommand(Object command) {
        return postCommand("/", command, 30);
    }

    public WebTestClient.ResponseSpec postCommand(String url, Object command, int timeoutSecs) {
        return webTestClient.mutate().responseTimeout(Duration.ofSeconds(timeoutSecs)).build()
                .post()
                .uri(url)
                .accept(MediaType.APPLICATION_JSON)
                .header("X-IOTA-API-Version", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(command)
                .exchange();
    }

}
