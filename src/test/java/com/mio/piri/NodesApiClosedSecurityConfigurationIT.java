/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri;

import com.mio.piri.nodes.api.dtos.UnregisterNodeRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.mio.piri.WebSecurityConfiguration.ROLE_NODE_ADMIN;
import static com.mio.piri.WebSecurityConfiguration.ROLE_NODE_CLIENT;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.springSecurity;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"piri.nodes.registration.secure=true"}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NodesApiClosedSecurityConfigurationIT {

    @Autowired
    private ApplicationContext context;

    private WebTestClient restClient;

    @Before
    public void setUpSecurity() {
        restClient = WebTestClient
                .bindToApplicationContext(context)
                .apply(springSecurity())
                .configureClient()
                .build();
    }

    @Test
    public void whenGetNodesThenOk() {
        restClient.get().uri("/nodes").exchange().expectStatus().isOk();
    }

    @Test
    public void whenRegisterNodeThenUnauthorized() {
        restClient.post().uri("/nodes").exchange().expectStatus().isUnauthorized();
    }

    @Test
    public void whenUnregisterNodeThenUnauthorized() {
        restClient.delete().uri("/nodes/foo").exchange().expectStatus().isUnauthorized();
    }

    @Test
    public void whenUnregisterNodeByPostThenUnauthorized() {
        restClient.post().uri("/nodes/unregister").exchange().expectStatus().isUnauthorized();
    }

    // authorized

    @WithMockUser(roles = ROLE_NODE_ADMIN)
    @Test
    public void givenNodeAdminRoleWhenUnregisterNodeByPostThenAllowed() {
        restClient.post().uri("/nodes/unregister").bodyValue(new UnregisterNodeRequest("foo")).exchange().expectStatus().isNotFound();
    }

    @WithMockUser(roles = ROLE_NODE_CLIENT)
    @Test
    public void givenRoleWhenUnregisterNodeByPostThenForbidden() {
        restClient.post().uri("/nodes/unregister").exchange().expectStatus().isForbidden();
    }


    @WithMockUser(roles = ROLE_NODE_CLIENT)
    @Test
    public void givenRoleWhenRegisterNodeThenAllowed() {
        restClient.post().uri("/nodes").contentType(MediaType.APPLICATION_JSON).exchange().expectStatus().isBadRequest();
    }

    @WithMockUser(roles = ROLE_NODE_CLIENT)
    @Test
    public void givenRoleWhenUnregisterNodeThenAllowed() {
        restClient.delete().uri("/nodes/foo").exchange().expectStatus().isNotFound();
    }

    // swagger

    @Test
    public void whenApiDocsThenUnauthorized() {
        restClient.get().uri("/v2/api-docs").exchange().expectStatus().isUnauthorized();
    }

    @Test
    public void whenSwaggerResourcesThenUnauthorized() {
        restClient.get().uri("/swagger-resources").exchange().expectStatus().isUnauthorized();
        restClient.get().uri("/swagger-resources/configuration/security").exchange().expectStatus().isUnauthorized();
        restClient.get().uri("/swagger-resources/configuration/ui").exchange().expectStatus().isUnauthorized();
    }

    @Test
    public void whenSwaggerUiThenUnauthorized() {
        restClient.get().uri("/swagger-ui.html").exchange().expectStatus().isUnauthorized();
        restClient.get().uri("/csrf").exchange().expectStatus().isUnauthorized(); // needed for some reason
    }

}