/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri;

import com.mio.piri.commands.IriCommands;
import com.mio.piri.nodes.PowNode;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;

import java.time.Duration;

import static com.mio.piri.commands.IriCommands.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PiriInitializationTest {

    private final Environment env = mock(Environment.class);

    @Before
    public void setupMocks() {
        when(env.getProperty(anyString(), anyString())).thenReturn("42s");
    }

    @Test
    public void shouldInitTimeouts() {
        when(env.getProperty(eq("iri.timeout.default"), anyString())).thenReturn("1s");
        when(env.getProperty(eq("iri.timeout.attachToTangle"), anyString())).thenReturn("2s");
        when(env.getProperty(eq("iri.timeout.getTrytes"), anyString())).thenReturn("3s");
        when(env.getProperty(eq("iri.timeout.getTransactionsToApprove"), anyString())).thenReturn("4s");
        when(env.getProperty(eq("pow.timeout.attachToTangle"), anyString())).thenReturn("6s");
        new PiriInitialization(env);
        assertThat(IriCommands.DEFAULT_TIMEOUT).isEqualTo(Duration.ofSeconds(1));
        assertThat(ATTACH_TO_TANGLE.getTimeout()).isEqualTo(Duration.ofSeconds(2));
        assertThat(GET_TRYTES.getTimeout()).isEqualTo(Duration.ofSeconds(3));
        assertThat(GET_TRANSACTIONS_TO_APPROVE.getTimeout()).isEqualTo(Duration.ofSeconds(4));
        assertThat(PowNode.POW_TIMEOUT).isEqualTo(Duration.ofSeconds(6));
    }

}