/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.metrics;

import com.mio.piri.nodes.selection.SessionBinding;
import com.mio.piri.nodes.IriNode;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.utils.MetricNames;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.Test;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MeterFactoryTest {

    private final MeterRegistry registry = new SimpleMeterRegistry();
    private final MeterFactory factory = new MeterFactory(registry);

    @Test
    public void shouldCreateCommandTimer() {
        Timer fooTimer = factory.createCommandTimer("fooCommand", "fooNode", "fooKey");
        assertThat(fooTimer).isNotNull();
        assertThat(fooTimer.getId().getName()).isEqualTo("iri.commands");
        assertThat(fooTimer.getId().getTag("command")).isEqualTo("fooCommand");
        assertThat(fooTimer.getId().getTag("node")).isEqualTo("fooNode");
        assertThat(fooTimer.getId().getTag("key")).isEqualTo("fooKey");
    }

    @Test
    public void givenDifferentCommandWhenCreateThenReturnDifferentTimers() {
        Timer fooTimer = factory.createCommandTimer("fooCommand", "fooNode", "fooKey");
        Timer barTimer = factory.createCommandTimer("barCommand", "fooNode", "fooKey");
        assertThat(fooTimer).isNotSameAs(barTimer);
        assertThat(fooTimer).isNotEqualTo(barTimer);
    }

    @Test
    public void givenDifferentNodeWhenCreateThenReturnDifferentTimers() {
        Timer fooTimer = factory.createCommandTimer("fooCommand", "fooNode", "fooKey");
        Timer barTimer = factory.createCommandTimer("fooCommand", "barNode", "barKey");
        assertThat(fooTimer).isNotSameAs(barTimer);
        assertThat(fooTimer).isNotEqualTo(barTimer);
    }

    @Test
    public void givenSameCommandAndNodeWhenCreateThenReturnSameTimer() {
        Timer fooTimer = factory.createCommandTimer("fooCommand", "fooNode", "fooKey");
        Timer anotherFoo = factory.createCommandTimer("fooCommand", "fooNode", "fooKey");
        assertThat(fooTimer).isSameAs(anotherFoo);
    }

    @Test
    public void shouldCreateRejectedCommandCounter() {
        Counter fooCounter = factory.createRejectedCommandCounter("fooCommand");
        assertThat(fooCounter).isNotNull();
        assertThat(fooCounter.getId().getName()).isEqualTo("iri.commands.rejected");
        assertThat(fooCounter.getId().getTag("command")).isEqualTo("fooCommand");
    }

    @Test
    public void shouldCreateFailedCommandCounter() {
        Counter fooCounter = factory.createFailedCommandCounter("foo-command", "foo-error");
        assertThat(fooCounter).isNotNull();
        assertThat(fooCounter.getId().getName()).isEqualTo("iri.commands.retry");
        assertThat(fooCounter.getId().getTag("command")).isEqualTo("foo-command");
        assertThat(fooCounter.getId().getTag("error")).isEqualTo("foo-error");
    }

    @Test
    public void givenDifferentCommandsWhenCreateThenReturnDifferentCounter() {
        Counter fooCounter = factory.createRejectedCommandCounter("fooCommand");
        Counter barCounter = factory.createRejectedCommandCounter("barCommand");
        assertThat(fooCounter).isNotSameAs(barCounter);
        assertThat(fooCounter).isNotEqualTo(barCounter);
    }

    @Test
    public void givenSameCommandsWhenCreateThenReturnSameCounter() {
        Counter fooCounter = factory.createRejectedCommandCounter("fooCommand");
        Counter anotherFoo = factory.createRejectedCommandCounter("fooCommand");
        assertThat(fooCounter).isSameAs(anotherFoo);
    }

    @Test
    public void shouldCreateTotalNodesGauge() {
        Map<String, IriNode> nodes = new HashMap<>();
        Gauge totalNodes = factory.createTotalNodesGauge(nodes);
        assertThat(totalNodes).isNotNull();
        assertThat(totalNodes.value()).isZero();
        nodes.put("foo", inactiveNode());
        assertThat(totalNodes.value()).isOne();
    }

    @Test
    public void shouldCreateActiveNodesGauge() {
        Map<String, IriNode> nodes = new HashMap<>();
        Gauge activeNodes = factory.createActiveNodesGauge(nodes);
        assertThat(activeNodes).isNotNull();
        assertThat(activeNodes.value()).isZero();
        // add active node
        nodes.put("bar", activeNode());
        assertThat(activeNodes.value()).isOne();
    }

    @Test
    public void shouldCreateInactiveNodesGauge() {
        Map<String, IriNode> nodes = new HashMap<>();
        Gauge inactiveNodes = factory.createInactiveNodesGauge(nodes);
        assertThat(inactiveNodes).isNotNull();
        assertThat(inactiveNodes.value()).isZero();
        nodes.put("foo", inactiveNode());
        assertThat(inactiveNodes.value()).isOne();
    }

    @Test
    public void shouldCreateSyncedNodesGauge() {
        Map<String, IriNode> nodes = new HashMap<>();
        Gauge syncedNodes = factory.createSyncedNodesGauge(nodes);
        assertThat(syncedNodes).isNotNull();
        assertThat(syncedNodes.value()).isZero();
        nodes.put("foo", syncedNode());
        assertThat(syncedNodes.value()).isOne();
    }

    @Test
    public void shouldCreateClientMappingsGauge() {
        SessionBinding mapper = new SessionBinding(Duration.ofMillis(100), Duration.ofHours(1));
        Gauge mappedClients = factory.createClientMappingsGauge(mapper);
        assertThat(mappedClients).isNotNull();
        assertThat(mappedClients.value()).isZero();
        mapper.putNodeForClient("foo", "bar");
        assertThat(mappedClients.value()).isOne();
    }

    @Test
    public void shouldCreateRateLimitersGauge() {
        Map<String, RateLimiter> rateLimiters = new HashMap<>();
        Gauge gauge = factory.createRateLimiterCountGauge(rateLimiters);
        assertThat(gauge).isNotNull();
        assertThat(gauge.value()).isZero();
        rateLimiters.put("foo", mock(RateLimiter.class));
        assertThat(gauge.value()).isOne();
    }

    @Test
    public void shouldReturnAllRegisteredCommandTimers() {
        Timer fooTimer = factory.createCommandTimer("fooCommand", "fooNode", "fooKey");
        assertThat(factory.findAllCommandTimers()).contains(fooTimer);
    }

    @Test
    public void givenNoCommandTimersWhenFindAllThenReturnNullOrEmptyCollection() {
        assertThat(factory.findAllCommandTimers()).isNullOrEmpty();
    }

    @Test
    public void shouldCreateGaugesForCircuitBreaker() {
        CircuitBreaker cb = mock(CircuitBreaker.class);
        when(cb.getName()).thenReturn("foo");
        factory.createMetricsForCircuitBreaker(cb);
        assertThat(registry.get("iri.circuit." + MetricNames.STATE).tag("node", "foo").gauge()).isNotNull();
        assertThat(registry.get("iri.circuit." + MetricNames.FAILED).tag("node", "foo").gauge()).isNotNull();
        assertThat(registry.get("iri.circuit." + MetricNames.NOT_PERMITTED).tag("node", "foo").gauge()).isNotNull();
        assertThat(registry.get("iri.circuit." + MetricNames.SUCCESSFUL).tag("node", "foo").gauge()).isNotNull();
        // not needed:
        assertThat(registry.find("iri.circuit." + MetricNames.BUFFERED).tag("node", "foo").gauge()).isNull();
        assertThat(registry.find("iri.circuit." + MetricNames.BUFFERED_MAX).tag("node", "foo").gauge()).isNull();
    }

    private IriNode activeNode() {
        IriNode activeNode = inactiveNode();
        when(activeNode.isAvailable()).thenReturn(true);
        return activeNode;
    }

    private IriNode syncedNode() {
        IriNode syncedNode = inactiveNode();
        when(syncedNode.isSynced()).thenReturn(true);
        return syncedNode;
    }

    private IriNode inactiveNode() {
        return mock(IriNode.class);
    }


}