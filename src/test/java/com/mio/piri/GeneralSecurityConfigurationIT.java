/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.mio.piri.WebSecurityConfiguration.ROLE_ACTUATOR_USER;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.springSecurity;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "piri.nodes.registration.secure=true",
        "piri.security.users=foo",
        "piri.security.users.foo.password=bar",
        "piri.security.users.foo.roles=ACTUATOR_USER"
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GeneralSecurityConfigurationIT {

    @Autowired
    private ApplicationContext context;

    private WebTestClient restClient;

    @Before
    public void setUpSecurity() {
        restClient = WebTestClient
                .bindToApplicationContext(context)
                .apply(springSecurity())
                .configureClient()
                .build();
    }

    @Test
    public void whenActuatorHealthThenOK() {
        restClient.get().uri("/actuator/health").exchange().expectStatus().isOk();
    }

    @Test
    public void whenGetThenSendFrameOriginHeader() {
        restClient.get().uri("/actuator/health").exchange().expectHeader().valueMatches("X-Frame-Options", "SAMEORIGIN");
    }

    @Test
    public void whenActuatorInfoThen401Unauthorized() {
        restClient.get().uri("/actuator/info").exchange().expectStatus().isUnauthorized();
    }

    @Test
    public void whenPrometheusThen401Unauthorized() {
        restClient.get().uri("/actuator/prometheus").exchange().expectStatus().isUnauthorized();
    }

    @WithMockUser
    @Test
    public void givenUserWhenActuatorInfoThen403Forbidden() {
        restClient.get().uri("/actuator/info").exchange().expectStatus().isForbidden();
    }

    @WithMockUser
    @Test
    public void givenUserWhenPrometheusThen403Forbidden() {
        restClient.get().uri("/actuator/prometheus").exchange().expectStatus().isForbidden();
    }

    @WithMockUser(roles = ROLE_ACTUATOR_USER)
    @Test
    public void givenEndpointAdminWhenActuatorInfoThen200Ok() {
        restClient.get().uri("/actuator/info").exchange().expectStatus().isOk();
    }

    @WithMockUser(roles = ROLE_ACTUATOR_USER)
    @Test
    public void givenEndpointAdminWhenPrometheusThen200Ok() {
        restClient.get().uri("/actuator/prometheus").exchange().expectStatus().isOk();
    }

    @Test
    public void whenUnknownPathThen401Unauthorized() {
        restClient.get().uri("/foo/bar").exchange().expectStatus().isUnauthorized();
    }

    @WithMockUser
    @Test
    public void givenUserWhenUnknownPathThen404NotFound() {
        restClient.get().uri("/foo/bar").exchange().expectStatus().isNotFound();
    }

    @WithUserDetails("foo")
    @Test
    public void givenActuatorUserWhenActuatorInfoThen200OK() {
        restClient.get().uri("/actuator/info").exchange().expectStatus().isOk();
    }

    @WithMockUser(username = "invalid", password = "credentials")
    @Test
    public void givenInvalidUserWhenActuatorInfoThen403Forbidden() {
        restClient.get().uri("/actuator/info").exchange().expectStatus().isForbidden();
    }

}