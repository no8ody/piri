package com.mio.piri.commands;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class FindTransactionsTest {

    @Test
    public void whenGetRateLimitCountThenReturnAddressesCount() {
        FindTransactions command = new FindTransactions();
        command.setAddresses(Arrays.asList("a", "b", "c"));
        assertThat(command.getRateLimitCount()).isEqualTo(3);
    }

    @Test
    public void whenGetRateLimitCountThenReturnTagsCount() {
        FindTransactions command = new FindTransactions();
        command.setTags(Arrays.asList("a", "b", "c"));
        assertThat(command.getRateLimitCount()).isEqualTo(3);
    }

    @Test
    public void whenGetRateLimitCountThenReturnApproveesCount() {
        FindTransactions command = new FindTransactions();
        command.setApprovees(Arrays.asList("a", "b", "c"));
        assertThat(command.getRateLimitCount()).isEqualTo(3);
    }

    @Test
    public void whenGetRateLimitCountThenReturnBundlesCount() {
        FindTransactions command = new FindTransactions();
        command.setBundles(Arrays.asList("a", "b", "c"));
        assertThat(command.getRateLimitCount()).isEqualTo(3);
    }

    @Test
    public void whenGetRateLimitCountThenReturnCombinedCount() {
        FindTransactions command = new FindTransactions();
        command.setAddresses(Collections.singletonList("a"));
        command.setTags(Arrays.asList("a", "b"));
        command.setBundles(Arrays.asList("a", "b", "c"));
        command.setApprovees(Arrays.asList("a", "b", "c", "d"));
        assertThat(command.getRateLimitCount()).isEqualTo(10);
    }


}