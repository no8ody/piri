/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.process;

import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.nodes.Node;
import io.vavr.collection.List;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetNodeInfoResponsePostProcessorTest {

    private static final String FEATURES_REMOTE_POW = "\"features\":[\"RemotePOW\"]";
    private static final String FEATURES_SOMETHING = "\"features\":[\"something\"]";
    private static final String FEATURES_SOMETHING_WITH_POW = "\"features\":[\"something\",\"RemotePOW\"]";
    private static final String FEATURES_EMPTY = "\"features\":[]";

    private static final String INFO_WITH_REMOTE_POW = "{" + FEATURES_REMOTE_POW + "}";
    private static final String INFO_WITH_SOMETHING_AND_REMOTE_POW = "{" + FEATURES_SOMETHING_WITH_POW + "}";
    private static final String INFO_WITHOUT_REMOTE_POW = "{" + FEATURES_SOMETHING + "}";
    private static final String INFO_WITH_EMPTY_FEATURES = "{" + FEATURES_EMPTY + "}";
    private static final String INFO_EMPTY = "{}";

    private final NodeInfoOverride overrides = mock(NodeInfoOverride.class);

    @Test
    public void givenExpectedPowFeatureWhenPostProcessThenDoNothing() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, false, Duration.ZERO, overrides);
        assertThat(postProcessor.postProcess(INFO_WITH_REMOTE_POW)).contains(FEATURES_REMOTE_POW);
        assertThat(postProcessor.postProcess(INFO_WITH_SOMETHING_AND_REMOTE_POW)).contains(FEATURES_SOMETHING_WITH_POW);
    }

    @Test
    public void givenMissingRemotePowFeatureWhenPostProcessThenAdd() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, false, Duration.ZERO, overrides);
        assertThat(postProcessor.postProcess(INFO_WITHOUT_REMOTE_POW)).contains(FEATURES_SOMETHING_WITH_POW);
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains(FEATURES_REMOTE_POW);
        assertThat(postProcessor.postProcess(INFO_WITH_EMPTY_FEATURES)).contains(FEATURES_REMOTE_POW);
    }

    @Test
    public void givenExpectedMissingPowFeatureWhenPostProcessThenDoNothing() {
        GetNodeInfoResponsePostProcessor postProcessorPowDisabled = new GetNodeInfoResponsePostProcessor(false, false, Duration.ZERO, overrides);
        assertThat(postProcessorPowDisabled.postProcess(INFO_WITHOUT_REMOTE_POW)).contains(FEATURES_SOMETHING);
        assertThat(postProcessorPowDisabled.postProcess(INFO_EMPTY)).doesNotContain("features");
        assertThat(postProcessorPowDisabled.postProcess(INFO_WITH_EMPTY_FEATURES)).contains(FEATURES_EMPTY);
    }

    @Test
    public void givenUnexpectedRemotePowFeatureWhenPostProcessThenRemove() {
        GetNodeInfoResponsePostProcessor postProcessorPowDisabled = new GetNodeInfoResponsePostProcessor(false, false, Duration.ZERO, overrides);
        assertThat(postProcessorPowDisabled.postProcess(INFO_WITH_REMOTE_POW)).contains(FEATURES_EMPTY);
        assertThat(postProcessorPowDisabled.postProcess(INFO_WITH_SOMETHING_AND_REMOTE_POW)).contains(FEATURES_SOMETHING);
    }

    @Test
    public void whenAcceptThenAssertGetNodeInfoAndOkResponse() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, false, Duration.ZERO, overrides);
        assertThat(postProcessor.accept(new GetNodeInfo(), ResponseEntity.ok("test"))).isTrue();
        assertThat(postProcessor.accept(null, ResponseEntity.ok("test"))).isFalse();
        assertThat(postProcessor.accept(new GetNodeInfo(), ResponseEntity.badRequest().build())).isFalse();
        assertThat(postProcessor.accept(null, null)).isFalse();
    }

    @Test
    public void whenUpdateGlobalNodeInfoThenCalculateNeighbors() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ZERO, overrides);
        NodeInfo result = postProcessor.updateGlobalNodeInfo(List.of(
                node(neighbors(4)), // median
                node(neighbors(132)),
                node(neighbors(3))
        ));
        assertThat(result.getNeighbors()).isEqualTo(4);
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("neighbors\":4");
    }

    @Test
    public void whenUpdateGlobalNodeInfoThenCalculateProcessors() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ZERO, overrides);
        NodeInfo result = postProcessor.updateGlobalNodeInfo(List.of(
                node(processors(4)), // median
                node(processors(32)),
                node(processors(3))
        ));
        assertThat(result.getJreAvailableProcessors()).isEqualTo(4);
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("jreAvailableProcessors\":4");
    }

    @Test
    public void whenUpdateGlobalNodeInfoThenCalculateMemory() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ZERO, overrides);
        NodeInfo result = postProcessor.updateGlobalNodeInfo(List.of(
                node(memory(1, 2, 3)),
                node(memory(16, 17, 18)),
                node(memory(16, 3, 10))
        ));

        assertThat(result.getJreTotalMemory()).isEqualTo(16);
        assertThat(result.getJreFreeMemory()).isEqualTo(3);
        assertThat(result.getJreMaxMemory()).isEqualTo(10);
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("jreTotalMemory\":16");
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("jreFreeMemory\":3");
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("jreMaxMemory\":10");
    }

    @Test
    public void whenUpdateGlobalNodeInfoThenCalculateTips() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ZERO, overrides);
        NodeInfo result = postProcessor.updateGlobalNodeInfo(List.of(
                node(tips(4)),
                node(tips(42)),
                node(tips(111))
        ));
        assertThat(result.getTips()).isEqualTo(42);
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("tips\":42");
    }

    @Test
    public void whenUpdateGlobalNodeInfoThenCalculateTransactionsToRequest() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ZERO, overrides);
        NodeInfo result = postProcessor.updateGlobalNodeInfo(List.of(
                node(txToRequest(4)),
                node(txToRequest(42)),
                node(txToRequest(111))
        ));
        assertThat(result.getTransactionsToRequest()).isEqualTo(42);
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("transactionsToRequest\":42");
    }

    @Test
    public void whenUpdateGlobalNodeInfoThenUseAtLeastTwoSyncedNodes() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ZERO, overrides);
        NodeInfo old = postProcessor.updateGlobalNodeInfo(List.empty());
        assertThat(old.getNeighbors()).isZero();
        NodeInfo result = postProcessor.updateGlobalNodeInfo(List.of(
                node(neighbors(6)),
                node(ResponseDtoBuilder.nodeInfoBuilder().neighbors(7).build()),
                node(ResponseDtoBuilder.nodeInfoBuilder().neighbors(8).build())
        ));
        assertThat(result.getNeighbors()).isZero();
        NodeInfo newInfo = postProcessor.updateGlobalNodeInfo(List.of(
                node(neighbors(6)),
                node(neighbors(7)),
                node(ResponseDtoBuilder.nodeInfoBuilder().neighbors(8).build())
        ));
        assertThat(newInfo.getNeighbors()).isEqualTo(6);
    }

    @Test
    public void whenUpdateGlobalNodeInfoThenUpdateOnlyIfOutdated() throws InterruptedException {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ofMillis(100), overrides);
        postProcessor.updateGlobalNodeInfo(List.of(
                node(tips(4)),
                node(tips(42)),
                node(tips(111))
        ));
        NodeInfo old = postProcessor.updateGlobalNodeInfo(List.of(
                node(tips(1)),
                node(tips(2)),
                node(tips(3))
        ));
        assertThat(old.getTips()).isEqualTo(42); // not second update yet
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("tips\":42");
        TimeUnit.MILLISECONDS.sleep(100);
        NodeInfo fresh = postProcessor.updateGlobalNodeInfo(List.of(
                node(tips(1)),
                node(tips(2)),
                node(tips(3))
        ));
        assertThat(fresh.getTips()).isEqualTo(2); // updated
        assertThat(postProcessor.postProcess(INFO_EMPTY)).contains("tips\":2");
    }

    @Test
    public void givenInvalidGlobalNodeInfoWhenPostProcessThenDoNotUpdateGlobalProperties() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, true, Duration.ZERO, overrides);
        assertThat(postProcessor.postProcess("{\"tips\":2}")).contains("tips\":2");
        postProcessor.updateGlobalNodeInfo(List.of(
                node(tips(42)) // one node is not enough to update global node info
        ));
        assertThat(postProcessor.postProcess("{\"tips\":2}")).contains("tips\":2");
    }

    @Test
    public void givenAccumulationDisabledWhenPostProcessThenDoNotUpdateGlobalProperties() {
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(true, false, Duration.ZERO, overrides);
        NodeInfo nodeInfo = postProcessor.updateGlobalNodeInfo(List.of(
                node(tips(42)),
                node(tips(42))
        ));
        assertThat(nodeInfo.getTips()).isZero();
        assertThat(postProcessor.postProcess("{\"tips\":3}")).contains("tips\":3");
    }

    @Test
    public void givenOverrideWhenPostProcessThenOverride() {
        when(overrides.getAppName()).thenReturn("app-name");
        when(overrides.getAppVersion()).thenReturn("app-version");
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(false, true, Duration.ZERO, overrides);
        postProcessor.updateGlobalNodeInfo(List.of(node(someValidInfo()), node(someValidInfo())));
        assertThat(postProcessor.postProcess("{}")).contains("appName\":\"app-name");
        assertThat(postProcessor.postProcess("{}")).contains("appVersion\":\"app-version");
    }

    @Test
    public void givenOverrideButNoAccumulationWhenPostProcessThenDoNotOverride() {
        when(overrides.getAppName()).thenReturn("app-name");
        when(overrides.getAppVersion()).thenReturn("app-version");
        GetNodeInfoResponsePostProcessor postProcessor = new GetNodeInfoResponsePostProcessor(false, false, Duration.ZERO, overrides);
        postProcessor.updateGlobalNodeInfo(List.of(node(someValidInfo()), node(someValidInfo())));
        assertThat(postProcessor.postProcess("{\"appName\":\"foo\"}")).contains("\"appName\":\"foo\"");
        assertThat(postProcessor.postProcess("{\"appName\":\"foo\"}")).doesNotContain("app-name");
        assertThat(postProcessor.postProcess("{\"appVersion\":\"foo\"}")).contains("\"appVersion\":\"foo\"");
        assertThat(postProcessor.postProcess("{\"appVersion\":\"foo\"}")).doesNotContain("app-version");
    }

    private Node node(NodeInfo nodeInfo) {
        Node node = mock(Node.class);
        when(node.getLatestNodeInfo()).thenReturn(nodeInfo);
        return node;
    }

    private NodeInfo someValidInfo() {
        return nodeInfo().build();
    }

    private NodeInfo tips(int tips) {
        return nodeInfo().tips(tips).build();
    }

    private NodeInfo txToRequest(int txToRequest) {
        return nodeInfo().transactionsToRequest(txToRequest).build();
    }

    private NodeInfo neighbors(int neighbors) {
        return nodeInfo().neighbors(neighbors).build();
    }

    private NodeInfo processors(int processors) {
        return nodeInfo().availableProcessors(processors).build();
    }

    private NodeInfo memory(long totalMem, long freeMem, long maxMem) {
        return nodeInfo().jreTotalMemory(totalMem).jreFreeMemory(freeMem).jreMaxMemory(maxMem).build();
    }

    private ResponseDtoBuilder.NodeInfoBuilder nodeInfo() {
        return ResponseDtoBuilder.nodeInfoBuilder()
                .latestMilestoneIndex(666)
                .latestSolidSubtangleMilestoneIndex(666);
    }


}