/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;

public class GetBalancesErrorHandlerTest {

    private final GetBalancesErrorHandler errorHandler = new GetBalancesErrorHandler();

    @Test
    public void givenNoErrorWhenIsCriticalErrorThenFalse() {
        assertThat(errorHandler.isCriticalError(HttpStatus.OK, null)).isFalse();
        assertThat(errorHandler.isCriticalError(HttpStatus.BAD_REQUEST, null)).isFalse();
    }

    @Test
    public void givenErrorWhenIsCriticalErrorThenTrue() {
        assertThat(errorHandler.isCriticalError(HttpStatus.BAD_REQUEST, "Tips are not consistent.")).isTrue();
        assertThat(errorHandler.isCriticalError(HttpStatus.INTERNAL_SERVER_ERROR, null)).isTrue();
    }

}