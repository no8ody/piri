/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import com.mio.piri.util.JsonUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static com.mio.piri.commands.response.ResponseDtoBuilder.nodeInfoBuilder;
import static org.assertj.core.api.Assertions.assertThat;

public class NodeInfoTest {

    private final NodeInfo sameMileStone = nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(42).build();
    private final NodeInfo oneOff = nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(41).build();
    private final NodeInfo notSynced = nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(40).build();

    @SuppressWarnings("SpellCheckingInspection")
    private static final String nodeInfoExample = "{" +
            "\"appName\":\"IRI Testnet\"," +
            "\"appVersion\":\"1.5.6-RELEASE\"," +
            "\"jreAvailableProcessors\":8," +
            "\"jreFreeMemory\":12537798504," +
            "\"jreVersion\":\"1.8.0_181\"," +
            "\"jreMaxMemory\":51469877248," +
            "\"jreTotalMemory\":51469877249," +
            "\"latestMilestone\":\"LADAAQL9DV9MLVJKSMVVITNGOO9IEYZXURSKGXQJUUHTQEQWQXMSBMCZKCWXYKVOHJJQZUGCSQNCQC999\"," +
            "\"latestMilestoneIndex\":1084938," +
            "\"latestSolidSubtangleMilestone\":\"LADAAQL9DV9MLVJKSMVVITNGOO9IEYZXURSKGXQJUUHTQEQWQXMSBMCZKCWXYKVOHJJQZUGCSQNCQC99A\"," +
            "\"latestSolidSubtangleMilestoneIndex\":1084939," +
            "\"milestoneStartIndex\":434525," +
            "\"neighbors\":7," +
            "\"packetsQueueSize\":0," +
            "\"time\":1548758921513," +
            "\"tips\":1477," +
            "\"transactionsToRequest\":0," +
            "\"features\":[\"snapshotPruning\",\"dnsRefresher\",\"testnet\",\"zeroMessageQueue\",\"tipSolidification\",\"RemotePOW\"]," +
            "\"coordinatorAddress\":\"EQQFCZBIHRHWPXKMTOLMYUYPCN9XLMJPYZVFJSAY9FQHCCLWTOLLUGKKMXYFDBOOYFBLBI9WUEILGECYM\"," +
            "\"duration\":0" +
            "}";

    @Test
    public void whenIsSyncedThenReturnSyncStatus() {
        assertThat(sameMileStone.isSynced()).isTrue();
        assertThat(oneOff.isSynced()).isTrue();
        assertThat(notSynced.isSynced()).isFalse();
    }

    @Test
    public void whenCreateWithoutArgumentsThenIsNotSynced() {
        NodeInfo empty = nodeInfoBuilder().build();
        assertThat(empty.isSynced()).isFalse();
    }

    @Test
    public void testDeserialization() {
        assertThat(JsonUtil.toJson(nodeInfoBuilder().build())).doesNotContain("appName");
        assertThat(JsonUtil.toJson(nodeInfoBuilder().build())).doesNotContain("appVersion");
        assertThat(JsonUtil.toJson(nodeInfoBuilder().build()).toLowerCase()).doesNotContain("synced");
        assertThat(JsonUtil.toJson(nodeInfoBuilder().build()).toLowerCase()).doesNotContain("valid");
        assertThat(JsonUtil.toJson(nodeInfoBuilder().appName("foo").build()))
                .contains("\"appName\":\"foo\"");
        assertThat(JsonUtil.toJson(nodeInfoBuilder().appVersion("foo").build()))
                .contains("\"appVersion\":\"foo\"");
        assertThat(JsonUtil.toJson(nodeInfoBuilder().latestSolidSubtangleMilestoneIndex(42).build()))
                .contains("\"latestSolidSubtangleMilestoneIndex\":42");
        assertThat(JsonUtil.toJson(nodeInfoBuilder().latestMilestoneIndex(42).build()))
                .contains("\"latestMilestoneIndex\":42");
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void whenFromJsonThenAllPropertiesAreSet() {
        NodeInfo nodeInfo = JsonUtil.fromJson(nodeInfoExample, NodeInfo.class);
        assertThat(nodeInfo.getAppName()).isEqualTo("IRI Testnet");
        assertThat(nodeInfo.getAppVersion()).isEqualTo("1.5.6-RELEASE");
        assertThat(nodeInfo.getJreAvailableProcessors()).isEqualTo(8);
        assertThat(nodeInfo.getJreFreeMemory()).isEqualTo(12537798504L);
        assertThat(nodeInfo.getJreVersion()).isEqualTo("1.8.0_181");
        assertThat(nodeInfo.getJreMaxMemory()).isEqualTo(51469877248L);
        assertThat(nodeInfo.getJreTotalMemory()).isEqualTo(51469877249L);
        assertThat(nodeInfo.getLatestMilestone()).isEqualTo("LADAAQL9DV9MLVJKSMVVITNGOO9IEYZXURSKGXQJUUHTQEQWQXMSBMCZKCWXYKVOHJJQZUGCSQNCQC999");
        assertThat(nodeInfo.getLatestMilestoneIndex()).isEqualTo(1084938);
        assertThat(nodeInfo.getLatestSolidSubtangleMilestone()).isEqualTo("LADAAQL9DV9MLVJKSMVVITNGOO9IEYZXURSKGXQJUUHTQEQWQXMSBMCZKCWXYKVOHJJQZUGCSQNCQC99A");
        assertThat(nodeInfo.getLatestSolidSubtangleMilestoneIndex()).isEqualTo(1084939);
        assertThat(nodeInfo.getMilestoneStartIndex()).isEqualTo(434525);
        assertThat(nodeInfo.getNeighbors()).isEqualTo(7);
        assertThat(nodeInfo.getPacketsQueueSize()).isEqualTo(0);
        assertThat(nodeInfo.getTime()).isEqualTo(1548758921513L);
        assertThat(nodeInfo.getTips()).isEqualTo(1477);
        assertThat(nodeInfo.getTransactionsToRequest()).isEqualTo(0);
        assertThat(nodeInfo.getFeatures()).containsExactly("snapshotPruning", "dnsRefresher", "testnet", "zeroMessageQueue", "tipSolidification", "RemotePOW");
        assertThat(nodeInfo.getCoordinatorAddress()).isEqualTo("EQQFCZBIHRHWPXKMTOLMYUYPCN9XLMJPYZVFJSAY9FQHCCLWTOLLUGKKMXYFDBOOYFBLBI9WUEILGECYM");
        assertThat(nodeInfo.getDuration()).isEqualTo(0);
    }

    @Test
    public void whenFromJsonAndBackThenEquals() {
        NodeInfo nodeInfo = JsonUtil.fromJson(nodeInfoExample, NodeInfo.class);
        String back = JsonUtil.toJson(nodeInfo);
        assertThat(nodeInfoExample).isEqualTo(back);
    }

    @Test
    public void givenNoFeaturesWhenSupportsPowThenFalse() {
        NodeInfo nodeInfo = nodeInfoBuilder().build();
        assertThat(nodeInfo.supportsPow()).isFalse();
    }

    @Test
    public void givenNoPowFeatureWhenSupportsPowThenFalse() {
        NodeInfo nodeInfo = nodeInfoBuilder().features(Arrays.asList("foo", "bar")).build();
        assertThat(nodeInfo.supportsPow()).isFalse();
    }

    @Test
    public void givenPowFeatureWhenSupportsPowThenTrue() {
        NodeInfo nodeInfo = nodeInfoBuilder().features(Arrays.asList("foo", "RemotePOW", "bar")).build();
        assertThat(nodeInfo.supportsPow()).isTrue();
    }

    @Test
    public void whenAddFeatureThenCreateSet() {
        NodeInfo nodeInfo = nodeInfoBuilder().build();
        assertThat(nodeInfo.getFeatures()).isNull();
        nodeInfo.addFeature("foo");
        assertThat(nodeInfo.getFeatures()).contains("foo");
    }

    @Test
    public void whenRemoveFeaturesThenRemoveFromSet() {
        NodeInfo nodeInfo = nodeInfoBuilder().features(Collections.singletonList("foo")).build();
        nodeInfo.removeFeature("foo");
        nodeInfo.removeFeature("bar");
        assertThat(nodeInfo.getFeatures()).isEmpty(); // could this be a problem in the future that features is not null now?
    }

}