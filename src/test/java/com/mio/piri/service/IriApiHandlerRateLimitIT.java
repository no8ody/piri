/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.*;
import com.mio.piri.util.TestCommandFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "piri.rate.ip.limited.commands=getTips,getTrytes,getNodeInfo,findTransactions",
        "piri.rate.ip.getNodeInfo.limit=1",
        "piri.rate.ip.getNodeInfo.period=1m",
        "piri.rate.ip.getTips.limit=2",
        "piri.rate.ip.getTips.weight=3",
        "piri.rate.ip.getTips.period=1m",
        "piri.rate.ip.getTrytes.limit=4",
        "piri.rate.ip.getTrytes.weight=2",
        "piri.rate.ip.getTrytes.period=1m",
        "piri.rate.ip.findTransactions.limit=2",
        "piri.rate.ip.findTransactions.period=1m",
        "iri.allow.getNeighbors=true", // dummy for cleaning response queue of test endpoint
        "iri.nodes="}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerRateLimitIT extends AbstractIriApiHandlerTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void givenRateLimitTriggeredWhenExchangeThenTooManyRequests() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        util.postCommand(new GetNodeInfo()).expectStatus().isOk(); // rate limit not reached yet

        String responseBody = util.postCommand(new GetNodeInfo())
                .expectStatus().isEqualTo(HttpStatus.TOO_MANY_REQUESTS)
                .expectBody(String.class).returnResult().getResponseBody();

        logger.debug(responseBody);
        consumeLastResponse();
    }

    @Test
    public void givenBadRequestWhenExchangeThenCountWithoutWeight() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setResponseCode(400)
                .setBody(syncedNodeInfo));

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setResponseCode(400)
                .setBody(syncedNodeInfo));

        // to make sure 429 is not triggered in retry because of timeout
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        GetTips command = TestCommandFactory.getTips();
        util.postCommand(command).expectStatus().isEqualTo(HttpStatus.BAD_REQUEST); // rate limit not reached yet
        util.postCommand(command).expectStatus().isEqualTo(HttpStatus.BAD_REQUEST); // rate limit not reached yet

        String responseBody = util.postCommand(command)
                .expectStatus().isEqualTo(HttpStatus.TOO_MANY_REQUESTS)
                .expectBody(String.class).returnResult().getResponseBody();

        logger.debug(responseBody);
        consumeLastResponse();
    }

    @Test
    public void givenOkWhenExchangeThenCountWithWeight() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        // to make sure 429 is not triggered in retry because of timeout
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));


        GetTrytes command = TestCommandFactory.getTrytes("blah");
        util.postCommand(command).expectStatus().isEqualTo(HttpStatus.OK); // rate limit not reached yet
        util.postCommand(command).expectStatus().isEqualTo(HttpStatus.OK); // rate limit not reached yet

        String responseBody = util.postCommand(command)
                .expectStatus().isEqualTo(HttpStatus.TOO_MANY_REQUESTS)
                .expectBody(String.class).returnResult().getResponseBody();

        logger.debug(responseBody);
        consumeLastResponse();
    }

    @DirtiesContext // reset rate limit
    @Test
    public void givenMultipleTrytesThenCountWithWeight() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        // to make sure 429 is not triggered in retry because of timeout
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        FindTransactions command = TestCommandFactory.findTransactionsByAddress("blah", "blah"); // counts for two.
        util.postCommand(command).expectStatus().isEqualTo(HttpStatus.OK); // rate limit not reached yet

        String responseBody = util.postCommand(TestCommandFactory.findTransactionsByAddress("blah"))
                .expectStatus().isEqualTo(HttpStatus.TOO_MANY_REQUESTS) // third "blah" triggers rate limit.
                .expectBody(String.class).returnResult().getResponseBody();

        logger.debug(responseBody);
        consumeLastResponse();

    }

    @DirtiesContext // reset rate limit
    @Test
    public void givenMultipleTrytesThenTriggerRateLimit() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        // to make sure 429 is not triggered in retry because of timeout
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        FindTransactions command = TestCommandFactory.findTransactionsByAddress("blah", "blah", "blah"); // counts for three.

        String responseBody = util.postCommand(command)
                .expectStatus().isEqualTo(HttpStatus.TOO_MANY_REQUESTS) // third "blah" triggers rate limit.
                .expectBody(String.class).returnResult().getResponseBody();

        logger.debug(responseBody);
        consumeLastResponse();

    }

    // this method makes sure there is no stale response waiting on the test endpoint.
    private void consumeLastResponse() {
        IriCommand dummy = TestCommandFactory.getNeighborsCommand(); // some command that is not rate limited
        util.postCommand(dummy).expectStatus().isEqualTo(HttpStatus.OK);
    }


}