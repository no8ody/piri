/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.util.TestCommandFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"iri.allow.attachToTangle=true", "iri.nodes="}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerAttachToTangleIT extends AbstractIriApiHandlerTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String FEATURES_REMOTE_POW = "\"features\":[\"RemotePOW\"]";

    @Test
    public void givenNoNodeForPowWhenExchangeThenTooManyRequests() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        util.postCommand(new GetNodeInfo()).expectStatus().isOk(); // get node info is supported by node

        // for pow we do not find a node that supports this command
        String responseBody = util.postCommand(TestCommandFactory.attachToTangleCommand("a", "b", "c"))
                .expectStatus().isEqualTo(HttpStatus.TOO_MANY_REQUESTS)
                .expectBody(String.class).returnResult().getResponseBody();
        logger.debug("Response body: {}", responseBody);
    }

    @Test
    public void givenGetNodeInfoWhenExchangeThenReturnRemotePowEnabled() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        String body = util.postCommand(new GetNodeInfo())
                .expectStatus().isOk()
                .expectBody(String.class).returnResult().getResponseBody();

        // feature "RemotePOW" should have been added
        assertThat(body).contains(FEATURES_REMOTE_POW);
        logger.debug("Response body: {}", body);
    }

    @Override
    protected boolean powEnabled() {
        return false;
    }

}