/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.GetBalances;
import com.mio.piri.commands.GetTransactionsToApprove;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.util.IriClientTestUtility;
import com.mio.piri.util.responses.GetTransactionsToApproveResponse;
import com.mio.piri.util.responses.ResponseHashes;
import com.mio.piri.util.responses.ResponseTrytes;
import io.micrometer.core.instrument.Timer;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static com.mio.piri.commands.IriCommands.GET_NODE_INFO;
import static com.mio.piri.util.TestCommandFactory.*;
import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("SpellCheckingInspection")
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "iri.nodes=test",
        "iri.test.url=http://localhost:14265",
        "iri.test.pow=true",
        "iri.allow.loopback.bypass=true",
        "iri.allow.attachToTangle=true", // false by default
        "iri.allow.interruptAttachingToTangle=true", // false by default
        "iri.allow.getNeighbors=true", // false by default
        "iri.allow.removeNeighbors=true", // false by default
        "iri.allow.addNeighbors=true", // false by default
        "iri.allow.getMissingTransactions=true" // false by default
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerCommandsSysIT {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MeterFactory meterFactory;

    private IriClientTestUtility util;

    @Before
    public void initTestUtils() {
        util = new IriClientTestUtility(webTestClient);
    }


    // FIXME as nodes are checked for sync base on commands and no health check did run yet we don't find any nodes.
    // Solution: add health check on node registration and/or make defaults so that node seems to be 'synced' and not delayed.

    @Test
    public void whenPostThenRecordTime() {
        Timer timer = meterFactory.createCommandTimer(GET_NODE_INFO.getKey(), "test", "test");
        long count = timer.count();
        double start = timer.totalTime(TimeUnit.MILLISECONDS);
        int minimumDuration = 2; // we assume that the call takes at leas this long

        postCommandExpectOk(command(GET_NODE_INFO.getKey()));

        logger.info("Duration: {}", timer.totalTime(TimeUnit.MILLISECONDS) - start);
        Assertions.assertThat(timer.count()).isEqualTo(count + 1);
        Assertions.assertThat(timer.totalTime(TimeUnit.MILLISECONDS)).isGreaterThan(start + minimumDuration);
    }

    @Test
    public void givenGetNodeInfoWhenPostThenReturnIriNodeInfo() {
        String result = postCommandExpectOk(command(GET_NODE_INFO.getKey()));
        assertThat(result).startsWith("{\"appName\":\"IRI\"");
    }

    @Test
    public void givenGetTipsWhenPostThenReturnTips() {
        String result = postCommandExpectOk(command("getTips"));
        assertThat(result).startsWith("{\"hashes\":[\"");
    }

    @Test
    public void givenFindTransactionsWhenPostThenReturnHashes() {
        NodeInfo nodeInfo = postCommandExpectOk(command(GET_NODE_INFO.getKey()), NodeInfo.class);
        // might fail after snapshot
        String result = postCommandExpectOk(findTransactionsByApprover(nodeInfo.getLatestSolidSubtangleMilestone()));
        assertThat(result).startsWith("{\"hashes\":[\"");
    }

    @Test
    public void givenGetTrytesWhenPostThenReturnHashes() {
        String result = postCommandExpectOk(getTrytes("BDHIGZXHYDNTVQVNIRYYLNPMZITPOUXBJPHCVXYTTURNR9XGAVKMAAJARQPFYPLWEXKEHXYAVLOE99999"));
        assertThat(result).startsWith("{\"trytes\":[\"");
    }

    @Test
    public void givenGetInclusionStatesWhenPostThenReturnStates() {
        ResponseHashes tips = util.postCommand(command("getTips")).expectStatus().isOk().expectBody(ResponseHashes.class).returnResult().getResponseBody();
        assert tips != null && tips.getHashes() != null;
        String tx = tips.getHashes().get(0);
        String result = postCommandExpectOk(getInclusionStates(tx, tx));
        assertThat(result).startsWith("{\"states\":[true]"); // same tx naturally includes itself
    }

//    @Test
//    public void givenGetBalancesWhenPostThenReturnBalances() {
//        String address = "NAFAEIOFEAIJKKDMDVDEXKCDBIMSHIAFSMURU9WDFRVPKEZX9AKGYYOJSXKOAXRPNPJTDDRWXFRADKEDC";
//        String result = postCommandExpectOk(getBalances(address));
//        assertThat(result).startsWith("{\"balances\":[");
//    }

    @Test
    public void givenGetBalancesWhenPostThenReturnBalances() {
        GetBalances getBalances = getBalances();
        String[] addresses = {
                "HKRAKXCWXMYGP9IADTSVUMKPW9HYICJNDDTBPTPJF9XBEEHFQAMWLXMNGUBRDAFOKFYRDWGPPZV9SLLBC",
                "RHFDHFAJNHVKPTFEBRVZMRKAVTOKTUYGRXPI99XZBZNQ9CGKVXBHALGIJOTELBHPFQWZEDSBGCWWXNJLY",
        };
        getBalances.setAddresses(Arrays.asList(addresses));
        String result = postCommandExpectOk(getBalances);
        assertThat(result).startsWith("{\"balances\":[");
        logger.info(result);
    }

    @Test
    public void givenGetTransactionsToApproveWhenPostThenReturnTrunkAndBranchTx() {
        GetTransactionsToApprove gtta = getTransactionsToApprove();
        GetTransactionsToApproveResponse result = postCommandExpectOk(gtta, GetTransactionsToApproveResponse.class);
        Assertions.assertThat(result.getTrunkTransaction()).isNotBlank();
        Assertions.assertThat(result.getBranchTransaction()).isNotBlank();
    }

    @Test
    public void givenAttachToTangleWhenPostThenReturnTrytes() {
        GetTransactionsToApproveResponse gtta = postCommandExpectOk(getTransactionsToApprove(), GetTransactionsToApproveResponse.class);

        String firstTrytes = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA999999999999999999999999999CTYTETAG9999999999999999999KPQBABD99999999999A99999999OKXONXP9FERLWZYFZJEJWSIOVGZOHOXXYIJJNEJVBD9EZWHGTSWQJXB9WABGKNIVYVERUCXFWKPIRDSHD999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999TRYTETAG9999999999999999999999999999999999999999999999999999999999999999999999999";
        String secondTrytes = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB999999999999999999999999999TRYTETAG9999999999999999999KPQBABD99A99999999A99999999OKXONXP9FERLWZYFZJEJWSIOVGZOHOXXYIJJNEJVBD9EZWHGTSWQJXB9WABGKNIVYVERUCXFWKPIRDSHD999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999TRYTETAG9999999999999999999999999999999999999999999999999999999999999999999999999";

        AttachToTangle att = attachToTangleCommand(gtta.getTrunkTransaction(), gtta.getBranchTransaction(),
                firstTrytes, secondTrytes);
//
//        for (int i = 0; i < 5; i++) {
//            String result = util.postCommand("https://pool.trytes.eu", att, 5)
//                    .expectStatus().isOk()
//                    .expectBody(String.class).returnResult().getResponseBody();
//            logger.debug(result);
//            assertThat(result).isNotBlank();
//            assertThat(result).startsWith("{\"trytes\":[\"AAA");
//        }

        String result = util.postCommand("/", att, 45)
                .expectStatus().isOk()
                .expectBody(String.class).returnResult().getResponseBody();
        logger.debug(result);
        assertThat(result).isNotNull();
        assertThat(result).startsWith("{\"trytes\":");
        // assertThat(result).startsWith("{\"trytes\":[\"AAA"); this is not always true. depends on implementation currently (PoW nodes vs IRI)
    }

    @Test
    public void givenInterruptAttachingToTangleWhenPostThenOk() {
        String result = postCommandExpectOk(command("interruptAttachingToTangle"));
        assertThat(result).contains("duration");
    }

    @Test
    public void givenBroadcastTransactionsWhenPostThenOk() {
        NodeInfo nodeInfo = postCommandExpectOk(command(GET_NODE_INFO.getKey()), NodeInfo.class);
        ResponseTrytes responseTrytes = postCommandExpectOk(getTrytes(nodeInfo.getLatestSolidSubtangleMilestone()), ResponseTrytes.class); // convert to trytes
        String trytes = responseTrytes.getTrytes().get(0);
        String result = postCommandExpectOk(broadcastTransactions(trytes));
        assertThat(result).contains("duration");
    }

    @Test
    public void givenStoreTransactionsWhenPostThenOk() {
        NodeInfo nodeInfo = postCommandExpectOk(command(GET_NODE_INFO.getKey()), NodeInfo.class);
        ResponseTrytes responseTrytes = postCommandExpectOk(getTrytes(nodeInfo.getLatestSolidSubtangleMilestone()), ResponseTrytes.class); // convert to trytes
        String trytes = responseTrytes.getTrytes().get(0);
        String result = postCommandExpectOk(storeTransactions(trytes));
        assertThat(result).contains("duration");
    }

    @Test
    public void givenWereAddressesSpentFromWhenPostThenReturnStates() {
        String address = "UIQJD9ADKNZYSL9CCJULJQNWTZ9FWGVQMUFSXDK9SJVINWQOVGKXLMSNWJGCCMAQRUTSETKUSCFXGMTHA";
        String result = postCommandExpectOk(wereAddressesSpentFrom(address));
        assertThat(result).startsWith("{\"states\":");
    }

    @Test
    public void givenCheckConsistencyWhenPostThenOk() {
        NodeInfo nodeInfo = postCommandExpectOk(command(GET_NODE_INFO.getKey()), NodeInfo.class);
        String result = postCommandExpectOk(checkConsistency(nodeInfo.getLatestSolidSubtangleMilestone()));
        assertThat(result).startsWith("{\"state\":"); // could be false, too. in case of max depth, ...
    }

    @Test
    public void givenGetMissingTransactionsWhenPostThenReturnTips() {
        String result = postCommandExpectOk(command("getMissingTransactions"));
        assertThat(result).startsWith("{\"hashes\":[");
    }

    @Test
    public void givenGetNeighborsWhenPostThenReturnNeighbors() {
        String result = postCommandExpectOk(getNeighborsCommand());
        assertThat(result).startsWith("{\"neighbors\":[");
    }

    @Test
    public void givenAddNeighborsWhenPostThenReturnNumberOfAdded() {
        String result = postCommandExpectOk(addNeighborsCommand("udp://8.8.8.8:14600"));
        assertThat(result).startsWith("{\"addedNeighbors\":");
        util.postCommand(removeNeighborsCommand("udp://8.8.8.8:14600")).expectStatus().isOk(); // clean up
    }

    @Test
    public void givenRemoveNeighborsWhenPostThenReturnNumberOfRemoved() {
        String result = postCommandExpectOk(removeNeighborsCommand("udp://8.8.8.8:14600"));
        assertThat(result).startsWith("{\"removedNeighbors\":");
    }

    // Attach to tangle validation errors:

    @Test
    public void givenEmptyAttachToTangleThenSimulateIriBehaviour() {
        AttachToTangle att = attachToTangleCommand(null, null, (String[]) null);
        att.setMinWeightMagnitude(1);
        String result = util.postCommand(att)
                .expectStatus()
                .isBadRequest()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody(String.class).returnResult().getResponseBody();
        logger.info(result);
        assertThat(result).contains("{\"error\":\"Invalid parameters\",\"duration\":");
    }

    // behaviour is not 100 percent compatible to IRI as it returns {"error":"Invalid parameters","duration":"...
    @Test
    public void givenInvalidWeightWhenAttachToTangleThenDoNotSimulateIriBehaviourButReturnValidationErrors() {
        AttachToTangle att = attachToTangleCommand("foo", "bar", "foobar");
        att.setMinWeightMagnitude(1);
        String result = util.postCommand(att)
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        logger.info(result);
        assertThat(result).contains("\"error\":\"Bad Request");
        assertThat(result).contains("\"message\":\"Validation failure");
    }

    private <T> T postCommandExpectOk(Object command, Class<T> responseClass) {
        if (command instanceof IriCommand) {
            logger.debug("request: {}", toJson((IriCommand) command));
        } else {
            logger.debug("request: {}", command);
        }
        T result = util.postCommand(command)
                .expectStatus().isOk()
                .expectBody(responseClass).returnResult().getResponseBody();
        assertThat(result).isNotNull();
        logger.debug("result: {}", result.toString());
        return result;
    }

    private String postCommandExpectOk(Object command) {
        return postCommandExpectOk(command, String.class);
    }


    private String toJson(IriCommand command) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(command);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}