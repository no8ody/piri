/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.*;
import com.mio.piri.util.TestCommandFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "iri.nodes=",
        "iri.allow.attachToTangle=true",
        "iri.getTransactionsToApprove.depth.min=10",
        "iri.attachToTangle.trytes.max=10",
        "iri.getTrytes.hashes.max=10",
        "iri.findTransactions.addresses.max=10",
        "iri.findTransactions.bundles.max=11",
        "iri.findTransactions.tags.max=12",
        "iri.findTransactions.approvees.max=13",
        "iri.wereAddressesSpentFrom.addresses.max=10",
        "iri.getInclusionStates.transactions.max=10",
        "iri.getInclusionStates.tips.max=11",
        "iri.checkConsistency.tails.max=10",
        "iri.getBalances.addresses.max=10",
        "iri.getBalances.tips.max=11",
        "iri.storeTransactions.trytes.max=12",
        "iri.broadcastTransactions.trytes.max=13"
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerCommandValidationIT extends AbstractIriApiHandlerTest {

    @Test
    public void givenAttachToTangleWhenValidateThenError() {
        AttachToTangle attachToTangle = TestCommandFactory.attachToTangleCommand("foo", "bar",
                Collections.nCopies(11, "foo").toArray(new String[11]));
        String result = util.postCommand(attachToTangle)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("trytes size > 10");
    }

    @Test
    public void givenGetTransactionsToApproveWhenValidateThenError() {
        GetTransactionsToApprove gTTA = TestCommandFactory.getTransactionsToApprove();
        gTTA.setDepth(0);
        String result = util.postCommand(gTTA)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("depth < 10");
    }

    @Test
    public void givenGetTrytesWhenValidateThenError() {
        GetTrytes getTrytes = TestCommandFactory.getTrytes();
        getTrytes.setHashes(Collections.nCopies(11, "foo"));
        String result = util.postCommand(getTrytes)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("hashes size > 10");
    }

    @Test
    public void givenFindTransactionsWhenValidateThenError() {
        FindTransactions ft = TestCommandFactory.findTransactions();
        ft.setAddresses(Collections.nCopies(11, "foo"));
        ft.setBundles(Collections.nCopies(12, "foo"));
        ft.setTags(Collections.nCopies(13, "foo"));
        ft.setApprovees(Collections.nCopies(14, "foo"));
        String result = util.postCommand(ft)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("addresses size > 10");
        assertThat(result).contains("bundles size > 11");
        assertThat(result).contains("tags size > 12");
        assertThat(result).contains("approvees size > 13");
    }

    @Test
    public void givenWereAddressesSpentFromWhenValidateThenError() {
        WereAddressesSpentFrom command = TestCommandFactory.wereAddressesSpentFrom();
        command.setAddresses(Collections.nCopies(11, "foo"));
        String result = util.postCommand(command)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("addresses size > 10");
    }

    @Test
    public void givenGetInclusionStatesWhenValidateThenError() {
        GetInclusionStates command = TestCommandFactory.getInclusionStates();
        command.setTransactions(Collections.nCopies(11, "foo"));
        command.setTips(Collections.nCopies(12, "bar"));
        String result = util.postCommand(command)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("transactions size > 10");
        assertThat(result).contains("tips size > 11");
    }

    @Test
    public void givenCheckConsistencyWhenValidateThenError() {
        CheckConsistency command = TestCommandFactory.checkConsistency();
        command.setTails(Collections.nCopies(11, "foo"));
        String result = util.postCommand(command)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("tails size > 10");
    }

    @Test
    public void givenGetBalancesWhenValidateThenError() {
        GetBalances command = TestCommandFactory.getBalances();
        command.setAddresses(Collections.nCopies(11, "foo"));
        command.setTips(Collections.nCopies(12, "bar"));
        String result = util.postCommand(command)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("addresses size > 10");
        assertThat(result).contains("tips size > 11");
    }

    @Test
    public void givenStoreTransactionsWhenValidateThenError() {
        StoreTransactions command = TestCommandFactory.storeTransactions();
        command.setTrytes(Collections.nCopies(13, "foo"));
        String result = util.postCommand(command)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("trytes size > 12");
    }

    @Test
    public void givenBroadcastTransactionsWhenValidateThenError() {
        BroadcastTransactions command = TestCommandFactory.broadcastTransactions();
        command.setTrytes(Collections.nCopies(14, "foo"));
        String result = util.postCommand(command)
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("trytes size > 13");
    }

}