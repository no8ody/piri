/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.WereAddressesSpentFrom;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class WereAddressesSpentFromValidatorTest {

    private WereAddressesSpentFromValidator validator;

    private final WereAddressesSpentFrom command = mock(WereAddressesSpentFrom.class);
    private final MinMaxValidator minMaxValidator = mock(MinMaxValidator.class);
    private final Errors errors = mock(Errors.class);

    @Before
    public void initEnvironment() {
        Environment env = mock(Environment.class);
        when(env.getProperty("iri.wereAddressesSpentFrom.addresses.max", Integer.class, Integer.MAX_VALUE)).thenReturn(1);
        validator = new WereAddressesSpentFromValidator(env, minMaxValidator);
    }

    @Test
    public void whenSupportsThenTrue() {
        assertThat(validator.supports(WereAddressesSpentFrom.class)).isTrue();
    }

    @Test
    public void whenSupportsThenFalse() {
        assertThat(validator.supports(Object.class)).isFalse();
        assertThat(validator.supports(IriCommand.class)).isFalse();
    }

    @Test
    public void givenEmptyWhenValidateThenDoNotThrow() {
        // make sure that no NPE happens
        validator.validate(new WereAddressesSpentFrom(), errors);
    }

    @Test
    public void whenValidateThenValidateAddresses() {
        when(command.getAddresses()).thenReturn(Arrays.asList("foo", "bar"));
        validator.validate(command, errors);
        verify(minMaxValidator).validateMaxSize(2, 1, "addresses", errors);
    }

}