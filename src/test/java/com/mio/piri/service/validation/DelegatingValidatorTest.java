/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.GetNeighbors;
import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.GetTips;
import com.mio.piri.commands.IriCommand;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.Errors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class DelegatingValidatorTest {

    private final IriCommandValidator validator1 = mock(IriCommandValidator.class);
    private final IriCommandValidator validator2 = mock(IriCommandValidator.class);

    private DelegatingValidator validator;

    @Before
    public void setupMocks() {
        when(validator1.getSupportedCommand()).thenReturn(GetTips.class);
        when(validator1.supports(GetTips.class)).thenReturn(true);
        when(validator2.getSupportedCommand()).thenReturn(GetNodeInfo.class);
        when(validator2.supports(GetNodeInfo.class)).thenReturn(true);
        validator = new DelegatingValidator(IriCommand.class, validator1, validator2);
    }

    @Test
    public void givenValidClassWhenSupportsThenReturnTrue() {
        assertThat(validator.supports(IriCommand.class)).isTrue();
        assertThat(validator.supports(GetNodeInfo.class)).isTrue();
        assertThat(validator.supports(GetTips.class)).isTrue();
    }

    @Test
    public void givenUnsupportedClassWhenSupportsThenReturnFalse() {
        assertThat(validator.supports(Object.class)).isFalse();
    }

    @Test
    public void whenValidateThenCallMatchingValidator() {
        validator.validate(new GetNodeInfo(), mock(Errors.class));
        verify(validator2).validate(any(), any(Errors.class));
        verify(validator1, never()).validate(any(), any());
    }

    @Test
    public void givenNoValidatorWhenValidateThenIgnore() {
        validator.validate(new GetNeighbors(), mock(Errors.class));
        verify(validator1, never()).validate(any(), any());
        verify(validator2, never()).validate(any(), any());
    }

}