/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.GetTrytes;
import com.mio.piri.commands.IriCommand;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class GetTrytesValidatorTest {

    private GetTrytesValidator validator;

    private final MinMaxValidator minMaxValidator = mock(MinMaxValidator.class);
    private final GetTrytes command = mock(GetTrytes.class);
    private final Errors errors = mock(Errors.class);

    @Before
    public void initEnvironment() {
        Environment env = mock(Environment.class);
        when(env.getProperty("iri.getTrytes.hashes.max", Integer.class, Integer.MAX_VALUE))
                .thenReturn(42);
        validator = new GetTrytesValidator(env, minMaxValidator);
    }

    @Test
    public void givenGetTrytesWhenSupportsThenTrue() {
        assertThat(validator.supports(GetTrytes.class)).isTrue();
    }

    @Test
    public void givenOtherClassWhenSupportsThenFalse() {
        assertThat(validator.supports(Object.class)).isFalse();
        assertThat(validator.supports(IriCommand.class)).isFalse();
    }

    @Test
    public void whenValidateThenCallMinMaxValidator() {
        when(command.getHashes()).thenReturn(Arrays.asList("foo", "bar"));
        validator.validate(command, errors);
        verify(minMaxValidator).validateMaxSize(2, 42, "hashes", errors);
    }

}