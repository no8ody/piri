/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.FindTransactions;
import com.mio.piri.commands.IriCommand;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class FindTransactionsValidatorTest {

    private FindTransactionsValidator validator;

    private final MinMaxValidator minMaxValidator = mock(MinMaxValidator.class);
    private final FindTransactions ft = mock(FindTransactions.class);
    private final Errors errors = mock(Errors.class);

    @Before
    public void initEnvironment() {
        Environment env = mock(Environment.class);
        when(env.getProperty("iri.findTransactions.addresses.max", Integer.class, Integer.MAX_VALUE)).thenReturn(1);
        when(env.getProperty("iri.findTransactions.bundles.max", Integer.class, Integer.MAX_VALUE)).thenReturn(2);
        when(env.getProperty("iri.findTransactions.tags.max", Integer.class, Integer.MAX_VALUE)).thenReturn(3);
        when(env.getProperty("iri.findTransactions.approvees.max", Integer.class, Integer.MAX_VALUE)).thenReturn(4);
        validator = new FindTransactionsValidator(env, minMaxValidator);
    }

    @Test
    public void whenSupportsThenTrue() {
        assertThat(validator.supports(FindTransactions.class)).isTrue();
    }

    @Test
    public void whenSupportsThenFalse() {
        assertThat(validator.supports(Object.class)).isFalse();
        assertThat(validator.supports(IriCommand.class)).isFalse();
    }

    @Test
    public void givenEmptyWhenValidateThenDoNotThrow() {
        // make sure that no NPE happens
        validator.validate(new FindTransactions(), errors);
    }

    @Test
    public void whenValidateThenValidateAddresses() {
        when(ft.getAddresses()).thenReturn(Arrays.asList("foo", "bar"));
        validator.validate(ft, errors);
        verify(minMaxValidator).validateMaxSize(2, 1, "addresses", errors);
    }

    @Test
    public void whenValidateThenValidateBundles() {
        when(ft.getBundles()).thenReturn(Arrays.asList("foo", "bar"));
        validator.validate(ft, errors);
        verify(minMaxValidator).validateMaxSize(2, 2, "bundles", errors);
    }

    @Test
    public void whenValidateThenValidateTags() {
        when(ft.getTags()).thenReturn(Arrays.asList("foo", "bar"));
        validator.validate(ft, errors);
        verify(minMaxValidator).validateMaxSize(2, 3, "tags", errors);
    }

    @Test
    public void whenValidateThenValidateApprovees() {
        when(ft.getApprovees()).thenReturn(Arrays.asList("foo", "bar"));
        validator.validate(ft, errors);
        verify(minMaxValidator).validateMaxSize(2, 4, "approvees", errors);
    }

}