/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import org.junit.Test;
import org.springframework.validation.Errors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

public class MinMaxValidatorTest {

    private final MinMaxValidator validator = new MinMaxValidator();

    private final Errors errors = mock(Errors.class);

    @Test
    public void whenValidateThenAddNoError() {
        validator.validate(42, 42, 42, "foo", errors);
        verifyNoInteractions(errors);
    }

    @Test
    public void givenBelowMinWhenValidateThenAddError() {
        validator.validate(41, 42, 42, "foo", errors);
        verify(errors).rejectValue("foo", "Min", MinMaxValidator.MIN_MESSAGE + 42);
    }

    @Test
    public void givenAboveMaxWhenValidateThenAddError() {
        validator.validate(43, 42, 42, "foo", errors);
        verify(errors).rejectValue("foo", "Max", MinMaxValidator.MAX_MESSAGE + 42);
    }

    @Test
    public void givenZeroWhenMaxSizeThenNoError() {
        validator.validateMaxSize(0, 10, "foo", errors);
        verifyNoInteractions(errors);
    }

    @Test
    public void givenBelowWhenMaxSizeThenAddNoError() {
        validator.validateMaxSize(41, 42, "foo", errors);
        verifyNoInteractions(errors);
    }

    @Test
    public void givenAboveWhenMaxSizeThenError() {
        validator.validateMaxSize(43, 42, "foo", errors);
        verify(errors).rejectValue("foo", "Size", MinMaxValidator.MAX_SIZE_MESSAGE + 42);
    }

}