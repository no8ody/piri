/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.GetTransactionsToApprove;
import com.mio.piri.commands.GetTrytes;
import com.mio.piri.util.IriClientTestUtility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.mio.piri.util.TestCommandFactory.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "iri.allow.attachToTangle=false",
        "iri.allow.interruptAttachingToTangle=false",
        "iri.allow.getNeighbors=false",
        "iri.allow.removeNeighbors=false",
        "iri.allow.addNeighbors=false",
        "iri.getTransactionsToApprove.depth.min=2",
        "iri.attachToTangle.minWeightMagnitude.min=2",
        "iri.getTrytes.hashes.max=1"
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerErrorsSysIT {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private WebTestClient webTestClient;

    private IriClientTestUtility util;

    @Before
    public void initTestUtils() {
        util = new IriClientTestUtility(webTestClient);
    }

    @Test
    public void givenAttachToTangleWhenNotAllowedThen401() {
        assertUnsupportedCommand(attachToTangleCommand("FOO", "BAR", "FOOBAR"));
    }

    @Test
    public void givenRemoveNeighborsWhenNotAllowedThen401() {
        assertUnsupportedCommand(removeNeighborsCommand("foo"));
    }

    @Test
    public void givenAddNeighborsWhenNotAllowedThen401() {
        assertUnsupportedCommand(addNeighborsCommand("foo"));
    }

    @Test
    public void givenGetNeighborsWhenNotAllowedThen401() {
        assertUnsupportedCommand(getNeighborsCommand());
    }

    @Test
    public void givenEmptyAttachToTangleThenUnauthorized() {
        AttachToTangle att = attachToTangleCommand(null, null, (String[]) null);
        att.setMinWeightMagnitude(0);
        String result = util.postCommand(att)
                .expectStatus()
                .isUnauthorized()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(String.class).returnResult().getResponseBody();
        logger.info(result);
        assertThat(result).contains("{\"error\":\"COMMAND attachToTangle is not available on this node\",\"duration\":");
    }

    @Test
    public void givenInvalidDepthWhenGetTransactionsToApproveThenValidationFailure() {
        GetTransactionsToApprove gta = getTransactionsToApprove();
        gta.setDepth(1);
        String result = util.postCommand(gta)
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("Validation");
        assertThat(result).contains("depth");
    }

    @Test
    public void givenTooManyTrytesWhenGetTrytesThenValidationFailure() {
        GetTrytes gt = getTrytes("foo", "bar");
        String result = util.postCommand(gt)
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("Validation");
        assertThat(result).contains("hashes");
    }

    @Test
    public void givenTooLongCommandWhenPostThenError() {
        util.postCommand(command("thisCommandStringIsTooLongAndShouldFail"))
                .expectStatus()
                .isBadRequest();
    }

    @Test
    public void givenUnknownCommandWhenPostThenError() {
        util.postCommand(command("unknown"))
                .expectStatus()
                .isBadRequest();
    }

    @Test
    public void givenInvalidCommandWhenPostThenError() {
        util.postCommand(command("storeTransactions"))
                .expectStatus()
                .isBadRequest();
    }

    @Test
    public void givenBlankCommandWhenPostThenError() {
        util.postCommand(command("   ")).expectStatus().isBadRequest();
    }

    @Test
    public void givenNullCommandWhenPostThenError() {
        util.postCommand("{\"command\": null}").expectStatus().isBadRequest();
    }

    @Test
    public void givenInvalidJsonWhenPostThenError() {
        util.postCommand("{\"blah\": }").expectStatus().isBadRequest();
    }

    private void assertUnsupportedCommand(Object command) {
        String result = util.postCommand(command)
                .expectStatus().isUnauthorized()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(String.class)
                .returnResult().getResponseBody();
        logger.debug(result);
        assertNotNull(result);
        assertTrue(result, result.startsWith("{\"error\":\"COMMAND "));
        assertTrue(result, result.contains("is not available on this node\",\"duration\""));
    }

}