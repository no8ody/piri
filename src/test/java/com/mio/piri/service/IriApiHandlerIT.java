/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.util.JsonUtil;
import com.mio.piri.util.TestCommandFactory;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.StringJoiner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "iri.nodes=",
        "spring.codec.max-in-memory-size=850KB",
        "piri.circuit.buffer.closed=100", // we don't want circuit breaker to open
        "piri.circuit.failure.rate=100", // we don't want circuit breaker to open
        "piri.retry.successive-on-same-node=true", // for test with retries
        "piri.retry.max-per-node=5" // for test with retries
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerIT extends AbstractIriApiHandlerTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void givenGetNodeInfoWhenPostThenReturnIriNodeInfo() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        String result = util.postCommand(new GetNodeInfo())
                .expectStatus().isOk()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("latestMilestoneIndex");
    }

    @Test
    public void givenGetNodeInfoWhenExchangeThenReturnRemotePowEnabled() {
        NodeInfo nodeInfo = new NodeInfo();
        nodeInfo.setFeatures(new HashSet<>());
        nodeInfo.getFeatures().add("RemotePOW");

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(JsonUtil.toJson(nodeInfo)));

        String body = util.postCommand(new GetNodeInfo())
                .expectStatus().isOk()
                .expectBody(String.class).returnResult().getResponseBody();

        // attachToTangle is not enabled. Therefore the RemotePOW feature should be filtered out of the response.
        assertThat(body).doesNotContain("RemotePOW");
        logger.info("Response body: {}", body);
    }

    @Test
    public void givenTipsAreNotConsistentWhenExchangeThenRetry() {

        for (int i = 0; i < 3; i++) {
            prepareResponse(response -> response
                    .setHeader("Content-Type", "application/json")
                    .setResponseCode(400)
                    .setBody("Tips are not consistent"));
        }

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setResponseCode(400)
                .setBody("Final response."));

        String response = util.postCommand(TestCommandFactory.getBalances())
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();

        assertThat(response).contains("Final response.");

    }

    @Test
    public void givenRetriesConsumedWhenExchangeThenReturnErrorResponse() {

        for (int i = 0; i < 5; i++) {
            prepareResponse(response -> response
                    .setHeader("Content-Type", "application/json")
                    .setResponseCode(400)
                    .setBody("Tips are not consistent"));
        }


        String response = util.postCommand(TestCommandFactory.getBalances())
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();

        logger.info(response);
        assertThat(response).contains("Tips are not consistent");

    }

    @Test
    public void givenLargeRequestWithingBoundsWhenExchangeThenReturnOk() {
        StringJoiner sj = new StringJoiner("\",\"", "{ \"hashes\": [\"", "\"], \"duration\": 10 }");
        int noOfHashes = 10000;
        for (int i = 0; i < noOfHashes; i++) {
            sj.add(RandomStringUtils.randomAlphabetic(81).toUpperCase());
        }

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setResponseCode(200)
                .setBody(sj.toString()));

        int expected = noOfHashes * (81 + 3) + (34 - 3);
        logger.info("Expected response size: [{}].", expected);

        String response = util.postCommand(TestCommandFactory.getTips())
                .expectStatus().isOk()
                .expectBody(String.class).returnResult().getResponseBody();

        // count * (81 + 3 delimiters) + (prefix + suffix - only one delimiters at beginning and end)
        assertThat(response).hasSize(expected);
        logger.debug(response);
    }

    @Test
    public void givenTooLargeRequestWhenExchangeThenReturnError() {
        StringJoiner sj = new StringJoiner("\",\"", "{ \"hashes\": [\"", "\"], \"duration\": 10 }");
        int noOfHashes = 11000;
        for (int i = 0; i < noOfHashes; i++) {
            sj.add(RandomStringUtils.randomAlphabetic(81).toUpperCase());
        }

        logger.info("Response size: [{}]", sj.length());

        for (int i = 0; i < 5; i++) {
            prepareResponse(response -> response
                    .setHeader("Content-Type", "application/json")
                    .setResponseCode(200)
                    .setBody(sj.toString()));
        }

        String response = util.postCommand(TestCommandFactory.getTips())
                .expectStatus().is5xxServerError()
                .expectBody(String.class).returnResult().getResponseBody();
        logger.info(response);
        // TODO hide internal error message on server error
        assertThat(response).contains("\"error\":\"Internal Server Error\",\"message\":\"Exceeded limit on max bytes to buffer");

    }

}