/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api;

import com.mio.piri.nodes.api.dtos.RegisterNodeRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.UUID;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;


public class RegisterNodeRequestTest {

    @Test
    public void givenValidInputWhenCheckNameThenMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.NAME_REGEXP);
        assertThat(nodeName.matcher("Test").matches()).isTrue();
        assertThat(nodeName.matcher("some-test-node").matches()).isTrue();
        assertThat(nodeName.matcher("Hello_World").matches()).isTrue();
        assertThat(nodeName.matcher("Hello.World").matches()).isTrue();
        assertThat(nodeName.matcher("foo.bar.com").matches()).isTrue();
        assertThat(nodeName.matcher(UUID.randomUUID().toString()).matches()).isTrue();
        assertThat(nodeName.matcher("Hello World").matches()).isTrue();
        assertThat(nodeName.matcher("foo:bar").matches()).isTrue();
        assertThat(nodeName.matcher("http://foo.bar.com").matches()).isTrue();

    }

    @Test
    public void givenInvalidInputWhenCheckNameThenDoNotMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.NAME_REGEXP);
        assertThat(nodeName.matcher("Test ").matches()).isFalse();
        assertThat(nodeName.matcher(" Test").matches()).isFalse();
        assertThat(nodeName.matcher(" ").matches()).isFalse();
        assertThat(nodeName.matcher("HelloWorld!").matches()).isFalse();
        assertThat(nodeName.matcher("").matches()).isFalse();
        assertThat(nodeName.matcher("Node<script>alert(1)</script>").matches()).isFalse();
        assertThat(nodeName.matcher("a&b").matches()).isFalse();
        assertThat(nodeName.matcher("1+1=3").matches()).isFalse();
        assertThat(nodeName.matcher("foo:").matches()).isFalse();
        assertThat(nodeName.matcher("http://").matches()).isFalse();
    }

    @Test
    public void givenValidInputWhenCheckPasswordThenMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.PASS_REGEXP);
        assertThat(nodeName.matcher("Test").matches()).isTrue();
        assertThat(nodeName.matcher("foo#bar!com.").matches()).isTrue();
        assertThat(nodeName.matcher(UUID.randomUUID().toString()).matches()).isTrue();
        assertThat(nodeName.matcher("HelloWorld!").matches()).isTrue();
        assertThat(nodeName.matcher("DANWXQHUKKVWQYOUDWPRLRHXEYDQLCGUTQRDXTMLQDYPRYBIXJKWBAZQPOOORMMIYAFAJIRIAXRPLDOKR").matches()).isTrue();
        assertThat(nodeName.matcher("Node<script>alert(1)</script>").matches()).isTrue();
        assertThat(nodeName.matcher("foo:bar").matches()).isTrue();
        assertThat(nodeName.matcher("a&b").matches()).isTrue();
        assertThat(nodeName.matcher("http://foo.bar.com").matches()).isTrue();
        assertThat(nodeName.matcher("1+1=3").matches()).isTrue();

        assertThat(nodeName.matcher("Test ").matches()).isFalse();
        assertThat(nodeName.matcher(" Test").matches()).isFalse();
        assertThat(nodeName.matcher(" ").matches()).isFalse();
        assertThat(nodeName.matcher("").matches()).isFalse();
    }

    @Test
    public void givenInvalidInputWhenCheckPasswordThenDoNotMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.PASS_REGEXP);
        assertThat(nodeName.matcher("Test ").matches()).isFalse();
        assertThat(nodeName.matcher(" Test").matches()).isFalse();
        assertThat(nodeName.matcher("Hi there").matches()).isFalse();
        assertThat(nodeName.matcher("").matches()).isFalse();
    }

    private static final String VALID_ADDRESS = "TPYNGLGVXWNEQPOOGWFLGTL9NYCLIKUZOZA9IRFVNUBBVZIOPAXFAOXHHQELNDPDY9E9VSCFSTBCRZCRW";
    private static final String VALID_CHECKSUM = "NMWBMXOAC";

    @Test
    public void givenValidInputWhenCheckAddressThenMatchRegexp() {
        Pattern addressPattern = Pattern.compile(RegisterNodeRequest.ADDRESS_REGEXP);
        assertThat(addressPattern.matcher(RandomStringUtils.randomAlphabetic(81).toUpperCase()).matches()).isTrue();
        assertThat(addressPattern.matcher(RandomStringUtils.randomAlphabetic(90).toUpperCase()).matches()).isTrue();
        assertThat(addressPattern.matcher(VALID_ADDRESS).matches()).isTrue();
        assertThat(addressPattern.matcher(VALID_ADDRESS + VALID_CHECKSUM).matches()).isTrue();
    }

    @Test
    public void givenInvalidInputWhenCheckAddressThenDoNotMatchRegexp() {
        Pattern addressPattern = Pattern.compile(RegisterNodeRequest.ADDRESS_REGEXP);
        assertThat(addressPattern.matcher(RandomStringUtils.randomAlphabetic(80).toUpperCase()).matches()).as("too short").isFalse();
        assertThat(addressPattern.matcher(RandomStringUtils.randomAlphabetic(82).toUpperCase()).matches()).as("invalid size").isFalse();
        assertThat(addressPattern.matcher(RandomStringUtils.randomAlphabetic(91).toUpperCase()).matches()).as("too long").isFalse();
        assertThat(addressPattern.matcher(RandomStringUtils.randomNumeric(81)).matches()).as("contains [0-8]").isFalse();
        assertThat(addressPattern.matcher(RandomStringUtils.randomNumeric(90)).matches()).as("contains [0-8]").isFalse();
        assertThat(addressPattern.matcher(VALID_ADDRESS.replace('9', ' ')).matches()).as("contains spaces").isFalse();
    }

}