/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api;

import com.mio.piri.nodes.api.dtos.RegisterNodeRequest;
import com.mio.piri.nodes.api.dtos.RegisterNodeResponse;
import com.mio.piri.nodes.api.dtos.UnregisterNodeRequest;
import com.mio.piri.nodes.api.dtos.UnregisterNodeResponse;
import com.mio.piri.util.JsonUtil;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.util.UUID;
import java.util.function.Consumer;

import static com.mio.piri.WebSecurityConfiguration.ROLE_NODE_ADMIN;
import static com.mio.piri.commands.response.ResponseDtoBuilder.nodeInfoBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.springSecurity;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "piri.nodes.registration.secure=false",
        "piri.nodes.registration.from.node.only=false",
        "spring.security.user.name=piri",
        "spring.security.user.password=piri"
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NodesApiHandlerIT {

    private static final String NODE_URL = "http://localhost:1234";
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApplicationContext context;

    private MockWebServer server; // for health check
    private WebTestClient webTestClient;

    private final String syncedNodeInfo = JsonUtil.toJson(nodeInfoBuilder()
            .latestMilestoneIndex(42)
            .latestSolidSubtangleMilestoneIndex(41)
            .neighbors(3)
            .build());

    private String key = null; // registering node dirties context. We need this to clean up even if test fails.

    // needed because endpoint is secured
    @Before
    public void setUpSecurity() {
        webTestClient = WebTestClient
                .bindToApplicationContext(context)
                .apply(springSecurity())
                .configureClient()
                .defaultHeaders(headers -> headers.setBasicAuth("piri", "piri"))
                .build();
    }

    @Before
    public void setUpTestNode() throws IOException {
        server = new MockWebServer();
        server.start(1234);
    }

    @After
    public void shutdown() throws Exception {
        if (key != null) {
            unregisterNodeOk(key);
        }
        this.server.shutdown();
    }

    @Test
    public void whenRegisterThenReturn201() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated(UUID.randomUUID().toString());
        assertThat(key).isNotBlank();
        assertThat(key).hasSize(81);
    }

    @Test
    public void whenUnregisterThenReturnNodeName() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        String key = registerNodeCreated("foo");
        UnregisterNodeResponse response = unregisterNodeOk(key);
        assertThat(response.getName()).isEqualTo("foo");
    }

    @Test
    public void givenSameNameWhenRegisterThenReturn409() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");

        String result = registerNode(new RegisterNodeRequest("foo", "http://localhost:666", true))
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("already registered");
        logger.debug(result);

    }

    @Test
    public void givenSamePasswordWhenRegisterThenReturn409() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");

        String result = registerNode(new RegisterNodeRequest("bar", "http://localhost:666", key))
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("Use another password");
        logger.debug(result);

    }

    /*
    @Test
    public void givenSameUrlWhenRegisterThenReturn409() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");

        String result = registerNode(new RegisterNodeRequest("bar", "http://localhost:1234", true))
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("already registered");
        logger.debug("Result: {}", result);

    }
    */

    @Test
    public void givenInvalidCharacterWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("hel<lo.world", "http://localhost:1234", true))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("name must match");
    }

    @Test
    public void givenInvalidLengthWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("1234567890123456789012345678901234567890123", "http://localhost:1234", true))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("size must be");
    }

    @Test
    public void givenBlankNameWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest(" ", "http://localhost:1234", true))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("name must not be blank");
    }

    @Test
    public void givenInvalidUrlWhenRegisterThen400() {
        EntityExchangeResult<Object> result = registerNode(new RegisterNodeRequest("foo", "no.valid.url", true))
                .expectStatus().isBadRequest()
                .expectBody(Object.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
    }

    @Test
    public void givenShortPasswordWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("foo", "no.valid.url", "blah"))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("password size");
    }

    @Test
    public void givenInvalidPasswordWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("foo", "no.valid.url", "blah blah blah!"))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.info("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("password must match");
    }

    @Test
    public void givenShortAddressWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("foo", "http://localhost:1234", true, " ", "blahBlahBlah"))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("address size");
    }

    @Test
    public void givenInvalidAddressWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("foo", "http://localhost:1234", true,
                RandomStringUtils.randomNumeric(81).toUpperCase(), "blahBlahBlah"))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("address must match");
    }

    @Test
    public void givenUnreachableNodeWhenRegisterThen422() {
        EntityExchangeResult<Object> result = registerNode(new RegisterNodeRequest("foo", "http://localhost:666", true))
                .expectStatus().isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
                .expectBody(Object.class).returnResult();
        assertThat(result.getResponseBody()).isNotNull();
        assertThat(result.getResponseBody().toString()).contains("Connection refused");
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
    }

    @Test
    public void whenGetNodesThenReturnNodeStatus() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");
        WebTestClient.ResponseSpec response = getNodes();

        String body = response.expectBody(String.class).returnResult().getResponseBody();
        logger.info("Response: {}", body);
        verifyNodeStatusResponse(body, "foo", null);
    }

    @Test
    public void whenGetNodeByNameThenReturnNodeStatus() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");
        WebTestClient.ResponseSpec response = getNode("foo");
        String status = response.expectStatus().isOk().expectBody(String.class).returnResult().getResponseBody();
        logger.info("Response: {}", status);
        verifyNodeStatusResponse(status, "foo", null);
    }

    @Test
    public void whenGetNodeByNameThenReturnAddress() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        String address = RandomStringUtils.randomAlphabetic(81).toUpperCase();
        key = registerNodeCreated("address-test", address);
        WebTestClient.ResponseSpec response = getNode("address-test");
        String status = response.expectStatus().isOk().expectBody(String.class).returnResult().getResponseBody();
        logger.info("Response: {}", status);
        verifyNodeStatusResponse(status, "address-test", address);
    }



    private void verifyNodeStatusResponse(String body, String name, String address) {
        assertThat(body).contains(String.format("name\":\"%s\"", name));
        assertThat(body).contains("appName");
        assertThat(body).contains("appVersion");
        assertThat(body).contains("key\":\"");
        assertThat(body).contains("neighbors\":3");
        assertThat(body).contains("milestone\":42");
        assertThat(body).contains("solidMilestone\":41");
        assertThat(body).contains("delay\":1");
        assertThat(body).contains("pow\":");
        assertThat(body).doesNotContain("synced");
        assertThat(body).contains("available\":true");
        if (address == null) {
            assertThat(body).contains("address\":null");
        } else {
            assertThat(body).contains(String.format("address\":\"%s\"", address));
        }
        assertThat(body).contains("commands");
        assertThat(body).contains("millis");
        assertThat(body).contains("count");
    }

    @Test
    public void givenUnknownNameWhenGetNodeByNameThenReturnNotFound() {
        getNode("unknown").expectStatus().isNotFound();
    }

    @WithMockUser(roles = ROLE_NODE_ADMIN)
    @Test
    public void whenUnregisterByPostThenReturnOk() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        registerNodeCreated("foo");
        WebTestClient.ResponseSpec responseSpec = unregisterNode(new UnregisterNodeRequest("foo"));
        UnregisterNodeResponse response = responseSpec
                .expectStatus().isOk()
                .expectBody(UnregisterNodeResponse.class).returnResult().getResponseBody();
        assertThat(response).isNotNull();
        assertThat(response.getName()).isEqualTo("foo");
        assertThat(response.getUrl()).isEqualTo(NODE_URL);
    }

    @WithMockUser(roles = ROLE_NODE_ADMIN)
    @Test
    public void givenNoNameWhenUnregisterByPostThenError() {
        unregisterNode(new UnregisterNodeRequest(null)).expectStatus().isBadRequest();
    }

    @WithMockUser(roles = ROLE_NODE_ADMIN)
    @Test
    public void givenUnknownNameWhenUnregisterByPostThenNotFound() {
        unregisterNode(new UnregisterNodeRequest("blah")).expectStatus().isNotFound();
    }

    private String registerNodeCreated(String name) {
        return registerNodeCreated(name, null);
    }

    private String registerNodeCreated(String name, String donationAddress) {
        RegisterNodeResponse responseBody = registerNode(new RegisterNodeRequest(name, NODE_URL, true, donationAddress, null))
                .expectStatus().isCreated()
                .expectBody(RegisterNodeResponse.class)
                .returnResult().getResponseBody();
        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getName()).isEqualTo(name);
        assertThat(responseBody.getUrl()).isEqualTo("http://localhost:1234");
        assertThat(responseBody.getHash()).isNotBlank();
        assertThat(responseBody.getAddress()).isEqualTo(donationAddress);
        logger.info("Response: {}", JsonUtil.toJson(responseBody));
        return responseBody.getPassword();
    }

    private WebTestClient.ResponseSpec registerNode(Object request) {
        return webTestClient
                .post()
                .uri("/nodes")
                .bodyValue(request)
                .exchange();
    }

    private WebTestClient.ResponseSpec getNodes() {
        return webTestClient
                .get()
                .uri("/nodes")
                .exchange();
    }

    private WebTestClient.ResponseSpec getNode(String name) {
        return webTestClient
                .get()
                .uri("/nodes/{name}", name)
                .accept(MediaType.APPLICATION_JSON)
                .exchange();
    }

    private WebTestClient.ResponseSpec unregisterNode(UnregisterNodeRequest request) {
        return webTestClient
                .post()
                .uri("/nodes/unregister")
                .bodyValue(request)
                .exchange();
    }

    private UnregisterNodeResponse unregisterNodeOk(String key) {
        UnregisterNodeResponse response = webTestClient.delete().uri("/nodes/" + key).exchange()
                .expectStatus().isOk()
                .expectBody(UnregisterNodeResponse.class).returnResult().getResponseBody();
        logger.debug("Response: {}", JsonUtil.toJson(response));
        return response;
    }

    private void prepareResponse(Consumer<MockResponse> consumer) {
        MockResponse response = new MockResponse();
        consumer.accept(response);
        this.server.enqueue(response);
    }

}
