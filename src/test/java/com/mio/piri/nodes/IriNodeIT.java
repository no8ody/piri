/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.util.JsonUtil;
import io.micrometer.core.instrument.Timer;
import io.vavr.control.Try;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import static com.mio.piri.commands.IriCommands.ATTACH_TO_TANGLE;
import static com.mio.piri.commands.IriCommands.GET_NODE_INFO;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"iri.version.blacklist.regexp=.*RC.*"}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriNodeIT {

    private MockWebServer server;

    @Autowired
    private NodeRegistry nodeRegistry;

    @Autowired
    private MeterFactory meterFactory;

    private IriNode node;

    private final String syncedNodeInfo = jsonString(ResponseDtoBuilder.nodeInfoBuilder()
            .latestMilestoneIndex(42)
            .latestSolidSubtangleMilestoneIndex(42)
            .build());

    @Before
    public void setUpTestNode() throws IOException {
        server = new MockWebServer();
        server.start(1234);
        node = nodeRegistry.registerIriNode(UUID.randomUUID().toString(), server.url("/").toString(), "some-key", true);
    }

    @After
    public void shutdown() throws Exception {
        this.server.shutdown();
        nodeRegistry.unregisterNode(node.getName());
    }

    @Test
    public void whenCallThenReturnResponseVavr() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        Try<ResponseEntity<String>> response = Try.of(() -> node.call(new GetNodeInfo()).block());
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void whenCallThenReturnResponseStepVerifier() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        Mono<ResponseEntity<String>> response = node.call(new GetNodeInfo());
        StepVerifier.create(response)
                .assertNext(r -> assertThat(r.getStatusCode()).isEqualTo(HttpStatus.OK))
                .verifyComplete();
    }

    @Test
    public void givenCustomHeadersWhenCallThenSendThem() throws InterruptedException {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        node.setHeadersConsumer(httpHeaders -> httpHeaders.add("foo", "bar"));

        Mono<ResponseEntity<String>> response = node.call(new GetNodeInfo());
        StepVerifier.create(response)
                .assertNext(r -> assertThat(r.getStatusCode()).isEqualTo(HttpStatus.OK))
                .verifyComplete();

        String fooHeader = server.takeRequest().getHeader("foo");
        assertThat(fooHeader).isEqualToIgnoringCase("bar");
    }

    @Test
    public void whenCallThenRecordTime() throws InterruptedException {
        int delay = 100;
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo)
                .setBodyDelay(delay, TimeUnit.MILLISECONDS));

        Timer timer = meterFactory.createCommandTimer(GET_NODE_INFO.getKey(), node.getName(), node.getKey());
        long count = timer.count();
        double time = timer.totalTime(TimeUnit.MILLISECONDS);

        StepVerifier.create(node.call(new GetNodeInfo()))
                .expectNextCount(1)
                .verifyComplete();

        Thread.sleep(100); // doFinally is executed after mono completion

        assertThat(timer.count()).isGreaterThan(count);
        assertThat(timer.totalTime(TimeUnit.MILLISECONDS)).isGreaterThan(time + delay);
    }

    @Test
    public void givenErrorWhenCallThenRecordTime() {
        Timer timer = meterFactory.createCommandTimer(GET_NODE_INFO.getKey(), node.getName(), node.getKey());
        long count = timer.count();
        double time = timer.totalTime(TimeUnit.MILLISECONDS);

        StepVerifier.withVirtualTime(() -> node.call(new GetNodeInfo()))
                .expectSubscription()
                .expectNoEvent(GET_NODE_INFO.getTimeout())
                .expectError(TimeoutException.class)
                .log()
                .verify(Duration.ofSeconds(1));

        assertThat(timer.count()).isGreaterThan(count);
        assertThat(timer.totalTime(TimeUnit.MILLISECONDS)).isGreaterThan(time);
    }

    @Test
    public void givenNoResponseWhenCallThenTimeoutError() {
        StepVerifier.withVirtualTime(() -> node.call(new GetNodeInfo()))
                .expectSubscription()
                .expectNoEvent(GET_NODE_INFO.getTimeout())
                .expectError(TimeoutException.class)
                .log()
                .verify(Duration.ofSeconds(1));
    }

    /**
     * Error handling moved out of node to caller. Therefore we don't throw in case of 5xx.
     */
    @Test
    public void given503WhenCallThenSuccess() {

        // some nodes return 503 if they are not available. These commands should retry.
        prepareResponse(response -> response
                .setResponseCode(503)
                .setBody("Service not available."));

        StepVerifier.create(node.call(new GetNodeInfo()))
                .assertNext(response -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.SERVICE_UNAVAILABLE))
                .verifyComplete();
    }

    /**
     * Error handling moved out of node to caller. Therefore we don't throw in case of 5xx.
     */
    @Test
    public void given500WhenCallThenSuccess() {
        prepareResponse(response -> response
                .setResponseCode(500)
                .setBody("Service not available."));

        StepVerifier.create(node.call(new GetNodeInfo()))
                .assertNext(response -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR))
                .verifyComplete();
    }

    @Test
    public void givenNoResponseForAttachToTangleWhenCallThenTimeoutLater() {
        AttachToTangle command = new AttachToTangle();
        command.setCommand(ATTACH_TO_TANGLE.getKey());
        StepVerifier.withVirtualTime(() -> node.call(command))
                .expectSubscription()
                .expectNoEvent(Duration.ofSeconds(40))
                .thenAwait(Duration.ofSeconds(20))
                .expectError(TimeoutException.class)
                .log()
                .verify(Duration.ofSeconds(1));
    }

    @Test
    public void givenInvalidIriVersionWhenHealthCheckThenSetUnavailable() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(jsonString(ResponseDtoBuilder.nodeInfoBuilder()
                        .appVersion("1.2.3RC42")
                        .latestSolidSubtangleMilestoneIndex(42)
                        .latestMilestoneIndex(42).build())));

        Mono<NodeInfo> synced = node.queryNodeHealth();
        StepVerifier.create(synced)
                .assertNext(val -> {
                    assertThat(val).isNotNull();
                    assertThat(val.getAppVersion()).contains("RC");
                })
                .verifyComplete();

        assertThat(node.isAvailable()).isFalse();

    }

    @Test
    public void givenSuccessWhenHealthCheckThenSyncedIsTrue() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        Mono<NodeInfo> synced = node.queryNodeHealth();
        StepVerifier.create(synced)
                .assertNext(val -> {
                    assertThat(val).isNotNull();
                    assertThat(val.isSynced()).isTrue();
                })
                .verifyComplete();

        assertThat(node.isSynced()).isTrue();
    }

    @Test
    public void givenOutOfSyncWhenHealthCheckThenSyncedIsFalse() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(jsonString(ResponseDtoBuilder.nodeInfoBuilder().latestMilestoneIndex(42).build())));

        Mono<NodeInfo> unsynced = node.queryNodeHealth();
        StepVerifier.create(unsynced)
                .assertNext(val -> {
                    assertThat(val).isNotNull();
                    assertThat(val.isSynced()).isFalse();
                })
                .verifyComplete();

        assertThat(node.isSynced()).isFalse();
    }

    @Test
    public void givenInvalidResponseWhenHealthCheckThenHandleErrorSuccessfully() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "text/plain")
                .setBody(syncedNodeInfo));


        Mono<NodeInfo> result = node.queryNodeHealth();
        StepVerifier.create(result)
                .assertNext(nodeInfo -> {
                    assertThat(nodeInfo).isNotNull();
                    // on error resume with invalid node info
                    assertThat(nodeInfo.getLatestMilestoneIndex() < 0);
                    // node gets deactivated
                    assertThat(node.isAvailable()).isFalse();
                })
                .verifyComplete();
    }

    @Test
    public void givenNoResponseWhenHealthCheckThenHandleTimeoutSuccessfully() {
        // no response
        StepVerifier.withVirtualTime(() -> node.queryNodeHealth())
                .expectSubscription()
                .expectNoEvent(GET_NODE_INFO.getTimeout())
                .assertNext(nodeInfo -> {
                    assertThat(nodeInfo).isNotNull();
                    // on error resume with invalid node info
                    assertThat(nodeInfo.getLatestMilestoneIndex() < 0);
                    // node gets deactivated
                    assertThat(node.isAvailable()).isFalse();
                })
                .verifyComplete();
    }

    @Test
    public void givenAttachToTangleWhenCallThenOk() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody("FOO"));

        StepVerifier.create(node.call(new AttachToTangle()))
                .assertNext(responseEntity -> responseEntity.getStatusCode().is2xxSuccessful())
                .verifyComplete();
    }

    private void prepareResponse(Consumer<MockResponse> consumer) {
        MockResponse response = new MockResponse();
        consumer.accept(response);
        this.server.enqueue(response);
    }


    private String jsonString(Object nodeInfo) {
        return JsonUtil.toJson(nodeInfo);
    }

}