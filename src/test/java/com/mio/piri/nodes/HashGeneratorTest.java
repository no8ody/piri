/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("SpellCheckingInspection")
public class HashGeneratorTest {

    private final HashGenerator hashGenerator = new HashGenerator();

    @Test
    public void whenSha256ThenGenerateValidHash() {
        assertThat(hashGenerator.sha256("foo")).isEqualTo("2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae");
        assertThat(hashGenerator.sha256("bar")).isEqualTo("fcde2b2edba56bf408601fb721fe9b5c338d10ee429ea04fae5511b68fbf8fb9");
        assertThat(hashGenerator.sha256("test")).isEqualTo("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08");
    }

    @Test
    public void whenPublicKeyThenSubstringHash() {
        assertThat(hashGenerator.publicKey(hashGenerator.sha256("foo"))).isEqualTo("2c26b46b68");
        assertThat(hashGenerator.publicKey(hashGenerator.sha256("bar"))).isEqualTo("fcde2b2edb");
        assertThat(hashGenerator.publicKey(hashGenerator.sha256("test"))).isEqualTo("9f86d08188");
    }


}