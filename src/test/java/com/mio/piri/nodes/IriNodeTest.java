/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.util.JsonUtil;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;
import java.util.regex.Pattern;

import static com.mio.piri.commands.response.NodeInfo.REMOTE_POW;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

public class IriNodeTest {

    private final NodeClient nodeClient = mock(NodeClient.class);
    private final MeterFactory metrics = mock(MeterFactory.class);

    private final CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults("foo");
    private final Timer timer = Metrics.timer("timer");
    private final Timer.Sample sample = mock(Timer.Sample.class);

    private final IriCommand command = mock(IriCommand.class);
    private final ResponseEntity<String> okResponseEntity = ResponseEntity.status(HttpStatus.OK).body("foo");

    private final NodeInfo nodeInfo = mock(NodeInfo.class);

    private final IriNode node = new IriNode("foo", "url", "fooKey", true, nodeClient, metrics, circuitBreaker);

    @Before
    public void setUpMocks() {
        IriNode.VERSION_WHITELIST = null;
        IriNode.VERSION_BLACKLIST = null;
        when(command.getCommand()).thenReturn("foo");
        when(metrics.createCommandTimer(anyString(), anyString(), anyString())).thenReturn(timer);
        when(metrics.sample()).thenReturn(sample);
    }

    private void setWebClientResponse(ResponseEntity<String> clientResponse) {
        when(nodeClient.post(any(Node.class), any(IriCommand.class))).thenReturn(Mono.just(clientResponse));
        when(nodeClient.exchange(any(Node.class), any(AttachToTangle.class))).thenReturn(Mono.just(clientResponse));
    }

    @Test
    public void whenCallThenReturnResponse() {
        // we cannot mock timer and circuit breaker here
        setWebClientResponse(okResponseEntity);
        Mono<ResponseEntity<String>> response = node.call(command);
        assertNotNull(response);
    }

    @Test
    public void whenCallThenRecordMetrics() {
        setWebClientResponse(okResponseEntity);
        Timer mockTimer = mock(Timer.class);
        when(metrics.createCommandTimer(anyString(), eq("foo"), eq("fooKey"))).thenReturn(mockTimer);
        Mono<ResponseEntity<String>> response = node.call(command);
        verifyNoInteractions(sample); // metrics not recorded yet as operation is not finished
        response.block();
        verify(sample).stop(mockTimer);
    }

    @Test
    public void whenSetUnavailableThenChangeValue() {
        assertThat(node.isAvailable()).isTrue();
        node.setUnavailable("test");
        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void whenUpdateWithHealthCheckResultThenSetAvailable() {
        node.setUnavailable("test");
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isTrue();
    }

    @Test
    public void givenSupportedVersionWhenUpdateWithHealthCheckResultThenDoNothing() {
        IriNode.VERSION_WHITELIST = Pattern.compile("Hallo.*|1....-RELEASE");
        when(nodeInfo.getAppVersion()).thenReturn("1.2.3-RELEASE");
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isTrue();
    }

    @Test
    public void givenNullVersionWhenUpdateWithHealthCheckResultThenSetUnavailable() {
        IriNode.VERSION_WHITELIST = Pattern.compile(".*");
        when(nodeInfo.getAppVersion()).thenReturn(null);
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void givenUnsupportedVersionWhenUpdateWithHealthCheckResultThenSetUnavailable() {
        IriNode.VERSION_WHITELIST = Pattern.compile(".*RELEASE");
        assertThat(node.isAvailable()).isTrue();
        when(nodeInfo.getAppVersion()).thenReturn("1.7.1-RC2");
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void givenUnsupportedVersionWhenUpdateWithHealthCheckResultThenStayUnavailable() {
        IriNode.VERSION_WHITELIST = Pattern.compile(".*-RELEASE");
        node.setUnavailable("test");
        nodeInfo.setAppVersion("1.7.1-RC2");
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void givenNotBlacklistedVersionWhenUpdateWithHealthCheckResultThenDoNothing() {
        IriNode.VERSION_BLACKLIST = Pattern.compile(".*RC.*");
        when(nodeInfo.getAppVersion()).thenReturn("1.2.3-RELEASE");
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isTrue();
    }

    @Test
    public void givenBlacklistedVersionWhenUpdateWithHealthCheckResultThenSetUnavailable() {
        IriNode.VERSION_BLACKLIST = Pattern.compile(".*RC.*");
        assertThat(node.isAvailable()).isTrue();
        when(nodeInfo.getAppVersion()).thenReturn("1.7.1-RC2");
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void givenBlacklistedVersionWhenUpdateWithHealthCheckResultThenStayUnavailable() {
        IriNode.VERSION_BLACKLIST = Pattern.compile(".*alpha.*|.*RC.*|.*beta.*");
        node.setUnavailable("test");
        when(nodeInfo.getAppVersion()).thenReturn("1.7.1-RC2");
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void whenHandleHealthCheckErrorThenSetUnavailable() {
        assertThat(node.isAvailable()).isTrue();
        node.updateWithHealthCheckError(new RuntimeException("test"));
        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void whenIsSyncedThenAskNodeInfo() {
        assertThat(node.isSynced()).isFalse();
        when(nodeInfo.getLatestMilestoneIndex()).thenReturn(42L);
        when(nodeInfo.getLatestSolidSubtangleMilestoneIndex()).thenReturn(42L);
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.isSynced()).isTrue();
    }

    @Test
    public void whenGetLatestMilestoneThenAskNodeInfo() {
        node.updateWithHealthCheckResult(nodeInfo);
        node.getLatestMilestoneIndex();
        // get's called once on update and then on get
        verify(nodeInfo, times(2)).getLatestMilestoneIndex();

    }

    @Test
    public void whenGetDelayThenCompareWithLatestKnownMilestone() {
        when(nodeInfo.getLatestSolidSubtangleMilestoneIndex()).thenReturn(41L);
        node.updateWithHealthCheckResult(nodeInfo);
        node.updateLatestKnownMilestone(42);
        assertThat(node.getDelay()).isOne();
    }

    @Test
    public void givenInvalidNodeInfoWhenGetDelayThenReturnInvalidPositiveValue() {
        // less than 0 is not a realistic scenario as the latest known milestone should be larger than 0
        // but the result is wrong in this case. How to fix?
        // FIXME assertThat(node.getDelay()).isLessThan(0);
        node.updateLatestKnownMilestone(42);
        assertThat(node.getDelay()).isGreaterThanOrEqualTo(41);
    }

    @Ignore("Remove me soon. Resitting milestone information might not be necessary...")
    @Test
    public void whenUpdateWithHealthCheckErrorThenResetNodeMilestone() {
        when(nodeInfo.getLatestMilestoneIndex()).thenReturn(42L);
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.getLatestMilestoneIndex()).isEqualTo(42);
        node.updateWithHealthCheckError(new RuntimeException("test"));
        assertThat(node.getLatestMilestoneIndex()).isLessThan(0);
    }

    @Test
    public void givenPowEnabledWhenIsCommandSupportedReturnTrue() {
        assertThat(node.isSupported(mock(AttachToTangle.class))).isTrue();
    }

    /**
     * Tests case where node is configured to support pow but health check shows that pow is not available.
     */
    @Test
    public void givenPowUnavailableWhenIsCommandSupportedThenReturnFalse() {
        node.updatePowAvailability(false);
        assertThat(node.isSupported(mock(AttachToTangle.class))).isFalse();
    }

    @Test
    public void givenPowDisabledWhenIsCommandSupportedThenReturnFalse() {
        IriNode node = new IriNode("foo", "url", "fooKey", false, nodeClient, metrics, circuitBreaker);
        assertThat(node.isSupported(mock(AttachToTangle.class))).isFalse();
    }

    @Test
    public void whenUpdateWithHealthCheckResultThenSetNodeInfoValues() {
        NodeInfo nodeInfo = ResponseDtoBuilder.nodeInfoBuilder()
                .appName("IRI")
                .appVersion("Test")
                .neighbors(1)
                .latestSolidSubtangleMilestoneIndex(2)
                .latestMilestoneIndex(3).build();
        node.updateWithHealthCheckResult(nodeInfo);
        assertThat(node.getLatestNodeInfo().getAppName()).isEqualTo("IRI");
        assertThat(node.getLatestNodeInfo().getAppVersion()).isEqualTo("Test");
        assertThat(node.getLatestNodeInfo().getLatestSolidSubtangleMilestoneIndex()).isEqualTo(2);
        assertThat(node.getLatestNodeInfo().getLatestMilestoneIndex()).isEqualTo(3);
        assertThat(node.getLatestNodeInfo().getNeighbors()).isOne();
        assertThat(node.getLatestNodeInfo().getLatestSolidSubtangleMilestoneIndex()).isEqualTo(2);
        assertThat(node.getLatestNodeInfo().getLatestMilestoneIndex()).isEqualTo(3);
    }

    @Test
    public void givenPowSupportedWhenUpdateWithHealthCheckEnablePow() {
        NodeInfo nodeInfo = ResponseDtoBuilder.nodeInfoBuilder().features(Collections.singletonList(REMOTE_POW)).build();
        node.updatePowAvailability(false);

        node.updateWithHealthCheckResult(nodeInfo);

        assertThat(node.isPowAvailable()).isTrue();
    }

    @Test
    public void givenNoPowSupportedWhenUpdateWithHealthCheckDisablePow() {
        NodeInfo nodeInfo = ResponseDtoBuilder.nodeInfoBuilder().build();
        assertThat(node.isPowAvailable()).isTrue();

        node.updateWithHealthCheckResult(nodeInfo);

        assertThat(node.isPowAvailable()).isFalse();
    }

    @Test
    public void givenGetNodeInfoWhenCallNodeThenReuseForHealthUpdate() {
        IriCommand command = new GetNodeInfo();
        NodeInfo nodeInfo = ResponseDtoBuilder.nodeInfoBuilder().appVersion("updated").build();
        ResponseEntity<String> response = ResponseEntity.status(HttpStatus.OK).body(JsonUtil.toJson(nodeInfo));

        when(nodeClient.post(any(Node.class), any(IriCommand.class))).thenReturn(Mono.just(response));
        //when(webClient.post().uri(any(URI.class)).syncBody(any(IriCommand.class)).exchange()).thenReturn(Mono.just(response));

        // already in setup: when(response.getStatusCode()).thenReturn(HttpStatus.OK);
        Mono<ResponseEntity<String>> responseMono = node.call(command);
        StepVerifier.create(responseMono)
                .assertNext(r -> r.getStatusCode().is2xxSuccessful())
                .verifyComplete();

        assertThat(node.getLatestNodeInfo().getAppVersion()).isEqualTo("updated");

    }

    @Test
    public void givenClosedCircuitBreakerWhenHealthCheckThenDoNotThrow() {
        circuitBreaker.transitionToOpenState();
        when(nodeClient.retrieve(any(Node.class), any(IriCommand.class), any())).thenReturn(Mono.just(ResponseEntity.ok(nodeInfo)));
        Mono<NodeInfo> nodeInfoMono = node.queryNodeHealth();
        StepVerifier.create(nodeInfoMono)
                .assertNext(info -> {
                    assertThat(info).isNotNull();
                    assertThat(info.isSynced()).isFalse();
                })
                .verifyComplete();
    }

}