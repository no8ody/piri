/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.IriNode;
import com.mio.piri.nodes.Node;
import com.mio.piri.nodes.PowNode;
import io.vavr.Tuple;
import io.vavr.collection.HashMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RandomSelectionStrategyTest {

    @Mock
    private SelectionUtility selectionUtility;

    @InjectMocks
    private final RandomSelectionStrategy strategy = new RandomSelectionStrategy();

    @Mock
    private IriNode node;

    @Mock
    private PowNode powNode;

    @Mock
    private IriCommand command;

    @Mock
    private AttachToTangle attachToTangle;

//    @Before
//    public void initMocks() {
//        // only for logging
//        when(node.getName()).thenReturn("node");
//        when(powNode.getName()).thenReturn("powNode");
//        when(command.getCommand()).thenReturn("test");
//    }

    @Test
    public void givenAttachToTangleWhenSelectNodeThenReturnPowNode() {
        HashMap<String, Node> nodes = HashMap.fill(10, () -> Tuple.of(randomAlphabetic(10), node));
        when(selectionUtility.isCallFeasible(powNode, attachToTangle, Integer.MAX_VALUE)).thenReturn(true);

        assertThat(strategy.selectNode(attachToTangle, nodes.put("foo", powNode))).isSameAs(powNode);
    }

    @Test
    public void givenAttachToTangleButNoPowNodeWhenSelectNodeThenReturnOtherNode() {
        when(selectionUtility.isCallFeasible(node, attachToTangle, Integer.MAX_VALUE)).thenReturn(true);

        assertThat(strategy.selectNode(attachToTangle, HashMap.of("foo", node))).isSameAs(node);
    }

    @Test
    public void givenAttachToTangleButUnavailablePowNodeWhenSelectNodeThenReturnOtherNode() {
        when(selectionUtility.isCallFeasible(node, attachToTangle, Integer.MAX_VALUE)).thenReturn(true);

        assertThat(strategy.selectNode(attachToTangle, HashMap.of("foo", node, "bar", powNode))).isSameAs(node);
    }

    @Test
    public void givenNoAttachToTangleWhenSelectNodeThenReturnAnotherNode() {
        HashMap<String, Node> nodes = HashMap.fill(10, () -> Tuple.of(randomAlphabetic(10), powNode));
        when(selectionUtility.isCallFeasible(eq(node), any(IriCommand.class), anyInt())).thenReturn(true);

        assertThat(strategy.selectNode(command, nodes.put("foo", node))).isSameAs(node);
    }

    @Test
    public void givenDoesNotNeedUpToDateNodeWhenSelectThenReturnFirstNode() {
        when(selectionUtility.isCallFeasible(node, command, 10)).thenReturn(true);
        assertThat(strategy.selectNode(command, HashMap.of("foo", node))).isSameAs(node);
    }

    @Test
    public void givenAttachToTangleWhenSelectThenReturnFirstNode() {
        AttachToTangle att = mock(AttachToTangle.class);
        when(selectionUtility.isCallFeasible(node, att, Integer.MAX_VALUE)).thenReturn(true);
        assertThat(strategy.selectNode(att, HashMap.of("foo", node))).isSameAs(node);
    }

    @Test
    public void givenNeedsUpToDateNodeWhenSelectThenReturnNull() {
        when(command.needsUpToDateNode()).thenReturn(true);
        assertThat(strategy.selectNode(command, HashMap.of("foo", node))).isNull();
        verify(selectionUtility).isCallFeasible(node, command, 0);
        verify(selectionUtility).isCallFeasible(node, command, 1);
        verifyNoMoreInteractions(selectionUtility);
    }

    @Test
    public void givenWantsUpToDateNodeWhenSelectThenReturnNull() {
        when(command.wantsUpToDateNode()).thenReturn(true);
        assertThat(strategy.selectNode(command, HashMap.of("foo", node))).isNull();
        verify(selectionUtility).isCallFeasible(node, command, 0);
        verify(selectionUtility).isCallFeasible(node, command, 1);
        verify(selectionUtility).isCallFeasible(node, command, 2);
        verifyNoMoreInteractions(selectionUtility);
    }

    @Test
    public void givenSeveralNodesWhenSelectThenReturnLeastDelayed() {
        when(command.wantsUpToDateNode()).thenReturn(true);
        Node anotherNode = mock(Node.class);
        when(selectionUtility.isCallFeasible(anotherNode, command, 2)).thenReturn(true);
        assertThat(strategy.selectNode(command, HashMap.of("foo", node, "bar", anotherNode))).isSameAs(anotherNode);
    }

    @Test
    public void whenSelectThenReturnRandom() {
        when(selectionUtility.isCallFeasible(any(Node.class), any(IriCommand.class), anyInt())).thenReturn(true);
        HashMap<String, Node> nodes = HashMap.fill(10, () -> Tuple.of(randomAlphabetic(10), mock(Node.class)));
        Set<Node> resultNodes = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            resultNodes.add(strategy.selectNode(command, nodes));
        }
        assertThat(resultNodes).doesNotContain((Node) null);
        assertThat(resultNodes.size()).isGreaterThan(5); // would be very unlucky to fail here`
    }

    @Test
    public void whenSelectMultipleThenReturnNotMoreThanWanted() {
        when(selectionUtility.isCallFeasible(any(Node.class), any(IriCommand.class), anyInt())).thenReturn(true);

        assertThat(
                strategy.selectNodes(attachToTangle, HashMap.of("foo", node, "bar", powNode, "blah", node), 2)
        ).containsExactly(powNode, node);
    }

    @Test
    public void whenSelectMultipleThenReturnEmptyList() {
        assertThat(
                strategy.selectNodes(command, HashMap.of("foo", node), 2)
        ).isEmpty();
    }

    @Test
    public void whenSelectMultipleThenReturnMatches() {
        HashMap<String, Node> nodes = HashMap.fill(10, () -> Tuple.of(randomAlphabetic(10), node));

        when(command.needsUpToDateNode()).thenReturn(true);
        when(selectionUtility.isCallFeasible(any(Node.class), any(IriCommand.class), eq(0))).thenReturn(true).thenReturn(false);
        when(selectionUtility.isCallFeasible(any(Node.class), any(IriCommand.class), eq(1))).thenReturn(true).thenReturn(true).thenReturn(false);

        assertThat(
                strategy.selectNodes(command, nodes, 100)
        ).containsExactly(node, node, node);

        verify(selectionUtility, never()).isCallFeasible(any(Node.class), any(IriCommand.class), eq(2));
    }

    @Test
    public void whenSelectMultipleThenReturnMatchesInCorrectOrder() {
        HashMap<String, Node> nodes = HashMap.fill(10, () -> Tuple.of(randomAlphabetic(10), node));
        Node node1 = mock(Node.class);
        Node node2 = mock(Node.class);
        Node node3 = mock(Node.class);
        nodes = nodes.merge(HashMap.of("1", node1, "2", node2, "3", node3));

        when(command.wantsUpToDateNode()).thenReturn(true);
        when(selectionUtility.isCallFeasible(node1, command, 1)).thenReturn(true);
        when(selectionUtility.isCallFeasible(node2, command, 2)).thenReturn(true);
        when(selectionUtility.isCallFeasible(node3, command, 0)).thenReturn(true);

        assertThat(
                strategy.selectNodes(command, nodes, 100)
        ).containsExactly(node3, node1, node2);
    }

}