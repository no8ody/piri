/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.nodes.Node;
import org.junit.Test;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SessionBindingTest {

    private final SessionBinding nodeMapper = new SessionBinding(Duration.ofMillis(500), Duration.ofHours(1));

    @Test
    public void whenAddForClientThenAddNewEntry() {
        nodeMapper.putNodeForClient("foo", "bar");
        assertThat(nodeMapper.getSize()).isEqualTo(1);
    }

    @Test
    public void whenGetForClientThenReturnAddedNode() {
        nodeMapper.putNodeForClient("foo", "bar");
        assertThat(nodeMapper.getNodeForClient("foo")).containsExactly("bar");
    }

    @Test
    public void givenUnknownClientWhenGetForClientThenReturnNull() {
        assertThat(nodeMapper.getNodeForClient("foo")).isEmpty();
        assertThat(nodeMapper.getNodeForClient("bar")).isEmpty();
        assertThat(nodeMapper.getNodeForClient(null)).isEmpty();
    }

    @Test
    public void whenAddForSameClientThenReplaceEntry() {
        nodeMapper.putNodeForClient("foo", "bar");
        nodeMapper.putNodeForClient("foo", "bar2");
        assertThat(nodeMapper.getNodeForClient("foo")).containsExactly("bar2");
    }

    @Test
    public void givenNewEntryWhenCleanupDoNotRemove() {
        nodeMapper.putNodeForClient("foo", "bar");
        int cleaned = nodeMapper.cleanUpMappings();
        assertThat(cleaned).isZero();
        assertThat(nodeMapper.getNodeForClient("foo")).containsExactly("bar");
    }

    @Test
    public void givenOldEntryWhenCleanupDoRemove() throws InterruptedException {
        nodeMapper.putNodeForClient("foo", "bar");
        Thread.sleep(500);
        int cleaned = nodeMapper.cleanUpMappings();
        assertThat(cleaned).isOne();
        assertThat(nodeMapper.getNodeForClient("foo")).isEmpty();
    }

    @Test
    public void givenInteractionWhenGetNodeThenDoNotCleanUpMapping() throws InterruptedException {
        nodeMapper.putNodeForClient("foo", "bar");
        Thread.sleep(500);
        nodeMapper.getNodeForClient("foo"); // refreshes mapping
        int cleaned = nodeMapper.cleanUpMappings();
        assertThat(cleaned).isZero();
    }

    @Test
    public void whenRemoveMappingThenRemoveAllMatching() {
        // setup
        Node node = mock(Node.class);
        when(node.getName()).thenReturn("foo");
        nodeMapper.putNodeForClient("to-be-removed", "foo");
        nodeMapper.putNodeForClient("stays", "bar");
        nodeMapper.putNodeForClient("to-be-removed-too", "foo");

        // execute
        nodeMapper.removeNodeFromMapping(node);

        // verify
        assertThat(nodeMapper.getSize()).isEqualTo(1);
        assertThat(nodeMapper.getNodeForClient("stays")).containsExactly("bar");
    }

}