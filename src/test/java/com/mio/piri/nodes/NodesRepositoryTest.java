/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import org.apache.commons.lang3.RandomStringUtils;
import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NodesRepositoryTest {

    private static final String PASS = "test";

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private String absFileName;

    private final HashGenerator hashGenerator = mock(HashGenerator.class);

    private NodesRepository repository;

    @Before
    public void initFile() throws IOException {
        folder.create();
        absFileName = folder.getRoot().getAbsolutePath() + File.separatorChar + RandomStringUtils.randomAlphabetic(5) + ".h2";
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
    }

    @Test
    public void whenSaveToFileThenPersistData() {
        repository.addNode("some-key", "some-name", "some-url", false, "some-address");
        repository.saveToFile();
        when(hashGenerator.publicKey(anyString())).thenReturn("public-hash");
        NodesRepository newRepository = new NodesRepository(absFileName, PASS, true, hashGenerator);
        assertThat(newRepository.getAllNodes()).hasSize(1);
        NodeProperties node = newRepository.getAllNodes().iterator().next();
        assertThat(node.key).isEqualTo("public-hash");
        assertThat(node.name).isEqualTo("some-name");
        assertThat(node.url).isEqualTo("some-url");
        assertThat(node.pow).isFalse();
        assertThat(node.address).isEqualTo("some-address");
    }

    @Test
    public void givenNoFileNameWhenSaveToFileThenDoNotPersist() {
        repository = new NodesRepository(null, null, false, hashGenerator);
        repository.addNode("foo", "bar", "some-url", true, null);
        repository.saveToFile();
        NodesRepository newRepository = new NodesRepository(null, null, false, hashGenerator);
        assertThat(newRepository.getAllNodes()).hasSize(0);
    }

    @Test
    public void whenAddNodeThenAddToMap() {
        repository.addNode("foo", "bar", "some-url", true, null);
        assertThat(repository.getAllNodes()).hasSize(1);
    }

    @Test
    public void whenGetAllNodesThenReturnNodeProperties() {
        when(hashGenerator.publicKey(anyString())).thenReturn("public-hash");
        repository.addNode("some-key", "some-name", "some-url", true, "some-address");
        Collection<NodeProperties> allNodes = repository.getAllNodes();
        assertThat(allNodes.iterator().next().key).isEqualTo("public-hash");
        assertThat(allNodes.iterator().next().name).isEqualTo("some-name");
        assertThat(allNodes.iterator().next().url).isEqualTo("some-url");
        assertThat(allNodes.iterator().next().pow).isTrue();
        assertThat(allNodes.iterator().next().address).isEqualTo("some-address");
    }

    @Test
    public void whenGetNodeNameThenReturnOptionalName() {
        repository.addNode("foo", "bar", "some-url", true, null);
        assertThat(repository.getNodeName("foo").get()).isEqualTo("bar");
        assertThat(repository.getNodeName("not-available").isEmpty()).isTrue();
    }

    @Test
    public void whenRemoveNodeThenRemoveFromMap() {
        repository.addNode("foo", "bar", "some-url", true, null);
        repository.removeNode("foo");
        assertThat(repository.getAllNodes()).isEmpty();
    }

    @Test(expected = IllegalStateException.class)
    public void givenSameKeyWhenAddThenThrow() {
        repository.addNode("x", "foo", "bar", true, null);
        repository.addNode("x", "bar", "foo", true, null);
    }

    @Test
    public void whenRemoveNodeByNameThenReturnNodeAndRemove() {
        repository.addNode("x", "y", "z", false, null);
        assertThat(repository.removeNodeByName("y").isEmpty()).isFalse(); // returns node
        assertThat(repository.removeNodeByName("y").isEmpty()).isTrue(); // already removed

    }

    @Test
    public void givenValidKeyWhenContainsKeyThenReturnTrue() {
        repository.addNode("x", "y", "z", false, null);
        assertThat(repository.containsKey("x")).isTrue();
    }

    @Test
    public void givenUnknownKeyWhenContainsKeyThenReturnFalse() {
        repository.addNode("x", "y", "z", false, null);
        assertThat(repository.containsKey("foo")).isFalse();
    }

    @Test
    public void givenPartialKeyWhenContainsKeyStartingWithThenReturnTrue() {
        repository.addNode("xyz", "y", "z", false, null);
        assertThat(repository.containsKeyStartingWith("xy")).isTrue();
    }

    @Test
    public void givenUnknownKeyWhenContainsKeyStartingWithThenReturnFalse() {
        repository.addNode("xyz", "y", "z", false, null);
        assertThat(repository.containsKeyStartingWith("xz")).isFalse();
    }

    @Test(expected = IllegalStateException.class)
    public void givenV1DbWhenInitThenThrow() {
        // close (empty) test db. not sure if necessary
        repository.saveToFile();
        // init db in old db format
        MVStore.Builder storeBuilder = new MVStore.Builder().fileName(absFileName);
        storeBuilder.compress();
        storeBuilder.encryptionKey(PASS.toCharArray());
        MVStore store = storeBuilder.open();
        MVMap<String, String[]> registrations = store.openMap("registrations");
        registrations.put("9", new String[]{"y", "z"});
        store.close();
        // reopen test db.
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
    }

    @Test
    public void givenV3WhenInitThenConvertToV4() {
        // close (empty) test db. not sure if necessary
        repository.saveToFile();
        // init db in old db format
        MVStore.Builder storeBuilder = new MVStore.Builder().fileName(absFileName);
        storeBuilder.compress();
        storeBuilder.encryptionKey(PASS.toCharArray());
        MVStore store = storeBuilder.open();
        MVMap<String, String[]> registrations = store.openMap("registrations");
        registrations.put("abc9xyz", new String[]{"name", "url", "true"});
        store.close();
        // reopen test db
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
        NodeProperties nodeProperties = repository.removeNode("abc9xyz").get();
        assertThat(nodeProperties.name).isEqualTo("name");
        assertThat(nodeProperties.url).isEqualTo("url");
        assertThat(nodeProperties.pow).isTrue();
        assertThat(nodeProperties.address).isNull();
    }

}