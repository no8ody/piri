/*
 * Copyright (c) 2019.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class PowNodeTest {

    private final NodeClient nodeClient = mock(NodeClient.class);
    private final MeterFactory metrics = mock(MeterFactory.class);

    private final CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults("foo");
    private final Timer timer = Metrics.timer("timer");
    private final Timer.Sample sample = mock(Timer.Sample.class);

    private final IriCommand command = mock(IriCommand.class);
    private final ResponseEntity<String> okResponseEntity = ResponseEntity.status(HttpStatus.OK).body("foo");

    private final PowNode node = new PowNode("foo", "url", "fooKey", nodeClient, metrics, circuitBreaker);

    @Before
    public void setUpMocks() {
        when(command.getCommand()).thenReturn("foo");
        when(metrics.createCommandTimer(anyString(), anyString(), anyString())).thenReturn(timer);
        when(metrics.sample()).thenReturn(sample);
    }

    private void setWebClientResponse(ResponseEntity<String> clientResponse) {
        when(nodeClient.exchange(any(Node.class), any(AttachToTangle.class))).thenReturn(Mono.just(clientResponse));
    }

    @Test
    public void shouldSupportAttachToTangle() {
        assertThat(node.isSupported(new AttachToTangle())).isTrue();
    }

    @Test
    public void givenClosedCircuitBreakerWhenHealthCheckThenDoNotThrow() {
        setWebClientResponse(okResponseEntity); // mock node client to return response mono.
        circuitBreaker.transitionToOpenState(); // no call because circuit is open.
        Mono<NodeInfo> nodeInfoMono = node.queryNodeHealth();
        StepVerifier.create(nodeInfoMono)
                .assertNext(info -> {
                    assertThat(info).isNotNull();
                    assertThat(info.isSynced()).isFalse();
                })
                .verifyComplete();
        assertThat(node.isAvailable()).isFalse(); // failed health check disables node
    }

    @Test
    public void givenPowAvailableWhenPowCheckThenSetAvailable() {
        setWebClientResponse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
        node.setUnavailable("test preparation");

        StepVerifier.create(node.queryNodeHealth())
                .assertNext(info -> assertThat(info.supportsPow()).isTrue())
                .verifyComplete();

        assertThat(node.isAvailable()).isTrue();
    }

    @Test
    public void given401WhenPowCheckThenSetDisabled() {
        setWebClientResponse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());

        StepVerifier.create(node.queryNodeHealth())
                .assertNext(info -> assertThat(info.supportsPow()).isTrue())
                .verifyComplete();

        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void given429WhenPowCheckThenSetDisabled() {
        setWebClientResponse(ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build());

        StepVerifier.create(node.queryNodeHealth())
                .assertNext(info -> assertThat(info.supportsPow()).isTrue())
                .verifyComplete();

        assertThat(node.isAvailable()).isFalse();
    }

    @Test
    public void given5xxWhenPowCheckThenSetDisabled() {
        setWebClientResponse(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());

        StepVerifier.create(node.queryNodeHealth())
                .assertNext(info -> assertThat(info.supportsPow()).isTrue())
                .verifyComplete();

        assertThat(node.isAvailable()).isFalse();
    }

}